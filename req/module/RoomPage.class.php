<?php
class RoomPage extends AbstractPageModule {
	
	function doBeforeOutput(){
		$this->doInit();
	}

	function doContent(){
		$id = $this->request->getValue('id');
		
		if(!$id){  // Весь список
			
			$query = $this->conn->newStatement("SELECT r.id_category,  c.name AS name_category
												FROM room r INNER JOIN category c ON r.id_category=c.id 
												WHERE r.active=1 
												GROUP BY r.id_category ORDER BY c.pos DESC");
			$data = $query->getAllRecords(); 
			foreach ($data as $key=>$value) {
				$query = $this->conn->newStatement("SELECT * FROM room WHERE id_category=:id_category: AND active=1 ORDER BY seats ASC");
				$query->setInteger('id_category', $value['id_category']);
				$data_rooms = $query->getAllRecords(); 
				if ($data_rooms) {
					$data[$key]['rooms'] = $data_rooms;
				}
			}
			$this->template->assign('data', $data);
			$this->setPageTitle("Номера и цены");
		}
		else{ // выбранный элемент
			$query = $this->conn->newStatement("SELECT r.*, cat.name AS name_category
												FROM room r 
												INNER JOIN category cat ON r.id_category=cat.id 
												WHERE r.id=:id:");
			$query->setInteger('id', $id);
			$data_item = $query->getFirstRecord();

			$query = $this->conn->newStatement("SELECT attr_room.*, attr.name AS name_attribute, attr.ext AS ext
												FROM attribute_room attr_room 
												INNER JOIN attribute attr ON attr_room.id_attribute=attr.id 
												WHERE attr_room.id_room=:id_room: 
												ORDER BY attr.pos DESC");
			$query->setInteger('id_room', $id);
			$data_attribute = $query->getAllRecords();
			if ($data_attribute)
				$data_item['attribute'] = $data_attribute;
				
			$this->template->assign('data_item', $data_item);
			
			$this->setPageTitle("".($data_item['title']?$data_item['title']:$data_item['name'])." / Номера и цены");
						
			// достаем галерею.
			$query = $this->conn->newStatement("SELECT * FROM room_photo WHERE id_room=:id_room: ORDER BY pos DESC, id DESC");
			$query->setInteger('id_room', $id);
			$data_photo = $query->getAllRecords();
			$this->template->assign('data_photo', $data_photo);
			
			$query = $this->conn->newStatement("SELECT r.*, cat.name AS name_category
												FROM room r 
												INNER JOIN category cat ON r.id_category=cat.id 
												WHERE r.id!=:id: LIMIT 3");
			$query->setInteger('id', $id);
			$data_rooms = $query->getAllRecords();
			$this->template->assign('data_rooms', $data_rooms);

			$query = $this->conn->newStatement("SELECT * FROM info WHERE id=:id:");
			$query->setInteger('id', 5);
			$data_phone = $query->getFirstRecord();
			$this->template->assign('data_phone', $data_phone);
		}
		
		$this->response->write($this->renderTemplate('room.tpl'));
	}
}
?>