<?php
require_once('module/FormPageModule.class.php');
require_once('validator/Validator.class.php');
require_once("util/ImageUtil.class.php");

class UserPage extends FormPageModule {
	function doBeforeOutput(){
		$this->doInit();
		$rand_new = rand(1, 100000);
		$this->template->assign('rand_new', $rand_new);
	}

	function doFormInit(){
		
		$id = $this->request->getValue("id");
		
		if ($this->user['id'] == $id) { // если ID в URLe тотже что залогинет
			$conn = &DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM user WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();
			
			if($data['dob'] AND $data['dob']!="0000-00-00"){
				$temp = explode("-", $data['dob']);
				$data['dob'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
			}
			
			$this->template->assign('data', $data);
			
			$this->response->write($this->renderTemplate('user/user.tpl'));
		}
		else{
			$this->response->redirect("/");
		}
	}

	function doFormInValid(){		
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate('user/user.tpl'));
	}

	function doFormValid(){
		//$action = $this->request->getValue("action");
		$id = $this->request->getValue('id');
		
		$conn = &DbFactory::getConnection();

		// преобразование имени сайта
		if($this->formData["site"]){
			if(!strstr($this->formData["site"], "http://")){
				$this->formData["site"] = "http://{$this->formData['site']}";
			}
		}
		
		// преобразование даты
		if($this->formData["dob"]){
			$temp = explode("-", $this->formData["dob"]);
			$this->formData["dob"] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
		}
				
		$query = $conn->newStatement("UPDATE user SET name=:name:, email=:email:, avatar=:avatar:, sex=:sex:, dob=:dob:, city=:city:, about=:about:, site=:site:, skype=:skype:, icq=:icq:, address=:address:, tel=:tel: WHERE id=:id:");
	    $query->setInteger('id', $id);
		$query->setVarChar('name',$this->formData['name']);
		$query->setVarChar('email',$this->formData['email']);
		$query->setInteger("sex", $this->formData["sex"]?$this->formData["sex"]:0);
		$query->setDate("dob", $this->formData["dob"]?$this->formData["dob"]:NULL);
		$query->setVarChar("city", $this->formData["city"]);
		$query->setText("about", $this->formData["about"]);
		$query->setVarChar("site", $this->formData["site"]);
		$query->setVarChar("skype", $this->formData["skype"]);
		$query->setVarChar("icq", $this->formData["icq"]);
		$query->setVarChar("address", $this->formData["address"]);
		$query->setVarChar("tel", $this->formData["tel"]);	
		
		$image = $this->request->getValue("avatar");
		if ($image){
			if ($image['tmp_name']) {
				$image_pieces = explode(".",$image['name']);
				$image_type = $image_pieces[count($image_pieces)-1];
								
				$query->setVarChar('avatar', $image_type);
				$query->execute();
				
				ImageUtil::uploadImage($image['tmp_name'],"uploaded/user","{$id}_tmp.{$image_type}");
				$tmp_image = new ImageUtil("uploaded/user/{$id}_tmp.{$image_type}");
				$tmp_image->ResizeFromRaf(50, 50, "uploaded/user/{$id}.{$image_type}");
				FileSystem::deleteFile("uploaded/user/{$id}_tmp.{$image_type}");
			}
			else{
				$query->setVarChar('avatar', $this->formData['photo_type']);
				$query->execute();
			}
		}
		else{
			$query->execute();
		}
		
		$this->response->redirect("/user/{$id}/?note=save");
	}

	function doValidation() {
		// пользователь может сменить email надо проверить нет ли такого в базе!!!!
		$conn = &DbFactory::getConnection();
       
		$validator = new Validator($this->formData);
		$rules = array(
			new EmptyFieldRule("email", "Эл. почта"),
			new WrongEmailFormatRule("email", "Эл. почта"),
		);

		$validator->validate($rules);
		if ($errors = $validator->getErrorList()) {
			$this->template->assign("errors", $errors);
			return false;
		}
		else {
			$id_user_same_email = UserAction::checkEmailBusy($this->formData['email']);
						
			if($id_user_same_email AND ($id_user_same_email != $this->user['id']) ){ // надо проверить не его ли это email!
				$errors[] = "Пользователь с таким адресом эл. почты уже есть в базе";
				$this->template->assign("errors", $errors);
				return false;
			}
		}        
        return true;
	}
}

?>