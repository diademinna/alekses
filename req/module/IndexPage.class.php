<?php
class IndexPage extends AbstractPageModule {

	function doBeforeOutput(){
		//$this->registerThis('aaa');  // XAJAX - функции для этого класса
		//$this->processRequest();  // если нет общих XAJAX-функций 
		$this->doInit();
	}

	function doContent(){
		$this->template->assign('unit', "index");
		
		//3 номера на главной
		$query = $this->conn->newStatement("SELECT r.*, cat.name AS name_category
											 FROM room r 
											 INNER JOIN category cat ON r.id_category=cat.id 
											 WHERE r.main=1 AND r.active=1 ORDER BY r.pos LIMIT 3");
		$data_room_main = $query->getAllRecords();
		if (!$data_room_main) {
			$query = $this->conn->newStatement("SELECT r.*, cat.name AS name_category
											 FROM room r 
											 INNER JOIN category cat ON r.id_category=cat.id 
											 WHERE r.active=1 ORDER BY r.pos LIMIT 3");
			$data_room_main = $query->getAllRecords();
		}
		$this->template->assign('data_room_main', $data_room_main);

		$query = $this->conn->newStatement("SELECT * FROM pages WHERE id=:id:");
		$query->setInteger('id', 1);
		$data_main_text = $query->getFirstRecord();
		$this->template->assign('data_main_text', $data_main_text);
	
		//дополнительные услуги
		$query = $this->conn->newStatement("SELECT * FROM service WHERE active=1 ORDER BY pos DESC LIMIT 3");
		$data_service = $query->getAllRecords();
		$this->template->assign('data_service', $data_service);

		
		$this->response->write($this->renderTemplate('index.tpl'));
	}
	
	
	//*** DEVELOPER AJAX ***//	
	//	function aaa(){
	//		$xajax = new xajaxResponse();
	//		$xajax->assign('map', 'innerHTML', "str_text" );	
	//		return $xajax;
	//	}
	
}
?>