<?php
class ReviewPage extends AbstractPageModule{
	
	function doContent(){
		$name = $this->request->getValue('name');
		$email = $this->request->getValue('email');
		$text = $this->request->getValue('text');
		$rating = $this->request->getValue('rating');

	
		$query=$this->conn->newStatement("INSERT INTO review SET name=:name:, email=:email:, rating=:rating:, date=now(), text=:text: ");
		$query->setVarChar('name', $name);
		$query->setVarChar('email', $email);
		$query->setInteger('rating', $rating?$rating:"");
		$query->setText('text', $text?$text:"");
		$query->execute();
		

		$id_email_bd = 13;
		$query=$this->conn->newStatement("SELECT * FROM info WHERE id=:id:");
		$query->setInteger('id', $id_email_bd);
		$data_email = $query->getFirstRecord();

		$to  = $data_email['text'];
		$from = 'info@testy-na5ballov.ru';
		$subject = 'Добавлен новый отзыв / '.$_SERVER['HTTP_HOST'];
		
		$message = "Отзыв: ".$text."<br/>";
		if ($name)
			$message .= "Имя: ".$name."<br/>";
		if ($email)
			$message .= "E-mail: ".$email."<br/>";
		if ($rating)
			$message .= "Оценка: ".$rating."<br/>";

		$headers = "Content-type: text/html; charset=utf-8\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";
		mail($to, $subject, $message, $headers);
		echo json_encode(array('status'=>'success'));
		
		
		
		die();		
	}
	
}
?>