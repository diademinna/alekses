<?php
class CheckDateBookPage extends AbstractPageModule{
	
	function doContent(){
		$id_room = $this->request->getValue('id_room');

		$query = $this->conn->newStatement("SELECT * FROM book_room WHERE id_room=:id_room: ORDER BY date_input ASC");
		$query->setInteger('id_room', $id_room);
		$data = $query->getAllRecords();
		
		if ($data) {
			$arrayOfDatesAll = array();
			foreach ($data as $key=>$value) {
				$from = new DateTime($value['date_input']);
				$to   = new DateTime($value['date_output']);
				$to = $to->modify( '+1 day' ); 

				$period = new DatePeriod($from, new DateInterval('P1D'), $to);

				$arrayOfDates = array_map(
					function($item){return $item->format('Y-m-d');},
					iterator_to_array($period)
				);
				$arrayOfDatesAll = array_merge($arrayOfDatesAll, $arrayOfDates);
			}
		}
		
		echo json_encode(array('data'=>$arrayOfDatesAll));
		
		die();		
	}
	
}
?>