<?php
class BookRoomPage extends AbstractPageModule{
	
	function doContent(){
		$date_input = $this->request->getValue('date_input');
		$date_output = $this->request->getValue('date_output');
		$phone = $this->request->getValue('phone');
		$email = $this->request->getValue('email');
		$name_room = $this->request->getValue('name_room');
		$price_room = $this->request->getValue('price_room');
		$id_room = $this->request->getValue('id_room');
		if($date_input AND $date_input!="0000-00-00"){ // преобразование даты
			$temp = explode(".", $date_input);
			$date_input = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
		}
		if($date_output AND $date_output!="0000-00-00"){ // преобразование даты
			$temp = explode(".", $date_output);
			$date_output = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
		}
		$query=$this->conn->newStatement("INSERT INTO book_room SET date=now(), date_input=:date_input:, date_output=:date_output:, phone=:phone:, email=:email:, name_room=:name_room:, price_room=:price_room:, id_room=:id_room:");
		$query->setDate('date_input', $date_input);
		$query->setDate('date_output', $date_output);
		$query->setVarChar('phone', $phone?$phone:"");
		$query->setVarChar('email', $email?$email:"");
		$query->setVarChar('name_room', $name_room?$name_room:"");
		$query->setVarChar('price_room', $price_room?$price_room:"");
		$query->setInteger('id_room', $id_room?$id_room:"");
		$query->execute();
		

		$id_email_bd = 1;
		$query=$this->conn->newStatement("SELECT * FROM info WHERE id=:id:");
		$query->setInteger('id', $id_email_bd);
		$data_email = $query->getFirstRecord();

		$to  = $data_email['text'];
		$from = "{$_SERVER['HTTP_HOST']} <".ADMIN_EMAIL.">";
		
		$subject = 'Бронирование номера / '.$_SERVER['HTTP_HOST'];
		
		$message = "";
		if ($date_output)
			$message .= "Дата заезда: ".$date_input."<br/>";
		if ($date_output)
			$message .= "Дата выезда: ".$date_output."<br/>";
		if ($phone)
			$message .= "Контактный номер: ".$phone."<br/>";
		if ($email)
			$message .= "E-mail: ".$email."<br/>";
		if ($name_room)
			$message .= "Номер: ".$name_room."<br/>";
		if ($price_room)
			$message .= "Цена за номер: ".$price_room." руб/сут";

		$headers = "Content-type: text/html; charset=utf-8\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";

		mail($to, $subject, $message, $headers);
		//письмо клиенту
		$to  = $email;
		$from = "{$_SERVER['HTTP_HOST']} <".ADMIN_EMAIL.">";
		
		$subject = 'Бронирование номера / '.$_SERVER['HTTP_HOST'];
		
		$message = "<body>
								<p style='font-size:12px;font-style:italic;'>Отель «Алексес»</p>
<p style='font-size:12px;font-style:italic;'>Адрес: 440600, г.Пенза, Проезд Володарского, 4</p>
<p style='font-size:12px;font-style:italic;'>Телефон: 8(8412)454445, 8(8412)456654,89374343322</p>
<p>&nbsp;</p>
<p style='font-size:16px;font-weight:800;text-align:center;'>ПОДТВЕРЖДЕНИЕ БРОНИРОВАНИЯ</p>
<p>&nbsp;</p>
<p style='font-style:italic;text-align:center;'>Уважаемый(ая) !</p>
<p>&nbsp;</p>
<p>Служба размещения отеля «Алексес» подтверждает бронирование и предоставляет Вам номер с ".$date_input." по ".$date_output." согласно Вашей заявке. В стоимость номера входит завтрак.<br />
Спасибо, что Вы выбрали наш отель.</p>
<p>&nbsp;</p>
<p style='text-align:right;font-style:italic;'>С уважением, администрация отеля «Алексес»</p>

<p>&nbsp;</p>
								</body>";

		$headers = "Content-type: text/html; charset=utf-8\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";

		mail($to, $subject, $message, $headers);
				
		
		echo json_encode(array('status'=>'success'));
		
		
		
		die();		
	}
	
}
?>