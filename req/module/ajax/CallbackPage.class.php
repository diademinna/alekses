<?php
class CallbackPage extends AbstractPageModule{
	
	function doContent(){
		$name = $this->request->getValue('name');
		$phone = $this->request->getValue('phone');
		$text = $this->request->getValue('text');
		$query=$this->conn->newStatement("INSERT INTO callback SET date=now(), name=:name:, phone=:phone:, text=:text:");
		$query->setVarChar('name', $name?$name:"");
		$query->setVarChar('phone', $phone);
		$query->setText('text', $text?$text:"");
		$query->execute();
		
		$id_email_bd = 1;
		$query=$this->conn->newStatement("SELECT * FROM info WHERE id=:id:");
		$query->setInteger('id', $id_email_bd);
		$data_email = $query->getFirstRecord();

		$to  = $data_email['text'];
		$from = "{$_SERVER['HTTP_HOST']} <".ADMIN_EMAIL.">";
		
		$subject = 'Заявка на звонок / '.$_SERVER['HTTP_HOST'];
		
		$message = "";
		
		if ($name)
			$message .= "Имя: ".$name."<br/>";
		if ($phone)
			$message .= "Контактный номер: ".$phone."<br/>";
			$message .= "<br/>";
		if ($text)
			$message .= "Комментарий: ".$text;

		$headers = "Content-type: text/html; charset=utf-8\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "From: " . $from . "\r\n";
		$headers .= "Reply-To: " . $from . "\r\n";
		$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";

		mail($to, $subject, $message, $headers);

		echo json_encode(array('status'=>'success'));
		
		
		
		die();		
	}
	
}
?>