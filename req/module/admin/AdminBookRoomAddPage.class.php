<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminBookRoomAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {	
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', "book_room");

		/*список номеров*/
		$query = $this->conn->newStatement("SELECT r.id, r.name, r.price, cat.name AS name_category
											 FROM room r 
											 INNER JOIN category cat ON r.id_category=cat.id 
											 ORDER BY r.pos ");
		$data_room= $query->getAllRecords();
		$this->template->assign('data_room', $data_room);
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM book_room WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();
			if($data['date_input'] AND $data['date_input']!="0000-00-00"){ // преобразование даты
				$temp = explode("-", $data['date_input']);
				$data['date_input'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
			}
			if($data['date_output'] AND $data['date_output']!="0000-00-00"){ // преобразование даты
				$temp = explode("-", $data['date_output']);
				$data['date_output'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
			}
			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_book_room_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		if($this->formData['date_input'] AND $this->formData['date_input']!="0000-00-00"){ // преобразование даты
			$temp = explode("-", $this->formData['date_input']);
			$this->formData['date_input'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
		}
		if($this->formData['date_output'] AND $this->formData['date_output']!="0000-00-00"){ // преобразование даты
			$temp = explode("-", $this->formData['date_output']);
			$this->formData['date_output'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
		}
		

		$query = $this->conn->newStatement("SELECT r.id, r.name, r.price, cat.name AS name_category
											 FROM room r 
											 INNER JOIN category cat ON r.id_category=cat.id 
											 WHERE r.id=:id:");
		$query->setInteger('id', $this->formData['id_room']);
		$data_room_book = $query->getFirstRecord();
		if (!empty($id) && $action == "edit"){
			$query = $this->conn->newStatement("UPDATE book_room SET date_input=:date_input:,
											 date_output=:date_output:, 
											 email=:email:,
											 phone=:phone:,
											 id_room=:id_room:,
											 date=now(),
											 text=:text:,
											 name_room=:name_room:,
											 price_room=:price_room:
											 WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else {
			$query = $this->conn->newStatement("INSERT INTO book_room SET 
											 date_input=:date_input:,
											 date_output=:date_output:, 
											 email=:email:,
											 phone=:phone:,
											 id_room=:id_room:,
											 date=now(),
											 text=:text:,
											 name_room=:name_room:,
											 price_room=:price_room:");
		}
		$query->setDate('date_input', $this->formData['date_input']);
		$query->setDate('date_output', $this->formData['date_output']);
		$query->setVarChar('email', $this->formData['email']);
		$query->setVarChar('phone', $this->formData['phone']);
		$query->setInteger('id_room', $this->formData['id_room']);
		$query->setText('text', $this->formData['text']);
		$query->setVarChar('name_room', $data_room_book['name_category']." ".$data_room_book['name']);
		$query->setVarChar('price_room', $data_room_book['price']);
		$query->execute();
			

		$this->response->redirect("/admin/book_room/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_book_room_add.tpl"));
	}
	
	
	function doValidation(){		
		$rules = array(
			new EmptyFieldRule("date_input", 'Дата заезда'),
			new EmptyFieldRule("date_output", 'Дата выезда'),
			new EmptyFieldRule("email", 'Почтовый ящик'),
			new EmptyFieldRule("phone", 'Телефон'),
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM room WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/room/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_prev.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_big.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_banner.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE room SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/room/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>