<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminReviewAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', 'review');
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM review WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();

			$date = new DateTime($data['date']);
			$data['date'] = $date->format('Y-m-d');
			if($data['date'] AND $data['date']!="0000-00-00"){ // преобразование даты
				$temp = explode("-", $data['date']);
				$data['date'] = "{$temp[2]}-{$temp[1]}-{$temp[0]}";
			}

			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_review_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");

			
		$conn = &DbFactory::getConnection();
		// преобразование даты
		if($this->formData["date"]){
			$temp = explode("-", $this->formData["date"]);
			$this->formData["date"] = "{$temp[2]}-{$temp[1]}-{$temp[0]}"." ".date('H:i:s');
		}
		else{
			$this->formData["date"] = date('Y-m-d H:i:s');  // берем текущую
		}
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE review SET name=:name:, text=:text:, date=:date:, rating=:rating: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
			$query_pos = $this->conn->newStatement("SELECT MAX(pos)+1 FROM review");
			$pos = (int)$query_pos->getOneValue();
			if(!$pos){
				$pos = 1;
			}
			
			$query = $conn->newStatement("INSERT INTO review SET name=:name:, text=:text:, date=:date:, pos=:pos:, rating=:rating:, active=:active:");
			$query->setInteger('active', 1);
			$query->setInteger("pos", $pos);
		}
		
		$query->setVarChar('name', $this->formData['name']);
		$query->setText('text', $this->formData['text']);
		$query->setDate('date', $this->formData['date']);
		$query->setInteger('rating', $this->formData['rating']);
		$query->execute();
		
		$this->response->redirect("/admin/review/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_review_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("name", 'Имя'),			
			new EmptyFieldRule("text", 'Текст'),
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM review WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/review/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/review/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE review SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/review_gallery/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>