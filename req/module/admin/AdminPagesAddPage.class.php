<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminPagesAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		$id = $this->request->getValue("id");
		$this->template->assign("unit", 'page');
		$this->template->assign("subunit", $id);
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM pages WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();
			
			
			$this->template->assign('data', $data);
		}
		$this->template->assign("parent_id", $parent_id);
		$this->response->write($this->renderTemplate('admin/admin_pages_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
			
		$conn = &DbFactory::getConnection();
		
		
		
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE pages SET name=:name:, text=:text:,title=:title: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
			$query = $conn->newStatement("INSERT INTO pages SET name=:name:, text=:text:, title=:title:");
		}
		
		$query->setVarChar('name', $this->formData['name']);
		$query->setText('text', $this->formData['text']);
		$query->setVarChar('title', $this->formData['title']);
		$query->execute();
		
		
		
		$this->response->redirect("/admin/pages/list/");
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_pages_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("name", 'Название'),			
			new EmptyFieldRule("text", 'Контент'),
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	
	
}

?>