<?php
class AdminRoomListPage extends AbstractPageModule {

	function doBeforeOutput(){
		$this->Authenticate();
		
		$this->registerThis("ChangeCountPage", "Activate", "Sort", "ActivateMain", "ActivateBanner");
		$this->processRequest();
		$this->template->assign('unit', "room");
		$this->template->assign('subunit', "room");
	}

	function doContent(){
		$conn = &DbFactory::getConnection();
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		else{
			$get_param = "";
		}
		$this->template->assign('get_param', $get_param);
				
		$page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $page);
		
		$id = $this->request->getValue('id');
		$action = $this->request->getValue ('action');


		
		if ($action == "delete" && !empty($id))	{
			
			$query = $conn->newStatement("SELECT * FROM room WHERE id=:id:");
	        $query->setInteger('id', $id);
	        $data = $query->getFirstRecord();
			
			$query = $conn->newStatement("DELETE FROM room WHERE id=:id:");
	        $query->setInteger('id', $id);
	        $query->execute();
			FileSystem::deleteFile("uploaded/room/{$id}_sm.{$data['ext']}");
			FileSystem::deleteFile("uploaded/room/{$id}_banner.{$data['ext']}");
			FileSystem::deleteFile("uploaded/room/{$id}_prev.{$data['ext']}");
			FileSystem::deleteFile("uploaded/room/{$id}_big.{$data['ext']}");
			FileSystem::deleteFile("uploaded/room/{$id}.{$data['ext']}");
			
			///////////// удаление галлереи /////////////////////
			$query = $conn->newStatement("SELECT * FROM room_photo WHERE id_room=:id_room: ORDER BY id ASC");
			$query->setInteger('id_room', $id);
			$data_photo = $query->getAllRecords();
			
			$query = $conn->newStatement("DELETE FROM room_photo WHERE id_room=:id_room:");
	        $query->setInteger('id_room', $id);
	        $query->execute();
	        
	        foreach ($data_photo as $key=>$value){
	        	FileSystem::deleteFile("uploaded/room/{$id}/{$value['id']}_sm.{$value['ext']}");
	        	FileSystem::deleteFile("uploaded/room/{$id}/{$value['id']}_big.{$value['ext']}");
				FileSystem::deleteFile("uploaded/room/{$id}/{$value['id']}.{$value['ext']}");
	        }
			
			$this->response->redirect("/admin/room/list/{$page}/".($get_param?$get_param:""));
		}
		else {
			$query = $this->conn->newStatement("SELECT * FROM category");
			$data_category = $query->getAllRecords('id');

			if($this->request->getValue('count_page')){
				define('COUNT_PAGE', $this->request->getValue('count_page'));
			}
			else{
				define('COUNT_PAGE', 20);
			}
			require_once 'module/common/PagerFactory.class.php';
						
			$pager = new PagerFactory();
			$sql = "SELECT * FROM room ORDER BY id_category DESC";
			$fromWhereCnt = "room";
			$href = "/admin/room/list/".$get_param;
			
			$pagerString = $pager->getPagerString($page, $sql, $fromWhereCnt, $href);
			$data = $pager->getPageData();
			foreach ($data as $key=>$value) {
				$data[$key]['name_category'] = $data_category[$data[$key]['id_category']]['name'];
			}
			
			
			$this->template->assign('pager_string', $pagerString);
			$this->template->assign('data', $data);

			$number_main = 0;
			foreach ($data as $key=>$value) {
				if ($value['main'] == 1) {
					$number_main++;
				}
			}
			$this->template->assign('number_main', $number_main);						
			$this->response->write($this->renderTemplate('admin/admin_room_list.tpl'));
		}
	}
	
	//*** DEVELOPER AJAX ***//
	
	// Функция вызывается при смене количества отображаемых элементов на странице
	function ChangeCountPage($val, $get_param){
		$objResponse = new xajaxResponse();
		
		$new_get_param = $this->ParamGET($val, $get_param, "count_page");
		$objResponse->redirect("/admin/room/list/{$new_get_param}");
		
		return $objResponse;
	}
	
	// Отображать или скрыть выбранный элемент.
	function Activate($id){
		$xajax = new xajaxResponse();
		
		$conn =& DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM room WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		$query = $conn->newStatement("UPDATE room SET active=:active: WHERE id=:id:");
		$query->setInteger("active", $data['active']==1?0:1);
		$query->setInteger("id", $id);
		$query->execute();
		
		return $xajax;
	}
	function ActivateMain($id){
		$xajax = new xajaxResponse();
		
		$conn =& DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM room WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		$query = $conn->newStatement("UPDATE room SET main=:main: WHERE id=:id:");
		$query->setInteger("main", $data['main']==1?0:1);
		$query->setInteger("id", $id);
		$query->execute();
		
		return $xajax;
	}
	function ActivateBanner($id){
		$xajax = new xajaxResponse();
		
		$conn =& DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM room WHERE id={$id}");
		$data = $query->getFirstRecord();

		
		$query = $conn->newStatement("UPDATE room SET banner=:banner:");
		$query->setInteger("banner", 0);
		$query->execute();
		
		$query = $conn->newStatement("UPDATE room SET banner=:banner: WHERE id=:id:");
		$query->setInteger("banner", $data['banner']==1?0:1);
		$query->setInteger("id", $id);
		$query->execute();
		
		return $xajax;
	}
	
	// Сортировка с помощью плагина Sortable
	function Sort($mass_sort, $min_pos=1){ //  $min_pos - минимальное значение позиции на странице.
		$objResponse = new xajaxResponse();
		$conn = &DbFactory::getConnection();
		
		$mass_sort = str_replace('item_', "", $mass_sort);
		$mass_sort = array_reverse($mass_sort); // сортировка в обратном порядке.
		
		foreach ($mass_sort as $key => $value) {
			$query = $conn->newStatement("UPDATE room SET pos=:pos: WHERE id=:id:");
	        $query->setInteger('pos', $min_pos);
	        $query->setInteger('id', $value);
	        $query->execute();
			$min_pos++;
		}
		
		return $objResponse;
	}
	
	
}
?>