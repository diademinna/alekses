<?php
class AdminPagesListPage extends AbstractPageModule {

	function doBeforeOutput(){
		$this->Authenticate();
		
		$this->registerThis("ChangeCountPage");
		$this->processRequest();
		$this->template->assign("unit", 'page');
	}

	function doContent(){
		$conn = &DbFactory::getConnection();
		
		$id = $this->request->getValue('id');
		$action = $this->request->getValue ('action');
		
		if ($action == "delete" && !empty($id))	{
		
			$query = $conn->newStatement("DELETE FROM pages WHERE id=:id:");
			$query->setInteger('id', $id);
			$query->execute();
			
			
			$this->response->redirect("/admin/pages/list/{$id_pages}/");
		}
		else {
			
			$query = $conn->newStatement("SELECT * FROM pages ORDER BY id ASC");
			$data = $query->getAllRecords();
			$this->template->assign('data', $data);
									
			$this->response->write($this->renderTemplate('admin/admin_pages_list.tpl'));
		}
	}
	
	//*** DEVELOPER AJAX ***//
	
	// Функция вызывается при смене количества отображаемых элементов на странице
	function ChangeCountPage($val, $get_param){
		$objResponse = new xajaxResponse();
		
		$new_get_param = $this->ParamGET($val, $get_param, "count_page");
		$objResponse->redirect("/admin/pages/list/{$new_get_param}");
		
		return $objResponse;
	}
	
	
	
}
?>