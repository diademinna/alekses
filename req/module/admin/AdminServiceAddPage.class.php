<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminServiceAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', 'service');
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM service WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();

			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_service_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");

			
		$conn = &DbFactory::getConnection();
		
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE service SET name=:name:, text=:text: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
				$query_pos = $this->conn->newStatement("SELECT MAX(pos)+1 FROM service");
				$pos = (int)$query_pos->getOneValue();
				if(!$pos){
					$pos = 1;
				}

				$query = $conn->newStatement("INSERT INTO service SET name=:name:, text=:text:, pos=:pos:");
				$query->setInteger('pos', $pos);
			}
		
			$query->setVarChar('name', $this->formData['name']);
			$query->setText('text', $this->formData['text']);
			$query->execute();
				
				
		
		$this->response->redirect("/admin/service/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_service_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("name", 'Название')
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM service WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/service/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/service/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE service SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/service_gallery/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>