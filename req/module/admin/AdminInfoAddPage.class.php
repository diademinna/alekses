<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminInfoAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		
		$this->template->assign("tinymce", 1);
		$this->template->assign("unit", "info");
		
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM info WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();
			
			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_info_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
			
		$conn = &DbFactory::getConnection();
		
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE info SET text=:text: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
			$query = $conn->newStatement("INSERT INTO info SET name=:name:, text=:text:");
			$query->setVarChar('name', $this->formData['name']);
		
		}
		
		$query->setVarChar('text', $this->formData['text']);
		
		$query->execute();
		
		
		$this->response->redirect("/admin/info/list/");
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_info_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("text", 'Значение'),	
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
}

?>