<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminAttributeAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', 'room');
		$this->template->assign('subunit', 'attribute');
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM attribute WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();

			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_attribute_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");

			
		$conn = &DbFactory::getConnection();
		
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE attribute SET name=:name:, ext=:ext: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
				$query_pos = $this->conn->newStatement("SELECT MAX(pos)+1 FROM attribute");
				$pos = (int)$query_pos->getOneValue();
					if(!$pos){
						$pos = 1;
					}

				$query = $conn->newStatement("INSERT INTO attribute SET name=:name:, pos=:pos:, ext=:ext:");
				$query->setInteger('pos', $pos);
			}
		
			$query->setVarChar('name', $this->formData['name']);
			$image = $this->request->getValue("image");
			if ($image['tmp_name']){ // Если картинку ЗАГРУЖАЮТ
				
				// расширение
				$image_pieces = explode(".", $image['name']);
				$image_type = $image_pieces[count($image_pieces)-1];
				$query->setVarChar('ext', $image_type);
				$query->execute();
				
				if (!empty($id) && $action == "edit"){
					$id_new = $id;
				}
				else{
					$id_new = $query->getInsertId();
				}
				
				ImageUtil::uploadImage($image['tmp_name'], "uploaded/attribute/",  "{$id_new}.{$image_type}");
				
			}
			else{ // Если картинку не загружают
				$query->setVarChar('ext', $this->formData['ext']?$this->formData['ext']:NULL);
				$query->execute();
			}
		
		$this->response->redirect("/admin/attribute/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_attribute_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("name", 'Название')
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM attribute WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/attribute/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/attribute/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE attribute SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/attribute_gallery/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>