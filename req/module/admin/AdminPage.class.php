<?php
class AdminPage extends AbstractPageModule {

	function doBeforeOutput(){
		$this->Authenticate();
	}

	function doContent()	{
		//бронирование
		$query = $this->conn->newStatement("SELECT * FROM book_room ORDER BY date DESC LIMIT 5 ");
		$data_book_room = $query->getAllRecords();
		$this->template->assign('data_book_room', $data_book_room);

		$query = $this->conn->newStatement("SELECT COUNT(*) FROM book_room");
		$data_book_room_all = $query->getFirstRecord();
		$this->template->assign('data_book_room_all', $data_book_room_all['COUNT(*)']);

		$query = $this->conn->newStatement("SELECT COUNT(*) FROM book_room WHERE date>=CURDATE() ");
		$data_book_room_now = $query->getFirstRecord();
		$this->template->assign('data_book_room_now', $data_book_room_now['COUNT(*)']);


		//заявки на звонок
		$query = $this->conn->newStatement("SELECT * FROM callback ORDER BY date DESC LIMIT 5 ");
		$data_callback = $query->getAllRecords();
		$this->template->assign('data_callback', $data_callback);

		$query = $this->conn->newStatement("SELECT COUNT(*) FROM callback");
		$data_callback_all = $query->getFirstRecord();
		$this->template->assign('data_callback_all', $data_callback_all['COUNT(*)']);

		$query = $this->conn->newStatement("SELECT COUNT(*) FROM callback WHERE date>=CURDATE() ");
		$data_callback_now = $query->getFirstRecord();
		$this->template->assign('data_callback_now', $data_callback_now['COUNT(*)']);


//отзывы
		$query = $this->conn->newStatement("SELECT COUNT(*) FROM review");
		$data_review_all = $query->getFirstRecord();
		$this->template->assign('data_review_all', $data_review_all['COUNT(*)']);

		$query = $this->conn->newStatement("SELECT COUNT(*) FROM review WHERE date>=CURDATE() ");
		$data_review_now = $query->getFirstRecord();
		$this->template->assign('data_review_now', $data_review_now['COUNT(*)']);

		$this->response->write($this->renderTemplate('admin/index.tpl'));
	}

}

?>