<?php
class AdminInfoListPage extends AbstractPageModule {

	function doBeforeOutput(){
		$this->Authenticate();
		
		$this->registerThis("Activate", "Sort");
		$this->processRequest();
		$this->template->assign('unit', 'info');
	}

	function doContent(){
		$conn = &DbFactory::getConnection();
		
		
		$id = $this->request->getValue('id');
		$action = $this->request->getValue ('action');
		
		if ($action == "delete" && !empty($id))	{
			
			$query = $conn->newStatement("DELETE FROM info WHERE id=:id:");
	        $query->setInteger('id', $id);
	        $query->execute();
			
			
			$this->response->redirect("/admin/info/list/".($get_param?$get_param:""));
		}
		else {
			
			$query = $conn->newStatement("SELECT * FROM info ORDER BY pos ASC");
			$data = $query->getAllRecords();
			$this->template->assign('data', $data);
									
			$this->response->write($this->renderTemplate('admin/admin_info_list.tpl'));
		}
	}
	
	//*** DEVELOPER AJAX ***//
}
?>