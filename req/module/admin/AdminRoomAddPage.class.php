<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");
require_once("util/ImageUtil.class.php");

class AdminRoomAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {	
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', "room");

		/*список категорий*/
		$query = $this->conn->newStatement("SELECT * FROM category ORDER BY name ASC");
		$data_category = $query->getAllRecords();
		$this->template->assign('data_category', $data_category);

		$query = $this->conn->newStatement("SELECT * FROM attribute ORDER BY pos DESC");
		$data_attribute = $query->getAllRecords();
		$this->template->assign('data_attribute', $data_attribute);
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		
		if (!empty($id) AND $action == "edit"){
			$query = $this->conn->newStatement("SELECT * FROM room WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();
			$this->template->assign('data', $data);

			$query = $this->conn->newStatement("SELECT * FROM attribute_room WHERE id_room=:id_room:");
			$query->setInteger('id_room', $id);
			$data_attribute_room = $query->getAllRecords('id_attribute');
			$this->template->assign('data_attribute_room', $data_attribute_room);


		}
		$this->response->write($this->renderTemplate('admin/admin_room_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		$id_room = 0;
		
					
		if (!empty($id) && $action == "edit"){
			$query = $this->conn->newStatement("UPDATE room SET name=:name:, id_category=:id_category:, seats=:seats:, apartment=:apartment:, area=:area:, price=:price:, ext=:ext:, text=:text:, anons=:anons: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
			$query_pos = $this->conn->newStatement("SELECT MAX(pos)+1 FROM room");
			$pos = (int)$query_pos->getOneValue();
			if(!$pos){
				$pos = 1;
			}
					
			$query = $this->conn->newStatement("INSERT INTO room SET name=:name:, active=:active:, pos=:pos:, id_category=:id_category:, seats=:seats:, apartment=:apartment:, area=:area:, price=:price:, ext=:ext:, text=:text:, main=:main:, anons=:anons:");
			$query->setInteger('active', 1);
			$query->setInteger('main', 0);
			$query->setInteger("pos", $pos);

		}
		
		$query->setVarChar('name', $this->formData['name']);
		$query->setInteger('id_category', $this->formData['id_category']);
		$query->setInteger('seats', $this->formData['seats']);
		$query->setInteger('apartment', $this->formData['apartment']);
		$query->setFloat('area', $this->formData['area']);
		$query->setInteger('price', $this->formData['price']);
		$query->setText('text', $this->formData['text']);
		$query->setText('anons', $this->formData['anons']);
		$image = $this->request->getValue("image");
		if ($image['tmp_name']){ // Если картинку ЗАГРУЖАЮТ
			
			// расширение
			$image_pieces = explode(".", $image['name']);
			$image_type = $image_pieces[count($image_pieces)-1];
			$query->setVarChar('ext', $image_type);
			$query->execute();
			
			if (!empty($id) && $action == "edit"){
				$id_new = $id;
			}
			else{
				$id_new = $query->getInsertId();
			}
			
			ImageUtil::uploadImage($image['tmp_name'], "uploaded/room/",  "{$id_new}_tmp.{$image_type}");
			$tmp_img = new ImageUtil("uploaded/room/{$id_new}_tmp.{$image_type}");
			
			$image_width = $tmp_img->getWidth();
			$image_height = $tmp_img->getHeight();
			
			if($image_width > $image_height){ // horizontal
				$img_position = 1;
			}
			elseif($image_width < $image_height){ // vertical
				$img_position = 2;
			}
			elseif($image_width == $image_height){ // square
				$img_position = 3;
			}
				
			// создание большой картинки 
			if($image_width > 1024 AND ($img_position==1 OR $img_position==3) ){ // широкая
				$tmp_img->resizeProportionally("uploaded/room/{$id_new}.{$image_type}", 1024, 1);
			}
			elseif ($image_height > 800 AND $img_position==2) { // высокая
				$tmp_img->resizeProportionallyHeight("uploaded/room/{$id_new}.{$image_type}", 800, 1);
			}
			else{
				copy("uploaded/room/{$id_new}_tmp.{$image_type}", "uploaded/room/{$id_new}.{$image_type}");
			}
				
			//  РЕСАЙЗИТЬ по ОПРЕДЕЛЕННЫМ РАЗМЕРАМ
			if($img_position==1 OR $img_position==3){  // гориз.
				$tmp_img->ResizeFromRaf(105, 75, "uploaded/room/{$id_new}_sm.{$image_type}");
				$tmp_img->ResizeFromRaf(275, 195, "uploaded/room/{$id_new}_prev.{$image_type}");
				$tmp_img->ResizeFromRaf(435, 315, "uploaded/room/{$id_new}_big.{$image_type}");
				$tmp_img->ResizeFromRaf(620, 400, "uploaded/room/{$id_new}_banner.{$image_type}");
			}
			else{ // вертик.
				$tmp_img->ResizeFromRaf(56, 75, "uploaded/room/{$id_new}_sm.{$image_type}");
				$tmp_img->ResizeFromRaf(138, 195, "uploaded/room/{$id_new}_prev.{$image_type}");
				$tmp_img->ResizeFromRaf(228, 315, "uploaded/room/{$id_new}_big.{$image_type}");
				$tmp_img->ResizeFromRaf(620, 400, "uploaded/room/{$id_new}_banner.{$image_type}");
			}
			
			FileSystem::deleteFile("uploaded/room/{$id_new}_tmp.{$image_type}");
			
		}
		else{ // Если картинку не загружают
			$query->setVarChar('ext', $this->formData['ext']?$this->formData['ext']:NULL);
			$query->execute();
			if (!$id)
				$id_room = $query->getInsertId();
		}
		if ($id) { 
			$query = $this->conn->newStatement("DELETE FROM attribute_room WHERE id_room=:id_room:");
			$query->setInteger('id_room', $id_new);
			$query->execute();
		}
		
		if ($this->formData['attribute']) {
			foreach ($this->formData['attribute'] as $key=>$value) {
				$query = $this->conn->newStatement("INSERT INTO attribute_room SET id_room=:id_room:, id_attribute=:id_attribute:");
				$query->setInteger('id_room', $id_room);
				$query->setInteger("id_attribute", $key);
				$query->execute();
			}
		}
	        

		$this->response->redirect("/admin/room/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_room_add.tpl"));
	}
	
	
	function doValidation(){		
		$rules = array(
			new EmptyFieldRule("name", 'Название'),
			new EmptyFieldRule("id_category", 'Категория номера'),
			new EmptyFieldRule("seats", 'Количество мест'),
			new EmptyFieldRule("apartment", 'Количество комнат'),
			new EmptyFieldRule("area", 'Площадь'),
			new EmptyFieldRule("price", 'Цена'),
			new EmptyFieldRule("anons", 'Краткое описание'),
			new EmptyFieldRule("text", 'Описание'),
			new EmptyFieldRule("image", 'Изображение'),
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM room WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/room/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_prev.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_big.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}_banner.{$data['ext']}");
		FileSystem::deleteFile("uploaded/room/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE room SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/room/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>