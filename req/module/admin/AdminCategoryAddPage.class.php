<?php
require_once("module/FormPageModule.class.php");
require_once("validator/Validator.class.php");

class AdminCategoryAddPage extends FormPageModule {	
	
	var $page;
	var $get_param;
	
	public function doBeforeOutput() {
		$this->Authenticate();
		
		$this->registerThis("deleteImage");
		$this->processRequest();
		
		$this->template->assign("tinymce", 1);
		
		if($GLOBALS[_SERVER][QUERY_STRING]){
			$this->get_param = "?".$GLOBALS[_SERVER][QUERY_STRING];
		}
		$this->template->assign('get_param', $this->get_param);
		
		$this->page = $this->request->getValue('page')?$this->request->getValue('page'):1;
		$this->template->assign('page', $this->page);
		$this->template->assign('unit', 'room');
		$this->template->assign('subunit', 'category');
	}
	
	
	public function doFormInit(){
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");
		
		if (!empty($id) AND $action == "edit"){
			$conn =& DbFactory::getConnection();
			$query = $conn->newStatement("SELECT * FROM category WHERE id=:id:");
			$query->setInteger('id', $id);
			$data = $query->getFirstRecord();

			$this->template->assign('data', $data);
		}
		$this->response->write($this->renderTemplate('admin/admin_category_add.tpl'));
	}
	
	
	public function doFormValid() {
		$action = $this->request->getValue("action");
		$id = $this->request->getValue("id");

			
		$conn = &DbFactory::getConnection();
		
        	
		if (!empty($id) && $action == "edit"){
			$query = $conn->newStatement("UPDATE category SET name=:name: WHERE id=:id:");
	        $query->setInteger('id', $id);
		}
		else{
			$query_pos = $this->conn->newStatement("SELECT MAX(pos)+1 FROM room");
			$pos = (int)$query_pos->getOneValue();
			if(!$pos){
				$pos = 1;
			}
			$query = $conn->newStatement("INSERT INTO category SET name=:name:, pos=:pos:");
			$query->setInteger('pos', $pos);
		}
		
		$query->setVarChar('name', $this->formData['name']);
		
		$query->execute();
			
		
		$this->response->redirect("/admin/category/list/{$this->page}/".($this->get_param?$this->get_param:"") );
	}
	
	
	public function doFormInvalid(){
		$this->template->assign('data', $this->formData);
		$this->response->write($this->renderTemplate("admin/admin_category_add.tpl"));
	}
	
	
	function doValidation(){
		$rules = array(
			new EmptyFieldRule("name", 'Название')
		);
		
		$validator = new Validator($this->formData);
		
		if (!$validator->validate($rules)) {
			$this->template->assign('errors', $validator->getErrorList());
			return false;
		}
		else return true;
	}
	
	
	
	//*** DEVELOPER AJAX ***//
	
	// Удалить картинку из выбранного элемента
	function deleteImage($id){
		$xajax = new xajaxResponse();
		
		$conn = &DbFactory::getConnection();
		$query = $conn->newStatement("SELECT * FROM category WHERE id={$id}");
		$data = $query->getFirstRecord();
		
		FileSystem::deleteFile("uploaded/category/{$id}_sm.{$data['ext']}");
		FileSystem::deleteFile("uploaded/category/{$id}.{$data['ext']}");
		
		$query = $conn->newStatement("UPDATE category SET ext=NULL WHERE id={$id}");
		$query->execute();
		
		$xajax->remove("photo");
		//$xajax->redirect("/admin/category_gallery/add/{$page}/edit/{$id}/".($get_param?$get_param:"") );
		
		return $xajax;
	}
	
}

?>