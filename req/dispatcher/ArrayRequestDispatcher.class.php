<?php
require_once('dispatcher/RequestDispatcher.class.php');

class ArrayRequestDispatcher extends RequestDispatcher {

	var $resourceMap;
	var $httpResponse;
	var $subdomain = null;

	function ArrayRequestDispatcher(&$httpRequest,&$httpResponse){
		$this->httpResponse =& $httpResponse;
		parent::RequestDispatcher($httpRequest);				
		$this->resourceMap = $this->_defineResourceMap();
	}
	
	function _defineResourceMap() {
		return array(
			'/' 		=> 		array('moduleName' => 'IndexPage', 'moduleType' => 'module'),
			'/404/' 	=> array('moduleName' => 'common/404error.tpl', 'moduleType' => 'template'),
			'/index_ie/'=> array('moduleName' => 'common/index_ie.tpl', 'moduleType' => 'template'), // ie6, ie7 заглушка
			'/kcaptcha/'=> array('moduleName' => 'common/Captcha', 'moduleType' => 'module'), // капча
			
			// PAGES CLASS
			'/about/' 		=> array('moduleName' => 'PagesPage', 'moduleType' => 'module', 'id' => '2'),
			'/service/' 		=> array('moduleName' => 'PagesPage', 'moduleType' => 'module', 'id' => '3'),
			'/residence_rules/' 		=> array('moduleName' => 'PagesPage', 'moduleType' => 'module', 'id' => '4'),
			'/contract_offer/' 		=> array('moduleName' => 'PagesPage', 'moduleType' => 'module', 'id' => '5'),
			'/fire_safety_regulations/' 		=> array('moduleName' => 'PagesPage', 'moduleType' => 'module', 'id' => '6'),
			// PAGES CLASS
			
			'/content/(.+?)/'	=> array('moduleName' => 'ContentPage', 'moduleType' => 'module', 'url'=>'$1'),
			'/contacts/' 		=> array('moduleName' => 'ContactsPage', 'moduleType' => 'module'),			
			
			'/login/'			 =>	array('moduleName' => 'user/LoginPage', 'moduleType' => 'module'),
			'/logout/'			 =>	array('moduleName' => 'user/LogoutPage', 'moduleType' => 'module'),
			'/registration/' 	 => array('moduleName'  => 'user/RegistrationPage', 'moduleType' => 'module'),
			'/success_registration/' => array('moduleName' => 'user/SuccessRegistrationPage', 'moduleType' => 'module'),
			'/activate/' 		 => array('moduleName'  => 'user/ActivatePage', 'moduleType' => 'module'),
			'/restore_password/' => array('moduleName' => 'user/RestorePasswordPage', 'moduleType' => 'module'),
			'/user/(\d+)/'       => array('moduleName' => 'user/UserPage', 'moduleType' => 'module', 'id' => '$1'),
			'/user_info/(\d+)/'  => array('moduleName' => 'user/UserInfoPage', 'moduleType' => 'module', 'id' => '$1'),
			'/change_password/'  => array('moduleName' => 'user/ChangePasswordPage', 'moduleType' => 'module'),

			'/room/' 			=> array('moduleName' => 'RoomPage', 'moduleType' => 'module'),
			'/room/(\d+)/' 		=> array('moduleName' => 'RoomPage', 'moduleType' => 'module', 'id' => '$1'),


			/* DEVELOPER CODE */
			
			'/book_room/'    => array('moduleName' => 'ajax/BookRoomPage', 'moduleType' => 'module'), 
			'/callback/'    => array('moduleName' => 'ajax/CallbackPage', 'moduleType' => 'module'), 
			'/review_add/'    => array('moduleName' => 'ajax/ReviewAddPage', 'moduleType' => 'module'), 
			'/check_date_book/'    => array('moduleName' => 'ajax/CheckDateBookPage', 'moduleType' => 'module'), 
			
			
			/* CRON */
			'/review/' 			=> array('moduleName' => 'ReviewPage', 'moduleType' => 'module'),
			'/review/page/(\d+)/' => array('moduleName' => 'ReviewPage', 'moduleType' => 'module', 'page' => '$1'),
			'/review/(\d+)/' 		=> array('moduleName' => 'ReviewPage', 'moduleType' => 'module', 'id' => '$1'),

			
			
			
			
			/***************	АДМИНКА	*****************/
			'/admin/' 					=> array('moduleName' => 'admin/AdminPage', 'moduleType' => 'module'),
			'/admin/help/' 		=> array('moduleName' => 'admin/AdminHelpPage', 'moduleType' => 'module'),
			
			'/admin/contacts/' 						=> array('moduleName' => 'admin/AdminContactsPage', 'moduleType' => 'module'),			
			
			'/admin/pages/list/'						=> array('moduleName' => 'admin/AdminPagesListPage', 'moduleType' => 'module'),
			'/admin/pages/list/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminPagesListPage', 'moduleType' => 'module', 'action' => 'delete', 'id' => '$1'),
			'/admin/pages/add/'				=> array('moduleName' => 'admin/AdminPagesAddPage', 'moduleType' => 'module'),
			'/admin/pages/add/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminPagesAddPage', 'moduleType' => 'module', 'action' => 'edit', 'id' => '$1'),
			'/admin/pages_photo/add/(\d+)/'		=> array('moduleName' => 'admin/AdminPagesPhotoAddPage', 'moduleType' => 'module', 'id' => '$1'),
			
			/* DEVELOPER CODE  */
			
			/*Список аттрибутов номера*/
			'/admin/attribute/list/'						=> array('moduleName' => 'admin/AdminAttributeListPage', 'moduleType' => 'module'),
			'/admin/attribute/list/(\d+)/'				=> array('moduleName' => 'admin/AdminAttributeListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/attribute/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminAttributeListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/attribute/add/'						=> array('moduleName' => 'admin/AdminAttributeAddPage', 'moduleType' => 'module'),
			'/admin/attribute/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminAttributeAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),
			
			/*Список аттрибутов номера*/
			'/admin/category/list/'						=> array('moduleName' => 'admin/AdminCategoryListPage', 'moduleType' => 'module'),
			'/admin/category/list/(\d+)/'				=> array('moduleName' => 'admin/AdminCategoryListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/category/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminCategoryListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/category/add/'						=> array('moduleName' => 'admin/AdminCategoryAddPage', 'moduleType' => 'module'),
			'/admin/category/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminCategoryAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),
			
			/*список номеров*/
			'/admin/room/list/'						=> array('moduleName' => 'admin/AdminRoomListPage', 'moduleType' => 'module'),
			'/admin/room/list/(\d+)/'				=> array('moduleName' => 'admin/AdminRoomListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/room/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminRoomListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),			
			'/admin/room/add/'						=> array('moduleName' => 'admin/AdminRoomAddPage', 'moduleType' => 'module'),
			'/admin/room/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminRoomAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),			
			'/admin/room_photo/add/(\d+)/(\d+)/'		=> array('moduleName' => 'admin/AdminRoomPhotoAddPage', 'moduleType' => 'module', 'page' => '$1', 'id' => '$2'),
			
			/*Информация о сайте*/
			'/admin/info/list/'						=> array('moduleName' => 'admin/AdminInfoListPage', 'moduleType' => 'module'),
			'/admin/info/list/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminInfoListPage', 'moduleType' => 'module', 'action' => 'delete', 'id' => '$1'),
			'/admin/info/add/'						=> array('moduleName' => 'admin/AdminInfoAddPage', 'moduleType' => 'module'),
			'/admin/info/add/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminInfoAddPage', 'moduleType' => 'module', 'action' => 'edit', 'id' => '$1'),
			
			/*Бронирование номеров*/
			'/admin/book_room/add/'				    	=> array('moduleName' => 'admin/AdminBookRoomAddPage', 'moduleType' => 'module'),
			'/admin/book_room/add/(\d+)/edit/(\d+)/'	=> array('moduleName' => 'admin/AdminBookRoomAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),
			'/admin/book_room/list/'					=> array('moduleName' => 'admin/AdminBookRoomListPage', 'moduleType' => 'module'),
			'/admin/book_room/list/(\d+)/'				=> array('moduleName' => 'admin/AdminBookRoomListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/book_room/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminBookRoomListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			
			/*Заявки на звонок*/
			'/admin/callback/add/'				    	=> array('moduleName' => 'admin/AdminCallbackAddPage', 'moduleType' => 'module'),
			'/admin/callback/add/(\d+)/edit/(\d+)/'	=> array('moduleName' => 'admin/AdminCallbackAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),
			'/admin/callback/list/'					=> array('moduleName' => 'admin/AdminCallbackListPage', 'moduleType' => 'module'),
			'/admin/callback/list/(\d+)/'				=> array('moduleName' => 'admin/AdminCallbackListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/callback/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminCallbackListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			
			/*Акции*/
			'/admin/stock/list/'						=> array('moduleName' => 'admin/AdminStockListPage', 'moduleType' => 'module'),
			'/admin/stock/list/(\d+)/'				=> array('moduleName' => 'admin/AdminStockListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/stock/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminStockListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/stock/add/'						=> array('moduleName' => 'admin/AdminStockAddPage', 'moduleType' => 'module'),
			'/admin/stock/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminStockAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),

			/*Доп. услуги*/
			'/admin/service/list/'						=> array('moduleName' => 'admin/AdminServiceListPage', 'moduleType' => 'module'),
			'/admin/service/list/(\d+)/'				=> array('moduleName' => 'admin/AdminServiceListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/service/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminServiceListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/service/add/'						=> array('moduleName' => 'admin/AdminServiceAddPage', 'moduleType' => 'module'),
			'/admin/service/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminServiceAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),

			/*Отзывы*/
			'/admin/review/list/'						=> array('moduleName' => 'admin/AdminReviewListPage', 'moduleType' => 'module'),
			'/admin/review/list/(\d+)/'				=> array('moduleName' => 'admin/AdminReviewListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/review/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminReviewListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/review/add/'						=> array('moduleName' => 'admin/AdminReviewAddPage', 'moduleType' => 'module'),
			'/admin/review/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminReviewAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),
			
			/*Коды для продвижения*/
			'/admin/seo/list/'						=> array('moduleName' => 'admin/AdminSeoListPage', 'moduleType' => 'module'),
			'/admin/seo/list/(\d+)/'				=> array('moduleName' => 'admin/AdminSeoListPage', 'moduleType' => 'module', 'page' => '$1'),
			'/admin/seo/list/(\d+)/delete/(\d+)/'	=> array('moduleName' => 'admin/AdminSeoListPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'delete', 'id' => '$2'),
			'/admin/seo/add/'						=> array('moduleName' => 'admin/AdminSeoAddPage', 'moduleType' => 'module'),
			'/admin/seo/add/(\d+)/edit/(\d+)/'		=> array('moduleName' => 'admin/AdminSeoAddPage', 'moduleType' => 'module', 'page' => '$1', 'action' => 'edit', 'id' => '$2'),

		);
	}

	function getResoursePath($path){
		foreach ($this->resourceMap as $url => $resource) {  		
			if (preg_match("~^{$url}$~u", $path, $matches)) {
				return array(
					'resource' => $resource,
					'matches' => $matches
				);
			}
		}
		return false;
	}

	function dispatch(){
		$url = $this->httpRequest->url->getPath();
		if (strlen($url) > 0) {
			if ((substr($url, strlen($url) - 1)) != '/' ) {
				$url .= '/';
			}
		}

	    $resourceInfo = $this->getRequestedResource($url);

		if ($resourceInfo === false) {
			$resource = array('moduleName' => 'common/404error.tpl', 'moduleType' => 'template');
			$matches = array();
		} 
		else {
			$resource = $resourceInfo['resource'];
			$matches = $resourceInfo['matches'];
		}

		foreach ($resource as $key => $value) {
			foreach ($matches as $num => $replaceValue) {
				$replaceValue = urldecode($replaceValue);
				$value = str_replace('$' . $num, $replaceValue, $value);
			}
			$this->httpRequest->setValue($key, $value);
		}
		// run module

		switch ($resource['moduleType']) {
			case 'module':
				$moduleName = $resource['moduleName'];
				$this->includeModule($moduleName);
				$moduleClass = end(explode('/', $moduleName));
		
				$module = new $moduleClass($this->httpRequest, $this->httpResponse);
				return $module;
				// check access
		       break;
			case 'template':
				//To my mind, should be this one
				include_once("templatemodule/TemplateRunner.class.php");
				$template = new TemplateRunner($resource['moduleName'], $this->httpRequest, $this->httpResponse);
		  		return $template;
		}
	}

	function handleError404() {
		$this->httpResponse->redirect("/404/");
	}

	function includeModule($moduleName) {
		include_once("module/{$moduleName}.class.php");
	}

	function getRequestedResource($path) {
		// parse url here and return page modules
		// $path = $this->httpRequest->url->getPath();
		$mod_array = $this->getResoursePath($path);
		if (isset($mod_array)) {
			return  $mod_array;
		}
		else {
			return array('moduleName' => 'common/404error.tpl', 'moduleType' => 'template'); // 404 error
		}
	}	

	function handleAccessError() {
		$this->httpResponse->redirect("/");
	}
}

?>