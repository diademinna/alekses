'use strict';



function initPreview(context) {
  if (typeof Preview === 'undefined' || !jQuery.isFunction(Preview)) {
    return false;
  }

  var common = {
    duration: 1800,
    easing: 'easeOutCubic',
    trigger: 'jsheaderanimated',
    offsetScroll: 80
  };

  jQuery('.JS-Preview', context || document).not('.JS-Preview-ready').each(function() {
    new Preview(this, jQuery.extend({}, common, GLOBAL.parseData(jQuery(this).data('preview-params'))));
  });
}

function initHeader(context) {
  if (!jQuery('.JS-Header').length) {
    return;
  }

  var common = {
    cssAnimated: 'slideInDown animated'
  };

  jQuery('.JS-Header', context || document).not('.JS-Header-ready').each(function() {
    var $element = jQuery(this).addClass('JS-Header-ready'),
        params = jQuery.extend({}, common, GLOBAL.parseData($element.data('header-params')));

    jQuery(window).on('jsheaderanimated', function() {
      $element
        .css('display', 'block')
        .addClass(params.cssAnimated);
    });

  });
}

function initValidate(context) {
  if (!jQuery('.JS-Validate').length) {
    return false;
  }
  
  var common = {
    cssError: 'gui-error',
    cssSuccess: 'gui-success',
    type: 'POST',
    messageError: 'Произошла ошибка, проверьте содержимое формы.',
    messageSucces: 'Заявка принята.',
    submit: false,
  };
  
  jQuery('.JS-Validate', context || document).not('.JS-Validate-ready').each(function() {
    var $element = jQuery(this).addClass('JS-Validate-ready'),
        params = jQuery.extend({}, common, GLOBAL.parseData($element.data('validate-params')));

    $element.validate({
       debug: true,
       errorClass: params.cssError,
       validClass: params.cssSuccess,
       ignore: '',
       errorElement: "span",
       highlight: function(element, errorClass, validClass){ //если ошибка
          if (jQuery(element).is('input')) {
            jQuery(element).closest(".gui-field").addClass(errorClass).removeClass(validClass);
          } else {
            jQuery(element).parent().addClass(errorClass).removeClass(validClass);
          }
       },
       unhighlight: function(element, errorClass, validClass){ //если все успешно
         if (jQuery(element).is('input')) {
           jQuery(element).closest(".gui-field").removeClass(errorClass).addClass(validClass);
         } else {
            jQuery(element).parent().removeClass(errorClass).addClass(validClass);
         }
       },
      //  errorPlacement: function(error, element) { /* куда разместить сообщение для поля ввода*/ },
       rules: params.rules,
       messages: params.messages,
       submitHandler: function(form){ 
         if (params.submit) {
            form.submit();
         } else {
            formSubmit($element, params); 
         }
       }
     });
  });
}
/////
function formSubmit($form, params) {
  var data = $form.serialize(),
      $popup = jQuery(params.idPopupSuccess);

  jQuery.ajax({
    type: params.type,
    url: params.url,
    data: data,
    success: function(options){
    //  if (options.status === 'success') {
        openMagnificPopup($popup, 'block');
    //    return;
    //  }
    },
    error: function(options) {
      GLOBAL.showConnectError();
    }
  });
}
function initMagnificPopup(context) {
    if (!jQuery('.JS-Popup').length ||  typeof jQuery.fn.magnificPopup === 'undefined') {
        return false;
    }
    jQuery('.JS-Popup').magnificPopup();
}

function openMagnificPopup($popup, mode) {
  var params = {
    items: {
      src: $popup
    }
  };

  $.magnificPopup.open(jQuery.extend({}, params, GLOBAL.popup[mode]));
}
function initTime() {
  var $time = jQuery('#time'),
  time = {
  hour: parseInt($time.text().split(':')[0]),
  minute: parseInt($time.text().split(':')[1]),
  counting: function() {
    if (this.minute === 59) {
      this.minute = 0;

      if (this.hour === 23) {
        this.hour = 0;
      } 
      else {
        this.hour++;
      }
    } 
    else {
      this.minute++;
    }
  },
  set: function() {
    var time = '';

    time += (this.hour + '').length === 1 ? '0' + this.hour : this.hour;
    time += ':';
    time += (this.minute + '').length === 1 ? '0' + this.minute : this.minute;

    $time.text(time);
    }
  };

  setInterval(function() {
    time.counting();
    time.set();
  }, 60000);
}


function initDatepicker(context) {
  if (typeof jQuery.fn.royalSlider === 'undefined' || !jQuery('.JS-Datepicker').length) {
    return false;
  }
  $("#date_input, #date_output").datepicker();
}
function initRoomParams(context) {
  var name_room = "";
  var price_room = "";
  var id_room = "";
  $(".book-room").click(function(){
      name_room = $('.l-content').find('input[name="name_room_select"]').val();
      price_room = $('.l-content').find('input[name="price_room_select"]').val();
      id_room = parseInt($('.l-content').find('input[name="id_room_select"]').val());
    
      $("#popup-book").find('input[name="name_room"]').val(name_room);
      $("#popup-book").find('input[name="price_room"]').val(price_room);
      $("#popup-book").find('input[name="id_room"]').val(id_room);
  });
}
function initRoyalSlider(context) {
  if (typeof jQuery.fn.royalSlider === 'undefined' || !jQuery('.JS-RoyalSlider').length) {
    return false;
  }

  var common = {
    fullscreen: {
      enabled: true,
      nativeFS: true
    },
    controlNavigation: 'thumbnails',
    autoScaleSlider: true, 
    autoScaleSliderWidth: 455,     
    autoScaleSliderHeight: 444,
    imgWidth: 435,
    imgHeight: 315,
    autoCenter:false,
    loop: false,
    imageScaleMode: 'fit-if-smaller',
    navigateByClick: true,
    numImagesToPreload:2,
    arrowsNav:true,
    arrowsNavAutoHide: false,
    arrowsNavHideOnTouch: true,
    keyboardNavEnabled: true,
    fadeinLoadedSlide: true,
    
    thumbs: {
      appendSpan: true,
      firstMargin: true,
      paddingBottom: 8,
      spacing:8
    }
  };

  jQuery('.JS-RoyalSlider', context || document).not('.JS-RoyalSlider-ready').each(function() {
    var $element = jQuery(this).addClass('JS-RoyalSlider-ready'),
        params = GLOBAL.parseData($element.data('royalslider-params'));

    $element.royalSlider(jQuery.extend({}, common, params));
  });
}
function initScrolling(context) {
  if (!jQuery('.JS-Scrolling').length) {
    return false;
  }
  var common = {};

  jQuery('.JS-Scrolling', context || document).not('.JS-Scrolling-ready').each(function() {
    new Scrolling(this, jQuery.extend({}, common, GLOBAL.parseData(jQuery(this).data('scrolling'))));
  });
}

function initWeather(context) {
  if (typeof Weather === 'undefined' || !jQuery.isFunction(Weather)) {
    return false;
  }

  var common = {};

  jQuery('.JS-Weather', context || document).not('.JS-Weather-ready').each(function() {
    new Weather(this, jQuery.extend({}, common, GLOBAL.parseData(jQuery(this).data('weather-params'))));
  });
}

function initRangeDatepikerRoom(context) {
  if (typeof RangeDatepikerRoom === 'undefined' || !jQuery.isFunction(RangeDatepikerRoom)) {
    return false;
  }

  var common = {};

  jQuery('.JS-RangeDatepikerRoom', context || document).not('.JS-RangeDatepikerRoom-ready').each(function() {
    new RangeDatepikerRoom(this, jQuery.extend({}, common, GLOBAL.parseData(jQuery(this).data('rangedatepikerroom-params'))));
  });
}

jQuery(function() {
  if (typeof GLOBAL === 'undefined') {
    return false;
  }
  initTime();
  initHeader();
  // initPreview();
  initValidate();
  initRoyalSlider();
  initMagnificPopup();
  initDatepicker();
  initRoomParams();
  initScrolling();
  initWeather();
  // initRangeDatepikerRoom();
});
