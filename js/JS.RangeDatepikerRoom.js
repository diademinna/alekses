!function(namespace) {
  'use strict';

  function RangeDatepikerRoom(elem, params) {
    this.$element = jQuery(elem);
    this.params = params || {};

    this.cssReadyElement = this.params.cssReadyElement || 'JS-RangeDatepikerRoom-ready';
    this.cssSelectedElement = this.params.cssSelectedElement || 'JS-RangeDatepikerRoom-selected';

    this.url = this.params.url || '';
    this.type = this.params.type || 'POST';
    this.options = this.params.options || {
      dateFormat: 'dd-mm-yy',
      changeMonth: true,
      changeYear: true,
      minDate: new Date()
    };

    this.__construct();
  }

  RangeDatepikerRoom.prototype.__construct = function __construct() {
    this.$start = this.$element.find('.JS-RangeDatepikerRoom-Start');
    this.$end = this.$element.find('.JS-RangeDatepikerRoom-End');
    this.$room = this.$element.find('.JS-RangeDatepikerRoom-Room');
    this.$clear = this.$element.find('.JS-RangeDatepikerRoom-Clear');

    this.bookDate = [];

    this._init();
  };

  RangeDatepikerRoom.prototype._init = function _init() {
    var _this = this;

    this._initDatepiker();
    // this._updateBookDate();

    if (this.$room.is('select')) {
      this.$room.on('change.JS-RangeDatepikerRoom', function() {
        _this._updateBookDate.apply(_this, []);
      });
    }

    this.$clear.on('click.JS-RangeDatepikerRoom', function(event) {
      GLOBAL.stopEvent(event);
      _this._clear.apply(_this, []);
    });

    this._ready();
  };

  RangeDatepikerRoom.prototype._initDatepiker = function _initDatepiker() {
    this.$start.datepicker(jQuery.extend({}, this.options, {
      // onSelect: this._startOnSelect.bind(this),
      // beforeShowDay: this._beforeShowDay.bind(this)
    }));

    this.$end.datepicker(jQuery.extend({}, this.options, {
      // onSelect: this._endOnSelect.bind(this),
      // beforeShowDay: this._beforeShowDay.bind(this)
    }));
  };

  RangeDatepikerRoom.prototype._ready = function _ready() {
    this.$element
      .addClass(this.cssReadyElement)
      .addClass('JS-RangeDatepikerRoom-ready');
  };

  RangeDatepikerRoom.prototype._updateBookDate = function _updateBookDate() {
    var _this = this;

    jQuery.ajax({
      type: this.type,
      url: this.url,
      dataType: "json",
      data: {
        id_room: this.$room.val()
      },
      success: function(dataSuccess) {
        _this.bookDate = dataSuccess.data || [];

        _this._clear.apply(_this, []);
      },
      error: function(xhr, status, error) {
        _this.bookDate = [];
      }
    });
  };

  RangeDatepikerRoom.prototype._startOnSelect = function _startOnSelect() {
    var date = this.$start.val();

    this.$end.datepicker("option", {
      'minDate': date,
      'maxDate': this._getMaxDateBook(date)
    });

    this._rangeSelected();
  };

  RangeDatepikerRoom.prototype._endOnSelect = function _endOnSelect() {
    var date = this.$end.val();

    this.$start.datepicker("option", {
      'maxDate': date,
      'minDate': this._getMinDateBook(date)
    });

    this._rangeSelected();
  };

  RangeDatepikerRoom.prototype._beforeShowDay = function _beforeShowDay(date) {
    if (this.bookDate.length) { 
        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);

        if (this.bookDate.indexOf(string) === -1) {
            return [true];
        } else {
            return [false, 'book'];
        }
    } else {
        return [true];
    }
  };

  RangeDatepikerRoom.prototype._getMaxDateBook = function _getMaxDateBook(date) {
    var dateUTC = new Date(date.split('-')[2] + '-' + date.split('-')[1] + '-' + date.split('-')[0]).valueOf();

    for (var i = 0; i < this.bookDate.length; i++) {
      var item = new Date(this.bookDate[i]);

      if (item.valueOf() > dateUTC) {
        item = item.setDate(item.getDate() - 1);
        return new Date(item).toLocaleDateString().replace(/\./g, '-');
      }
    }

    return null;
  };

  RangeDatepikerRoom.prototype._getMinDateBook = function _getMinDateBook(date) {
    var dateUTC = new Date(date.split('-')[2] + '-' + date.split('-')[1] + '-' + date.split('-')[0]).valueOf();

    for (var i = this.bookDate.length; i > -1; i--) {
      var item = new Date(this.bookDate[i]);

      if (item.valueOf() < dateUTC) {
        item = item.setDate(item.getDate() + 1);
        return new Date(item).toLocaleDateString().replace(/\./g, '-');
      }
    }

    return null;
  };

  RangeDatepikerRoom.prototype._rangeSelected = function _rangeSelected() {
    if (!this.$start.val() || !this.$end.val()) {
      return;
    }

    this.$element.addClass(this.cssSelectedElement);
  };

  RangeDatepikerRoom.prototype._clear = function _clear() {
    var option = {
      'minDate': new Date(),
      'maxDate': null
    };

    this.$start.datepicker("setDate", null);
    this.$end.datepicker("setDate", null);
    this.$start.datepicker("option", option);
    this.$end.datepicker("option", option);

    this.$element.removeClass(this.cssSelectedElement);
  };

  namespace.RangeDatepikerRoom = RangeDatepikerRoom;
}(this);