!function(namespace) {
  'use strict';

  function Weather(elem, params) {
    this.$element = jQuery(elem);
    this.params = params || {};

    this.cssReadyElement = this.params.cssReadyElement || 'JS-Weather-ready';

    this.srcImages = this.params.srcImages || '/images/icons/weather/';
    this.formatImages = this.params.formatImages || '.png';
    this.url = this.params.url || 'http://api.openweathermap.org/data/2.5/forecast/city';
    this.type = this.params.type || 'GET';
    this.options = this.params.options || {
      id: 519560,
      units: 'metric',
      lang: 'ru',
      APPID: '500b04a6f23ed8539dcae56f0bffef1f'
    };

    this.__construct();
  }

  Weather.prototype.__construct = function __construct() {
    this.$temp = this.$element.find('.JS-Weather-Temp');
    this.$image = this.$element.find('.JS-Weather-Image');

    this._init();
  };

  Weather.prototype._init = function _init() {
    var _this = this;

    this._send();

    this._ready();
  };

  Weather.prototype._ready = function _ready() {
    this.$element
      .addClass(this.cssReadyElement)
      .addClass('JS-Weather-ready');
  };

  Weather.prototype._send = function _send() {
    var _this = this;

    jQuery.ajax({
      type: this.type,
      url: this.url,
      data: this.options,
      success: function(responce) {
        _this._set.apply(_this, [responce]);
      },
      error: function(xhr, status, error) {
        console.info('Ошибка запроса погоды');
      }
    });
  };

  Weather.prototype._set = function _set(responce) {
    var curMain = responce.list[0].main,
        weather = responce.list[0].weather[0];

    this.$temp.text(Math.round(curMain.temp));
    this.$image.attr({
      src: this.srcImages + weather.icon + this.formatImages,
      alt: weather.description,
      title: weather.description
    });
  };

  namespace.Weather = Weather;
}(this);