!function(namespace) {
  'use strict';

  function Scrolling(elem, params) {
    this.$element = jQuery(elem);
    this.params = params || {};

    this.cssReadyElement = this.params.cssReadyElement || 'JS-Scrolling-ready';
    this.margin = parseInt(this.params.margin) || 30;
    this.duration = this.params.duration || 600;
    this.loadScroll = this.params.loadScroll || false;

    this.__construct();
  }

    Scrolling.prototype.__construct = function __construct() {
      this.$window = jQuery(window);
      this.$blocks = this.$element.find('.JS-Scrolling-Block');
      this.$items = this.$element.find('.JS-Scrolling-Item');
      
      this._init();
    };

    Scrolling.prototype._init = function _init() {
      var _this = this;
      this.$window.on('load' + '.JS-Scrolling', function(event) {
        if (_this.loadScroll == 'false') {
          return;
        }

        _this._preparation.apply(_this, []);
      });

      this.$window.on('jsscrolling' + '.JS-Scrolling', function(event, hash) {
        _this._preparation.apply(_this, [hash]);
      });

      this.$items.on('click' + '.JS-Scrolling', function(event) {
        _this._preparation.apply(_this, [jQuery(this).attr('href')]);
      });

      this._ready();
    };

    Scrolling.prototype._ready = function _ready() {
      this.$element
        .addClass(this.cssReadyElement)
        .addClass('JS-Scrolling-ready');
    };

    Scrolling.prototype._preparation = function _preparation(hash) {
      if (!hash) {
        hash = window.location.hash;
      }

      var offsetTop = 0,
          $block;

      if (hash != '') {
        hash = this._removeAdditionalParams(hash);
        hash = hash.substring(1, hash.length);
        $block = this.$blocks.filter('[data-scrolling-id="'+ hash +'"]');
        
        if ($block.length) { 
          this._scroll($block);
        }
      }
    };

    Scrolling.prototype._removeAdditionalParams = function _removeAdditionalParams(str) {
      var index = str.indexOf('?');

      if (index < 0) {
        return str;
      }

      if (str[index - 1] == '/') {
        --index;
      }

      return str.substring(0, index);
    };
    
    Scrolling.prototype._scroll = function _scroll($source) {
      var offsetTop = $source.offset().top - this.margin;
      jQuery('html, body').stop().animate({scrollTop: offsetTop}, this.duration);
    };

  namespace.Scrolling = Scrolling;
}(this);
