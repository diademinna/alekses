<?php /* Smarty version Smarty3-b7, created on 2016-10-27 10:43:41
         compiled from ".\templates\admin/admin_room_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:100935811b02d3095b5-22446328%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d66d562723b9a797aead7a722e56b72c5c02a8e' => 
    array (
      0 => '.\\templates\\admin/admin_room_add.tpl',
      1 => 1477554186,
    ),
  ),
  'nocache_hash' => '100935811b02d3095b5-22446328',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
	Номера / <?php if ($_smarty_tpl->getVariable('data')->value['name']){?>Редактировать - <?php echo $_smarty_tpl->getVariable('data')->value['name'];?>
<?php }else{ ?>Добавить<?php }?>
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>


<?php ob_start(); ?>
<div class="ibox-content">
    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <?php $_template = new Smarty_Internal_Template("common/errors_block.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-8">
                <input name="name" class="form-control" type="text" value="<?php echo $_smarty_tpl->getVariable('data')->value['name'];?>
" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Категория* :</label>
            <div class="col-sm-10">
                <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_category')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                    <div class="radio">
                        <input id="category<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="id_category" value="<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" <?php if ($_smarty_tpl->getVariable('cur')->value['id']==$_smarty_tpl->getVariable('data')->value['id_category']){?>checked<?php }?>>
                        <label class="label_radio" for="category<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</label>
                    </div>
                <?php }} ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Количество мест* :</label>
            <div class="col-sm-2">
                <div class="radio">
                    <input id="seats1_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="seats" value="1" <?php if ($_smarty_tpl->getVariable('data')->value['seats']==1){?>checked<?php }?>>
                    <label class="label_radio" for="seats1_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">1 - местный</label>
                </div>
                <div class="radio">
                    <input id="seats2_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="seats" value="2" <?php if ($_smarty_tpl->getVariable('data')->value['seats']==2){?>checked<?php }?>>
                    <label class="label_radio" for="seats2_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">2 - местный</label>
                </div>
                <div class="radio">
                    <input id="seats3_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="seats" value="3" <?php if ($_smarty_tpl->getVariable('data')->value['seats']==3){?>checked<?php }?>>
                    <label class="label_radio" for="seats3_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">3 - местный</label>
                </div>
            </div>
            <label class="col-sm-2 control-label">Количество комнат* :</label>
            <div class="col-sm-2">
                <div class="radio">
                    <input id="apartment1_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="apartment" value="1" <?php if ($_smarty_tpl->getVariable('data')->value['apartment']==1){?>checked<?php }?>>
                    <label class="label_radio" for="apartment1_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">1 - комнатный</label>
                </div>
                <div class="radio">
                    <input id="apartment2_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="apartment" value="2" <?php if ($_smarty_tpl->getVariable('data')->value['apartment']==2){?>checked<?php }?>>
                    <label class="label_radio" for="apartment2_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">2 - комнатный</label>
                </div>
                <div class="radio">
                    <input id="apartment3_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
" type="radio" name="apartment" value="3" <?php if ($_smarty_tpl->getVariable('data')->value['apartment']==3){?>checked<?php }?>>
                    <label class="label_radio" for="apartment3_<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
">3 - комнатный</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Площадь* :</label>
            <div class="col-sm-2 input-group">
                <input type="text" class="form-control" name="area" value="<?php echo $_smarty_tpl->getVariable('data')->value['area'];?>
">
                <div class="input-group-addon">кв. м.</div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Цена* :</label>
            <div class="col-sm-2 input-group">
                <input type="text" class="form-control" name="price" value="<?php echo $_smarty_tpl->getVariable('data')->value['price'];?>
">
                <div class="input-group-addon">руб. / сут.</div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Оснащенность номера :</label>
            <div class="col-sm-8">
                <div class="row">
                    <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_attribute')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
                    <div class="col-xs-6">
                        <div class="checkbox">
                            <input id="attribute<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="checkbox" name="attribute[<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
]" value="<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" <?php ob_start();?><?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_smarty_tpl->getVariable('data_attribute_room')->value[$_tmp1]){?>checked<?php }?> >
                            <label class="label_checkbox" style="padding-left:25px;" for="attribute<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</label>
                        </div>
                    </div>
                    <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']%2==0){?></div><div class="row"><?php }?>
                    <?php }} ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Краткое описание* :</label>
            <div class="col-sm-10">
                <textarea class="tiny" name="anons"><?php echo $_smarty_tpl->getVariable('data')->value['anons'];?>
</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Описание* :</label>
            <div class="col-sm-10">
                <textarea class="tiny" name="text"><?php echo $_smarty_tpl->getVariable('data')->value['text'];?>
</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Изображение :</label>
            <div class="col-sm-10">
                <input  type="file" name="image" />
                <br /><br />
                <div id="photo">
                    <?php if ($_smarty_tpl->getVariable('data')->value['ext']){?>
                        <a href="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
.<?php echo $_smarty_tpl->getVariable('data')->value['ext'];?>
" target="_blank"><img src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
_sm.<?php echo $_smarty_tpl->getVariable('data')->value['ext'];?>
" class="photo" /></a>
                        &nbsp;<i onmouseover="this.style.cursor='pointer';" class="fa fa-times" onclick="if(confirm('Вы уверены?')) xajax_deleteImage('<?php echo $_smarty_tpl->getVariable('data')->value['id'];?>
'); return false;"></i>
                        <input type="hidden" name="ext" value="<?php echo $_smarty_tpl->getVariable('data')->value['ext'];?>
" />
                    <?php }?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>

<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
