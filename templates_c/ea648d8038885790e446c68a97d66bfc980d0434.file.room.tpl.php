<?php /* Smarty version Smarty3-b7, created on 2016-11-16 17:46:53
         compiled from ".\templates\room.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3986582c715d40a1f4-43071170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea648d8038885790e446c68a97d66bfc980d0434' => 
    array (
      0 => '.\\templates\\room.tpl',
      1 => 1479248580,
    ),
  ),
  'nocache_hash' => '3986582c715d40a1f4-43071170',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_replace')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.replace.php';
?><?php ob_start(); ?>
<div class="l-content content">
    <div class="l-layout">
        <?php if ($_smarty_tpl->getVariable('data')->value){?>
        <div class="rooms">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item">Номера и цены</li>
            </ul>
            <h2 class="gui-h2">Номера и цены</h2>
            <div class="rooms-list">
                <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                    <h3 class="gui-h3 rooms-list__subtitle"><?php echo $_smarty_tpl->getVariable('cur')->value['name_category'];?>
</h3>
                        <?php if ($_smarty_tpl->getVariable('cur')->value['rooms']){?>
                        <div class="row">
                            <?php  $_smarty_tpl->tpl_vars['cur2'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('cur')->value['rooms']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop2']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur2']->key => $_smarty_tpl->tpl_vars['cur2']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop2']['iteration']++;
?>
                            <div class="col-xs-4">
                                <div class="room">
                                    <div class="room__content">
                                        <div class="room-name"><a class="room-name__link" href="/room/<?php echo $_smarty_tpl->getVariable('cur2')->value['id'];?>
/"><?php echo $_smarty_tpl->getVariable('cur2')->value['name'];?>
</a></div>
                                        <?php if ($_smarty_tpl->getVariable('cur2')->value['ext']){?><img class="room__image" alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('cur2')->value['id'];?>
_prev.<?php echo $_smarty_tpl->getVariable('cur2')->value['ext'];?>
" /><?php }else{ ?><img class="room__image" src="/images/no-image_prev.png"/><?php }?>
                                        <div class="room__anons"><?php echo $_smarty_tpl->getVariable('cur2')->value['seats'];?>
-местный, <?php echo $_smarty_tpl->getVariable('cur2')->value['apartment'];?>
-комнатный</div>
                                        <div class="room__description"><?php echo $_smarty_tpl->getVariable('cur2')->value['anons'];?>
</div>
                                        <div class="room-price"><span class="room-price__coast"><?php echo $_smarty_tpl->getVariable('cur2')->value['price'];?>
 </span>руб./сут.</div>
                                        <!--<button class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</button>-->
                                        <a class="gui-button " href="/room/<?php echo $_smarty_tpl->getVariable('cur2')->value['id'];?>
/">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop2']['iteration']%3==0){?></div><div class="row"><?php }?>
                            <?php }} ?>
                        </div>
                        <?php }?>
                <?php }} ?>
            </div>
        </div>
        <?php }elseif($_smarty_tpl->getVariable('data_item')->value){?>
        <div class="detailed">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/room/">Номера и цены</a></li>
                <li class="breadcrumbs__item"><?php echo $_smarty_tpl->getVariable('data_item')->value['name'];?>
</li>
            </ul>
            <h2 class="gui-h2 name_room"><?php echo $_smarty_tpl->getVariable('data_item')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('data_item')->value['name'];?>
</h2>
            <div class="detailed-content">
                <input type="hidden" name="name_room_select" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('data_item')->value['name'];?>
">
                <input type="hidden" name="price_room_select" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['price'];?>
">
                <input type="hidden" name="id_room_select" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
">
                <div class="detailed-content__photo">
                    <?php if ($_smarty_tpl->getVariable('data_item')->value['ext']||$_smarty_tpl->getVariable('data_photo')->value){?>
                    <div class="royalSlider main-results__slider rsDefault JS-RoyalSlider">
                        <?php if ($_smarty_tpl->getVariable('data_item')->value['ext']){?>
                        <a class="rsImg" data-rsw="435" data-rsh="315"  data-rsbigimg="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
.<?php echo $_smarty_tpl->getVariable('data_item')->value['ext'];?>
" href="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
_big.<?php echo $_smarty_tpl->getVariable('data_item')->value['ext'];?>
">
                            <img class="rsTmb" src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
_sm.<?php echo $_smarty_tpl->getVariable('data_item')->value['ext'];?>
" height="75" width="105">
                        </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->getVariable('data_photo')->value){?>
                            <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_photo')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <a class="rsImg" data-rsw="<?php echo $_smarty_tpl->getVariable('cur')->value['width_big'];?>
" data-rsh="<?php echo $_smarty_tpl->getVariable('cur')->value['height_big'];?>
"  data-rsbigimg="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" href="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_big.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
">
                                <img class="rsTmb" src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_sm.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" height="<?php echo $_smarty_tpl->getVariable('cur')->value['height_sm'];?>
" width="<?php echo $_smarty_tpl->getVariable('cur')->value['width_sm'];?>
">
                            </a>
                            <?php }} ?>
                        <?php }?>
                    </div>
                    <?php }else{ ?>
                        <img src="/images/no-image_prev.png" />
                    <?php }?>
                </div>
                <div class="detailed-content__description">
                    <?php if ($_smarty_tpl->getVariable('data_item')->value['attribute']){?>
                    <ul class="detailed-attribute">
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_item')->value['attribute']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                        <li class="detailed-attribute__item">
                            <img class="detailed-attribute__image" src="/uploaded/attribute/<?php echo $_smarty_tpl->getVariable('cur')->value['id_attribute'];?>
.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" />
                            <div class="detailed-attribute__text"><?php echo $_smarty_tpl->getVariable('cur')->value['name_attribute'];?>
</div>
                        </li>
                        <?php }} ?>
                    </ul>
                    <?php }?>
                    <div class="detailed-content__seats"><?php echo $_smarty_tpl->getVariable('data_item')->value['seats'];?>
-местный, <?php echo $_smarty_tpl->getVariable('data_item')->value['apartment'];?>
-комнатный</div>
                    <?php if ($_smarty_tpl->getVariable('data_item')->value['area']){?><div class="detailed-content__area">Площадь номера: <?php echo $_smarty_tpl->getVariable('data_item')->value['area'];?>
 кв.м.</div><?php }?>
                    <div class="detailed-content__text"><?php echo $_smarty_tpl->getVariable('data_item')->value['text'];?>
</div>
                    <?php if ($_smarty_tpl->getVariable('data_info')->value[5]['text']){?>
                     <div class="detailed-content__phone">
                        <a class="detailed-content__link" href="tel:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_info')->value[5]['text'],' ','');?>
"><?php echo $_smarty_tpl->getVariable('data_info')->value[5]['text'];?>
</a>
                    </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_info')->value[7]['text']){?>
                    <div class="detailed-content__email">
                        <a class="detailed-content__link" href="mailto:<?php echo $_smarty_tpl->getVariable('data_info')->value[7]['text'];?>
"><?php echo $_smarty_tpl->getVariable('data_info')->value[7]['text'];?>
</a>
                    </div>
                    <?php }?>
                    <div class="detailed-content__actions">
                        <div class="detailed-content__price">
                            <span class="detailed-content__cost"><?php echo $_smarty_tpl->getVariable('data_item')->value['price'];?>
</span> руб./сут.
                        </div>
                        <div class="detailed-content__button"><a class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</a></div>
                    </div>
                </div>
            </div>
                <?php if ($_smarty_tpl->getVariable('data_rooms')->value){?>
                <h2 class="gui-h2">Другие номера</h2>
                <div class="detailed-other">
                    <div class="row">
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_rooms')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                        <div class="col-xs-4">
                            <div class="room">
                                <div class="room__content">
                                    <div class="room-name"><a class="room-name__link" href="/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><?php echo $_smarty_tpl->getVariable('cur')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a></div>
                                    <?php if ($_smarty_tpl->getVariable('cur')->value['ext']){?><img class="room__image" alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_prev.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" /><?php }else{ ?><img class="room__image" src="/images/no-image_prev.png" /><?php }?>
                                    <div class="room__anons"><?php echo $_smarty_tpl->getVariable('cur')->value['seats'];?>
-местный, <?php echo $_smarty_tpl->getVariable('cur')->value['apartment'];?>
-комнатный</div>
                                    <div class="room__description"><?php echo $_smarty_tpl->getVariable('cur')->value['anons'];?>
</div>
                                    <div class="room-price"><span class="room-price__coast"><?php echo $_smarty_tpl->getVariable('cur')->value['price'];?>
 </span>руб./сут.</div>
                                    <!--<a class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</a>-->
                                    <a class="gui-button" href="/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                </div>
                <?php }?>
            </div>
        <?php }?>
    </div>
</div>
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
