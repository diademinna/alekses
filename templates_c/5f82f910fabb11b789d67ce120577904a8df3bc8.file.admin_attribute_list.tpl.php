<?php /* Smarty version Smarty3-b7, created on 2016-10-28 15:59:19
         compiled from ".\templates\admin/admin_attribute_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2767958134ba7e2d678-99961898%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f82f910fabb11b789d67ce120577904a8df3bc8' => 
    array (
      0 => '.\\templates\\admin/admin_attribute_list.tpl',
      1 => 1477659558,
    ),
  ),
  'nocache_hash' => '2767958134ba7e2d678-99961898',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
	Список аттрибутов номера
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

    <script type="text/javascript">
   
       function delRecord(page, id, get_param){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/attribute/list/"+page+"/delete/"+id+"/"+get_param;
			}
		}
        $(document).ready(function() { 
			$("#sortable").sortable({
				  
				  update : function () { 
					var mass_sort = $('#sortable').sortable('toArray');				      
					xajax_Sort(mass_sort, $('#min_pos').val()); 
				  }
			});
		});		
       
    </script>
    <?php if ($_smarty_tpl->getVariable('data')->value){?>
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/attribute/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
                    <i class="fa fa-plus"></i> Добавить аттрибут</a>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">
                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');">
                        <?php $_template = new Smarty_Internal_Template("admin/common/select_count_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content" >
            <div class="table-responsive">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Иконка</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody id="sortable">
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</td>
                                <td><?php if ($_smarty_tpl->getVariable('cur')->value['ext']){?><img height="50px" src="/uploaded/attribute/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" /><?php }else{ ?>изображение не загружено<?php }?></td>
                                <td style="font-size:18px;">
                                    <a href="/admin/attribute/add/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('page')->value;?>
', '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
', '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                            </tr>
                    <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>  
        <?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>        
    </div>
</form>     

<?php }else{ ?>
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/attribute/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
            <i class="fa fa-plus"></i>	Добавить аттрибут</a>
        </div>
    </div>

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
