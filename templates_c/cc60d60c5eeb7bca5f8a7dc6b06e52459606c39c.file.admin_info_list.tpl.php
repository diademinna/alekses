<?php /* Smarty version Smarty3-b7, created on 2016-11-14 15:15:14
         compiled from ".\templates\admin/admin_info_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:37855829aad2e0a217-12312488%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc60d60c5eeb7bca5f8a7dc6b06e52459606c39c' => 
    array (
      0 => '.\\templates\\admin/admin_info_list.tpl',
      1 => 1479125714,
    ),
  ),
  'nocache_hash' => '37855829aad2e0a217-12312488',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
	Общая информация
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>
<?php if ($_smarty_tpl->getVariable('data')->value){?>
<script type="text/javascript">
        function delRecord(page, id, get_param){
            if(confirm("Вы уверены?")) {
                top.window.location = "/admin/info/list/delete/"+id+"/";
            }
        }
</script>
<form action="" method="post" id="forma_category">
    <div class="ibox float-e-margins">
       <!-- <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/info/add/">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
        </div>-->
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Значение</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>  
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
                        <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']==1){?>
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">Информация, отображаемая на сайте</td></tr>
                        <?php }elseif($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']==5){?>
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">SEO</td></tr>
                        <?php }elseif($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']==8){?>
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">Настройки</td></tr>
                        <?php }?>
                        <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                            <td><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</td>
                            <td><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</td>
                            <td style="font-size:18px;">
                                    <a href="/admin/info/add/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                <!--<i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('page')->value;?>
', '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
', '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');" onmouseover="this.style.cursor='pointer';"></i>-->
                            </td>
                            <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                        </tr>
                    <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>       
    </div>
</form>     

<?php }else{ ?>
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/info/add/">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
