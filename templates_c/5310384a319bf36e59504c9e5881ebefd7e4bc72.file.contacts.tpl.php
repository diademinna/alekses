<?php /* Smarty version Smarty3-b7, created on 2016-12-02 11:55:22
         compiled from ".\templates\contacts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15473584136fa21aa69-91648430%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5310384a319bf36e59504c9e5881ebefd7e4bc72' => 
    array (
      0 => '.\\templates\\contacts.tpl',
      1 => 1480668920,
    ),
  ),
  'nocache_hash' => '15473584136fa21aa69-91648430',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.escape.php';
if (!is_callable('smarty_modifier_replace')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.replace.php';
?><?php ob_start(); ?>

	
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init); // Как только будет загружен API и готов DOM, выполняем инициализацию

        function init () {
            // Создание экземпляра карты и его привязка к контейнеру с заданным id ("map")
            var myMap = new ymaps.Map('map', {
                // При инициализации карты, обязательно нужно указать ее центр и коэффициент масштабирования
                center: [<?php if ($_smarty_tpl->getVariable('data_contacts')->value['latitude']&&$_smarty_tpl->getVariable('data_contacts')->value['longitude']){?><?php echo $_smarty_tpl->getVariable('data_contacts')->value['latitude'];?>
, <?php echo $_smarty_tpl->getVariable('data_contacts')->value['longitude'];?>
<?php }else{ ?>53.199449, 45.020121<?php }?>], // Метка
//                center: [53.199449, 45.020121], // Пенза
                zoom: 14
            });

		
            // Для добавления элемента управления на карту используется поле controls, ссылающееся на коллекцию элементов управления картой.  Добавление элемента в коллекцию производится с помощью метода add().
            // В метод add можно передать строковый идентификатор элемента управления и его параметры.
            myMap.controls                
                .add('zoomControl') // Кнопка изменения масштаба
                .add('typeSelector') // Список типов карты
                .add('smallZoomControl', { right: 5, top: 75 }) // Кнопка изменения масштаба - компактный вариант.
                .add('mapTools') // Стандартный набор кнопок

            // Также в метод add можно передать экземпляр класса, реализующего определенный элемент управления.
            // Например, линейка масштаба ('scaleLine')
            myMap.controls.add(new ymaps.control.ScaleLine())
           		
			
			myPlacemark = new ymaps.Placemark([<?php if ($_smarty_tpl->getVariable('data_contacts')->value['latitude']&&$_smarty_tpl->getVariable('data_contacts')->value['longitude']){?><?php echo $_smarty_tpl->getVariable('data_contacts')->value['latitude'];?>
, <?php echo $_smarty_tpl->getVariable('data_contacts')->value['longitude'];?>
<?php }else{ ?>53.199449, 45.020121<?php }?>], 
				{balloonContentBody: '<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('data_contacts')->value['name_on_map'],'quotes');?>
'}
				
			);
				
//			myPlacemark.options.set({
//				iconImageHref: '/img/map_ico.png',
//				iconImageSize: [60, 94],
//				iconImageOffset: [-29, -94]
//			});
				
			myMap.geoObjects.add(myPlacemark);
			myPlacemark.balloon.open();  // открыть сразу балун
			
        }
    </script>
	


	<div class="l-content content">
    <div class="l-layout">
        <div class="contacts">
            <h2 class="gui-h2">Контакты</h2>
            <div class="contacts-content">
                <div class="row">
                    <div class="col-xs-4">
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['address']){?>
                        <div class="contacts-content__address">
                            <?php echo $_smarty_tpl->getVariable('data_contacts')->value['address'];?>

                        </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['phone']){?>
                        <div class="contacts-content__phone">
                           <?php echo $_smarty_tpl->getVariable('data_contacts')->value['phone'];?>

                        </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['email']){?>
                        <div class="contacts-content__email">
                            <a class="contacts-content__link" href="mailto:<?php echo $_smarty_tpl->getVariable('data_contacts')->value['email'];?>
"><?php echo $_smarty_tpl->getVariable('data_contacts')->value['email'];?>
</a>
                        </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['skype']){?>
                        <div class="contacts-content__skype">
                            <a class="contacts-content__link" href="skype:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_contacts')->value['skype'],' ','');?>
?call"><?php echo $_smarty_tpl->getVariable('data_contacts')->value['skype'];?>
</a>
                        </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['viber']){?>
                        <div class="contacts-content__viber">
                            <a class="contacts-content__link" href="tel:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_contacts')->value['viber'],' ','');?>
"><?php echo $_smarty_tpl->getVariable('data_contacts')->value['viber'];?>
</a>
                        </div>
                    <?php }?>
                    <?php if ($_smarty_tpl->getVariable('data_contacts')->value['whatsapp']){?>
                        <div class="contacts-content__whatsapp">
                            <a class="contacts-content__link" href="tel:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_contacts')->value['whatsapp'],' ','');?>
"><?php echo $_smarty_tpl->getVariable('data_contacts')->value['whatsapp'];?>
</a>
                        </div>
                    <?php }?>
                        <div class="contacts-content__wrap">
                            <div class="contacts-content__taxi">
                                Такси до гостиницы совершенно бесплатно
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div id="map" style="width:100%;height:400px;"></div>
                    </div>
                </div>
                <div class="contacts-content__texttaxi user_content"><?php echo $_smarty_tpl->getVariable('data_contacts')->value['taxi'];?>
</div>
            </div>
            <h2 class="gui-h3">В непосредственной близости находится</h2>
            <ul class="contacts-near">
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Пешеходная улица Московская" src="/images/icons/contacts-1.jpg" />
                    <div class="contacts-near__text">Пешеходная улица Московская</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Пензенский драматический театр им. Луначарского" src="/images/icons/contacts-2.jpg" />
                    <div class="contacts-near__text">Пензенский драматический театр<br/>им. Луначарского</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Центральный парк культуры и отдыха им. Белинского" src="/images/icons/contacts-3.jpg" />
                    <div class="contacts-near__text">Центральный парк культуры и отдыха<br/>им. Белинского</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Торгово-развлекательный центр 'Высшая лига'" src="/images/icons/contacts-4.jpg" />
                    <div class="contacts-near__text">Торгово-развлекательный центр "Высшая лига"</div>
                </li>
            </ul>
        </div>
    </div>
</div>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>


<?php $_template = new Smarty_Internal_Template("common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
