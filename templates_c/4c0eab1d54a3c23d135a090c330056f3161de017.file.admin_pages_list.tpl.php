<?php /* Smarty version Smarty3-b7, created on 2016-11-05 16:52:57
         compiled from ".\templates\admin/admin_pages_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9176581de4394e10f1-57206477%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c0eab1d54a3c23d135a090c330056f3161de017' => 
    array (
      0 => '.\\templates\\admin/admin_pages_list.tpl',
      1 => 1478353975,
    ),
  ),
  'nocache_hash' => '9176581de4394e10f1-57206477',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_truncate')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.truncate.php';
?><?php ob_start(); ?>
	Текстовые страницы
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

	<script type="text/javascript">
		function delRecord(id){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/pages/list/delete/"+id+"/";
			}
		}
	</script>
<?php if ($_smarty_tpl->getVariable('data')->value){?>
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
		<div class="ibox-title">
            <!--<div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/pages/add/<?php echo $_smarty_tpl->getVariable('id_pages')->value;?>
/">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
				<div class="col-xs-2">
                    Выводить по :
                </div>    
                <div class="col-xs-3">

                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');">
                        <?php $_template = new Smarty_Internal_Template("admin/common/select_count_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

                    </select>	
                </div>
            </div>-->
        </div>
		<div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Текст</th>
                            <th width="140px">Действия</th>
                        </tr>
                    </thead>
                    <tbody id="sortable">  
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</td>
                                <td><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('cur')->value['text'],150,"...");?>
</td>
                                
                                <td style="font-size:18px;">
                                    <a href="/admin/pages/add/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
');" onmouseover="this.style.cursor='pointer';"></i>

                                   
                                </td>
                                <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                            </tr>
                    <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>
	</div>
</form>
<?php }else{ ?>
    <!--<div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/pages/add/<?php echo $_smarty_tpl->getVariable('id_pages')->value;?>
/">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>-->

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
