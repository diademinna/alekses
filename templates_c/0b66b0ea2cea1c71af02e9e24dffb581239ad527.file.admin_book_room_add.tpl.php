<?php /* Smarty version Smarty3-b7, created on 2016-11-10 23:18:29
         compiled from ".\templates\admin/admin_book_room_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:239155824d615e1ce58-56855507%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b66b0ea2cea1c71af02e9e24dffb581239ad527' => 
    array (
      0 => '.\\templates\\admin/admin_book_room_add.tpl',
      1 => 1478809103,
    ),
  ),
  'nocache_hash' => '239155824d615e1ce58-56855507',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_cost')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.cost.php';
?><?php ob_start(); ?>
	Номера / <?php if ($_smarty_tpl->getVariable('data')->value['name']){?>Редактировать - <?php echo $_smarty_tpl->getVariable('data')->value['name'];?>
<?php }else{ ?>Добавить<?php }?>
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>


<?php ob_start(); ?>
<div class="ibox-content">
    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <?php $_template = new Smarty_Internal_Template("common/errors_block.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

        <?php if ($_smarty_tpl->getVariable('data_room')->value){?>
        <div class="form-group">
            <label class="col-sm-2 control-label">Номера :</label>
            <div class="col-sm-8">
                <div class="row">
                    <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_room')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                    <div class="col-xs-6">
                        <div class="radio">
                            <input id="apartment_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="radio" name="id_room" value="<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" <?php if ($_smarty_tpl->getVariable('data')->value['id_room']==$_smarty_tpl->getVariable('cur')->value['id']){?>checked<?php }?> onchange="getBookDate('<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
');">
                            <label class="label_radio" for="apartment_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('cur')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
 <strong>(<?php echo smarty_modifier_cost($_smarty_tpl->getVariable('cur')->value['price']);?>
 руб/сут)</strong></label>
                        </div>
                    </div>
                    <?php }} ?>
                </div>
            </div>
        </div>
        <?php }?>
        <div id="info_book" <?php if (!$_smarty_tpl->getVariable('data')->value){?>style="display:none;"<?php }?>>
        <div class="form-group JS-RangeDate">
            <label class="col-sm-2 control-label">Дата заезда* :</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" class="form-control JS-RangeDate-Start" name="date_input" value="<?php echo $_smarty_tpl->getVariable('data')->value['date_input'];?>
">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                </div>
            </div>
            <label class="col-sm-2 control-label">Дата выезда* :</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" class="form-control JS-RangeDate-End" name="date_output" value="<?php echo $_smarty_tpl->getVariable('data')->value['date_output'];?>
">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Телефон * :</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="phone" value="<?php echo $_smarty_tpl->getVariable('data')->value['phone'];?>
">
                    <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                </div>
            </div>
            <label class="col-sm-2 control-label">Почтовый ящик* :</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="email" value="<?php echo $_smarty_tpl->getVariable('data')->value['email'];?>
">
                    <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Комментарий :</label>
            <div class="col-sm-7">
                <textarea name="text" class="form-control" type="text"><?php echo $_smarty_tpl->getVariable('data')->value['text'];?>
</textarea>
            </div>
        </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>


	<script type="text/javascript">	
		jQuery(function($){
			$.datepicker.regional['ru'] = {
				closeText: 'Закрыть',
				prevText: '',
				nextText: '',
				currentText: 'Сегодня',
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
				'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
				'Июл','Авг','Сен','Окт','Ноя','Дек'],
				dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
				dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
				dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
				dateFormat: 'yy-mm-dd', firstDay: 1,
				isRTL: false};
			$.datepicker.setDefaults($.datepicker.regional['ru']);
		}); 

        function getBookDate(id_room) {
            $('#info_book').slideDown(500);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/check_date_book/",
                async: false,
                data:{
                    id_room: id_room
                },
                success: function(response){
                    jQuery(window).trigger('jsChangeBookDate', [response.data || []]);
                }
            });
        }

        function initRangeDate() {
            var common = {
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                minDate: new Date(),
                /*beforeShowDay: function(date){
                    if (window.bookDateSelectNumber.length) { 
                        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [ window.bookDateSelectNumber.indexOf(string) === -1 ];
                    } else {
                        return [true];
                    }
                }*/
            };

            jQuery('.JS-RangeDate').not('JS-RangeDate-ready').each(function() {
                var $elem = jQuery(this).addClass('JS-RangeDate-ready'),
                    $star = $elem.find('.JS-RangeDate-Start'),
                    $end = $elem.find('.JS-RangeDate-End'),
                    bookDate = [];

                jQuery(window).on('jsChangeBookDate', function(event, data) {
                    bookDate = data;
                    
                    var option = {
                        minDate: new Date(),
                        maxDate: null,
                        beforeShowDay: function(date) {
                            if (bookDate.length) { 
                                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                                if (bookDate.indexOf(string) === -1) {
                                    return [true];
                                } else {
                                    return [false, 'book'];
                                }
                            } else {
                                return [true];
                            }
                        } 
                    };

                    $star.datepicker("option", option);
                    $end.datepicker("option", option);
                    $star.datepicker("setDate", null);
                    $end.datepicker("setDate", null);
                });

                 $star.datepicker(jQuery.extend({}, common, {
                     onSelect: function() {
                         $end.datepicker( "option", "minDate", $star[0].value );
                     }
                 }));

                 $end.datepicker(jQuery.extend({}, common, {
                     onSelect: function() {
                         $star.datepicker( "option", "maxDate", $end[0].value );
                     }
                 }));
            });
        }

        $(document).ready(function(){
            initRangeDate();
        });
	</script>


<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
