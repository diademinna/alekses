<?php /* Smarty version Smarty3-b7, created on 2016-11-05 16:49:25
         compiled from ".\templates\admin/admin_pages_add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:26372581de36573d297-93817804%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e287dd945a1da66bb8a4c5757ba17fb64e9785a0' => 
    array (
      0 => '.\\templates\\admin/admin_pages_add.tpl',
      1 => 1478353763,
    ),
  ),
  'nocache_hash' => '26372581de36573d297-93817804',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
	Текстовые страницы / <?php if ($_smarty_tpl->getVariable('data')->value['name']){?>Редактировать - <?php echo $_smarty_tpl->getVariable('data')->value['name'];?>
<?php }else{ ?>Добавить<?php }?>
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>


<?php ob_start(); ?>

	
<div class="ibox-content">
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="parent_id" value=<?php echo $_smarty_tpl->getVariable('parent_id')->value;?>
>
        <?php $_template = new Smarty_Internal_Template("common/errors_block.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

        <div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-10">
                <input name="name" class="form-control" value="<?php echo $_smarty_tpl->getVariable('data')->value['name'];?>
" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Текст* :</label>
            <div class="col-sm-10">
                <textarea name="text" class="tiny" type="text" ><?php echo $_smarty_tpl->getVariable('data')->value['text'];?>
</textarea>
            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-2 control-label">Тайтл:</label>
            <div class="col-sm-10">
                <input name="title" class="form-control" value="<?php echo $_smarty_tpl->getVariable('data')->value['title'];?>
" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>

		
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
