<?php /* Smarty version Smarty3-b7, created on 2016-11-13 17:08:31
         compiled from ".\templates\admin/admin_book_room_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14377582873dfd89934-45743422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '162f8daf00097cb00138f5abc2f674423bc6e496' => 
    array (
      0 => '.\\templates\\admin/admin_book_room_list.tpl',
      1 => 1478851330,
    ),
  ),
  'nocache_hash' => '14377582873dfd89934-45743422',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.date_format.php';
?><?php ob_start(); ?>
	Заявки на бронирование
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

    <script type="text/javascript">
   
       function delRecord(page, id, get_param){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/book_room/list/"+page+"/delete/"+id+"/"+get_param;
			}
		}		
     
    </script>
    <?php if ($_smarty_tpl->getVariable('data')->value){?>
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/book_room/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
                    <i class="fa fa-plus"></i> Забронировать номер</a>
                </div>
                <div class="col-xs-2" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">
                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');">
                        <?php $_template = new Smarty_Internal_Template("admin/common/select_count_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Дата заезда</th>
                            <th>Дата выезда</th>
                            <th>Контактный номер</th>
                            <th>E-mail</th>
                            <th>Название номера</th>
                            <th>Комментарий</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                                <td>
                                <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d.%m.%Y");?>
<br/>
                                <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%H:%M:%S");?>

                                </td>
                                <td><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date_input'],"%d.%m.%Y");?>
</td>
                                <td><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date_output'],"%d.%m.%Y");?>
</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['phone'];?>
</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['email'];?>
</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name_room'];?>
<br/><?php echo $_smarty_tpl->getVariable('cur')->value['price_room'];?>
 руб/сут</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</td>
                                <td style="font-size:18px;">
                                    <a href="/admin/book_room/add/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('page')->value;?>
', '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
', '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                            </tr>
                        <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>  
        <?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>        
    </div>
</form>     

<?php }else{ ?>
   Еще не поступило ни одной заявки

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
