<?php /* Smarty version Smarty3-b7, created on 2016-11-16 17:44:22
         compiled from ".\templates\admin/admin_room_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:751582c70c671b517-41401201%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '952eb9e1629ad6007891d203568500e88fbd67e1' => 
    array (
      0 => '.\\templates\\admin/admin_room_list.tpl',
      1 => 1479307458,
    ),
  ),
  'nocache_hash' => '751582c70c671b517-41401201',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
	Номера
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

<script type="text/javascript">
	function delRecord(page, id, get_param){
		if(confirm("Вы уверены?")) {
			top.window.location = "/admin/room/list/"+page+"/delete/"+id+"/"+get_param;
		}
	}
	
	$(function() {			
		$( "#sortable" ).sortable({
			opacity: 0.8,
			revert: true,
			axis:'y'
		});
	});		
	$(document).ready(function() { 
		$("#sortable").sortable({
				
				update : function () { 
				var mass_sort = $('#sortable').sortable('toArray');				      
				xajax_Sort(mass_sort, $('#min_pos').val()); 
				}
		});
	});
    function functionActivateMain(obj, id_room) {
        var num_check=0;
        $('#forma_category input[name="main"]:checkbox:checked').each(function(){
            if($(this).val() == 1)
                num_check++;
        });
        if (num_check>=3) { 
            $(obj).removeAttr('checked');
            alert("Нельзя выбрать больше 3 номеров для главной страницы");
        }
        else 
            xajax_ActivateMain(id_room);

    }
    
        
        


        
    
</script>
<?php if ($_smarty_tpl->getVariable('data')->value){?>
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <input type="hidden" name="number_main" value="<?php echo $_smarty_tpl->getVariable('number_main')->value;?>
"/>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/room/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
				<div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">

                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');">
                        <?php $_template = new Smarty_Internal_Template("admin/common/select_count_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <!--<th>На баннере</th>-->
                            <th>Категория</th>
                            <th>Название</th>
                            <th>Площадь</th>
                            <th>Цена</th>
                            <th>На главной</th>
                            <th>На сайте</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>  
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                                <!--<td>
                                    <div class="radio">
                                        <input id="banner<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="radio" name="banner" value="<?php echo $_smarty_tpl->getVariable('cur')->value['banner'];?>
" <?php if ($_smarty_tpl->getVariable('cur')->value['banner']==1){?>checked<?php }?> onclick="xajax_ActivateBanner('<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
');">
                                        <label class="label_radio" for="banner<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"></label>
                                    </div>
                                </td>-->
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name_category'];?>
</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
 <span style="font-style:italic;">(<?php if ($_smarty_tpl->getVariable('cur')->value['seats']==1){?>1-местный<?php }elseif($_smarty_tpl->getVariable('cur')->value['seats']==2){?>2-местный<?php }elseif($_smarty_tpl->getVariable('cur')->value['seats']==3){?>3-местный<?php }?>, <?php if ($_smarty_tpl->getVariable('cur')->value['apartment']==1){?>1-комнатный<?php }elseif($_smarty_tpl->getVariable('cur')->value['apartment']==2){?>2-комнатный<?php }elseif($_smarty_tpl->getVariable('cur')->value['apartment']==3){?>3-комнатный<?php }?>)</span></td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['area'];?>
 кв.м.</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['price'];?>
 руб/сут</td>
                                
                                <td>
                                    <div class="checkbox">
                                        <input id="main<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="checkbox" name="main" value="<?php echo $_smarty_tpl->getVariable('cur')->value['main'];?>
" <?php if ($_smarty_tpl->getVariable('cur')->value['main']==1){?>checked<?php }?> onclick="functionActivateMain(this, '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
');">
                                        <label class="label_checkbox" for="main<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="checkbox">
                                        <input id="male<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="checkbox" name="my_checkbox" value="<?php echo $_smarty_tpl->getVariable('cur')->value['active'];?>
" <?php if ($_smarty_tpl->getVariable('cur')->value['active']==1){?>checked<?php }?> onclick="xajax_Activate('<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
')">
                                        <label class="label_checkbox" for="male<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"></label>
                                    </div>
                                </td>
                                <td style="font-size:18px;">
									<a href="/admin/room_photo/add/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>"><i class="fa fa-picture-o" title="Фото" alt="Фото"></i></a> &nbsp &nbsp
                                    <a href="/admin/room/add/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('page')->value;?>
', '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
', '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                            </tr>
                    <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>  
		<?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>     
    </div>
</form>     

<?php }else{ ?>
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/room/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
