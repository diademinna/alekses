<?php /* Smarty version Smarty3-b7, created on 2016-12-02 11:39:42
         compiled from ".\templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:65275841334eb8ab73-24596494%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '749422d4cfc3eb5677cf499730392b6accd4d1c7' => 
    array (
      0 => '.\\templates\\index.tpl',
      1 => 1480667977,
    ),
  ),
  'nocache_hash' => '65275841334eb8ab73-24596494',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php ob_start(); ?>
<div class="main-banner">
    <img class="main-banner__image" src="/_media/banner.jpg" />
    <div class="main-banner__wrapp">
        <?php if ($_smarty_tpl->getVariable('data_main_text')->value['text']){?>
        <div class="main-banner__text"><?php echo $_smarty_tpl->getVariable('data_main_text')->value['text'];?>

            <a class="gui-button main-banner__button JS-Popup" href="#popup-book">Забронировать</a>
        </div>
        <?php }?>
    </div>
</div>
<div class="l-content content">
    <div class="l-layout">
        <div class="main">
            <?php if ($_smarty_tpl->getVariable('data_room_main')->value){?>
            <div class="main-rooms">
                <h2 class="gui-h2">Номера гостиницы</h2>
                <div class="main-rooms__wrap">
                    <div class="row">
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_room_main')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                        <div class="col-xs-4">
                            <div class="room">
                                <div class="room__content">
                                    <div class="room-name"><a class="room-name__link" href="/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><?php echo $_smarty_tpl->getVariable('cur')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a></div>
                                    <?php if ($_smarty_tpl->getVariable('cur')->value['ext']){?><img class="room__image" alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_prev.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" /><?php }else{ ?><img class="room__image" src="/images/no-image_prev.png" /><?php }?>
                                    <div class="room__anons"><?php echo $_smarty_tpl->getVariable('cur')->value['seats'];?>
-местный, <?php echo $_smarty_tpl->getVariable('cur')->value['apartment'];?>
-комнатный</div>
                                    <div class="room__description"><?php echo $_smarty_tpl->getVariable('cur')->value['anons'];?>
</div>
                                    <div class="room-price"><span class="room-price__coast"><?php echo $_smarty_tpl->getVariable('cur')->value['price'];?>
 </span>руб./сут.</div>
                                    <!--<button class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</button>-->
                                    <a class="gui-button" href="/room/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                </div>
            </div>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('data_stock')->value){?>
            <div class="main-actions JS-Scrolling-Block" data-scrolling-id="services">
                <h2 class="gui-h2">Акции</h2>
                <div class="main-actions__wrapp">
                <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_stock')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
                    <div class="actions JS-Scrolling-Block" data-scrolling-id="service-<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']%2!=0){?>
                            <div class="actions-image">
                                <img class="actions-image__img" alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/stock/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_sm.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" />
                            </div>
                                <div class="actions-text actions-text_right">
                                    <div class="actions-text__name"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</div>
                                    <div class="actions-text__anons"><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</div>
                                    <div class="actions-text__text"><?php echo $_smarty_tpl->getVariable('cur')->value['participation'];?>
</div>
                                </div>
                            <?php }else{ ?>
                                 <div class="actions-text actions-text_left">
                                    <div class="actions-text__name"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</div>
                                    <div class="actions-text__anons"><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</div>
                                    <div class="actions-text__text"><?php echo $_smarty_tpl->getVariable('cur')->value['participation'];?>
</div>
                                </div>
                                <div class="actions-image"><img class="actions-image__img actions-image__img_left" alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/stock/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
_sm.<?php echo $_smarty_tpl->getVariable('cur')->value['ext'];?>
" /></div>
                            <?php }?>
                    </div>
                <?php }} ?>
                </div>
            </div>
            <?php }?>
            <?php if ($_smarty_tpl->getVariable('data_service')->value){?>
            <div class="main-service">
                <h2 class="gui-h2">Дополнительные услуги</h2>
                <div class="main-service__wrapp">
                    <div class="row">
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_service')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                        <div class="col-xs-4">
                            <div class="service">
                                <img alt="<?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
" src="/uploaded/service/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
.png" />
                                <div class="service__name"><?php if ($_smarty_tpl->getVariable('cur')->value['id']==1){?><a class="service__link" href="/contacts/"><?php }?><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a></div>
                                <div class="service__text"><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</div>
                            </div>
                        </div>
                        <?php }} ?>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
