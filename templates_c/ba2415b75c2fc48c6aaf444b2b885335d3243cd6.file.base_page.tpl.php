<?php /* Smarty version Smarty3-b7, created on 2016-12-02 11:37:51
         compiled from ".\templates\admin/common/base_page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:32321584132df7949c0-49982313%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ba2415b75c2fc48c6aaf444b2b885335d3243cd6' => 
    array (
      0 => '.\\templates\\admin/common/base_page.tpl',
      1 => 1480665555,
    ),
  ),
  'nocache_hash' => '32321584132df7949c0-49982313',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>АДМИНКА</title>
	<link href="/styles/admin/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/styles/admin/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>

	<!--  v1.11  -  для датапикера datepicker и sortable -->
	<link type="text/css" href="/js/ui/css/jquery-ui.css" rel="stylesheet" />
	<link type="text/css" href="/styles/admin/font-awesome.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/ui/jquery-ui.min.js"></script>


	<?php echo $_smarty_tpl->getVariable('ajaxCode')->value;?>


	<?php $_template = new Smarty_Internal_Template("common/tinymce.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>


<script>
    jQuery(document).ready(function() {
        $('#side-menu a[data-toggle="collapse"] + ul').on('show.bs.collapse hide.bs.collapse', function () {
            jQuery(this).closest('li').toggleClass('active');
        });
    });
</script>

</head>

<body class="pace-done">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul id="side-menu" class="nav metismenu">
                    <li class="nav-header" style="">
                        <a href="/admin/"><div class="dropdown profile-element">
                            <span>
                                <img style="max-width:100%;" class="" src="/images/logo_admin.png" alt="image" />
                            </span>
                        </a>
                        </div>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="room"){?> class="active"<?php }?>>
                        <a href="#" data-toggle="collapse" data-target="#sidebar-collapse-1">
                            <i class="fa fa-bed" aria-hidden="true"></i>
                            <span class="nav-label">Номера</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse<?php if ($_smarty_tpl->getVariable('unit')->value=='room'){?> in<?php }?>" id="sidebar-collapse-1">
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="room"){?> class="active"<?php }?>><a href="/admin/room/list/">Список номеров</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="attribute"){?> class="active"<?php }?>><a href="/admin/attribute/list/">Оснащение номеров</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="category"){?> class="active"<?php }?>><a href="/admin/category/list/">Категории номеров</a></li>
                        </ul>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="book_room"){?> class="active"<?php }?>>
                        <a href="/admin/book_room/list" >
                            <i class="fa fa-check-square-o"></i>
                            <span class="nav-label">Бронирование номеров</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="callback"){?> class="active"<?php }?>>
                        <a href="/admin/callback/list" >
                            <i class="fa fa-phone"></i>
                            <span class="nav-label">Заявки на звонок</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="page"){?> class="active"<?php }?>>
                        <a href="#" data-toggle="collapse" data-target="#sidebar-collapse-2">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Текстовые страницы</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse<?php if ($_smarty_tpl->getVariable('unit')->value=='page'){?> in<?php }?>" id="sidebar-collapse-2">
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="1"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/1/">О гостинице (на главной)</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="2"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/2/">О гостинице</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="3"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/3/">Услуги</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="4"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/4/">Правила проживания</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="5"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/5/">Договор оферты</a></li>
                            <li<?php if ($_smarty_tpl->getVariable('subunit')->value=="6"){?> class="active"<?php }?>><a href="/admin/pages/add/edit/6/">Правила пожарной безопасности</a></li>
                        </ul>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="contacts"){?> class="active"<?php }?>>
                        <a href="/admin/contacts/">
                            <i class="fa fa-newspaper-o"></i>
                            <span class="nav-label">Контакты</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="stock"){?> class="active"<?php }?>>
                        <a href="/admin/stock/list" >
                            <i class="fa fa-gift"></i>
                            <span class="nav-label">Акции</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="service"){?> class="active"<?php }?>>
                        <a href="/admin/service/list" >
                            <i class="fa fa-cog"></i>
                            <span class="nav-label">Дополнительные услуги (на главной)</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="review"){?> class="active"<?php }?>>
                        <a href="/admin/review/list" >
                            <i class="fa fa-comments-o"></i>
                            <span class="nav-label">Отзывы</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="info"){?> class="active"<?php }?>>
                        <a href="/admin/info/list/">
                            <i class="fa fa-cogs"></i>
                            <span class="nav-label">Настройки</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="seo"){?> class="active"<?php }?>>
                        <a href="/admin/seo/list/">
                            <i class="fa fa-line-chart"></i>
                            <span class="nav-label">Счетчики, метрика</span>
                        </a>
                    </li>
                    <!--<li<?php if ($_smarty_tpl->getVariable('unit')->value=="room"){?> class="active"<?php }?>>
                        <a href="/admin/room/list/">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Номера</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="attribute"){?> class="active"<?php }?>>
                        <a href="/admin/attribute/list/">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Аттрибуты номеров</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="category"){?> class="active"<?php }?>>
                        <a href="/admin/category/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Категориии номеров</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="info"){?> class="active"<?php }?>>
                        <a href="/admin/info/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Настройки</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="book_room"){?> class="active"<?php }?>>
                        <a href="/admin/book_room/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Заявки</span>
                        </a>
                    </li>
                    <li<?php if ($_smarty_tpl->getVariable('unit')->value=="book_room"){?> class="active"<?php }?>>
                        <a href="/admin/pages/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Текстовые страницы</span>
                        </a>
                    </li>-->
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg" >
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" style="margin-bottom: 0" role="navigation">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Добро пожаловать в раздел администрирования</span>
                        </li>
                        <li>
                            <a target="_blank" href="/">
                                    <i class="fa fa-sign-out"></i>На сайт
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?php echo $_smarty_tpl->smarty->_smarty_vars['capture']['content_name'];?>
</h2>
                    <?php if ($_smarty_tpl->getVariable('mass_navigation')->value){?>
                        <ol class="breadcrumb">
                            <?php if ($_smarty_tpl->getVariable('unit')->value=='parameter'){?>
                                <li><a href="/admin/parameter/list/">Все параметры</a></li>
                            <?php }else{ ?>
                                <li><a href="/admin/category/list/">Все категории</a></li>
                            <?php }?>
                            
                            <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('mass_navigation')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['cur']->total=count($_from);
 $_smarty_tpl->tpl_vars['cur']->iteration=0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['total'] = $_smarty_tpl->tpl_vars['cur']->total;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['cur']->iteration++;
 $_smarty_tpl->tpl_vars['cur']->last = $_smarty_tpl->tpl_vars['cur']->iteration === $_smarty_tpl->tpl_vars['cur']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['last'] = $_smarty_tpl->tpl_vars['cur']->last;
?>
                                <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['last']){?>
                                    <li class="active">
                                        <strong><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</strong>
                                    </li>
                                <?php }else{ ?>
                                    <li>
                                        <?php if ($_smarty_tpl->getVariable('unit')->value=='parameter'){?>
                                            <a href="/admin/parameter/list/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a>
                                        <?php }else{ ?>
                                            <a href="/admin/category/list/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a>
                                        <?php }?>
                                    </li>
                                <?php }?>
                            <?php }} ?>
                        </ol>
                    <?php }?>
                </div>
            </div>
            <?php echo $_smarty_tpl->smarty->_smarty_vars['capture']['content'];?>

        </div>
    </div>
</body>
</html>
