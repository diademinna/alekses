<?php /* Smarty version Smarty3-b7, created on 2016-12-01 16:24:53
         compiled from ".\templates\common/base_page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11801584024a5caa545-38123886%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7ea33b6fdf0c1432e6017bf0d11ae65cd0b94df0' => 
    array (
      0 => '.\\templates\\common/base_page.tpl',
      1 => 1480598637,
    ),
  ),
  'nocache_hash' => '11801584024a5caa545-38123886',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_replace')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.replace.php';
?><!DOCTYPE html>
<html>
<head>
	<title><?php if ($_smarty_tpl->getVariable('page_title')->value){?><?php echo $_smarty_tpl->getVariable('page_title')->value;?>
 - <?php }?><?php if ($_smarty_tpl->getVariable('data_info')->value[2]['text']){?><?php echo $_smarty_tpl->getVariable('data_info')->value[2]['text'];?>
<?php }else{ ?>Алексес Отель<?php }?></title>
	<meta charset="utf-8" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="<?php echo $_smarty_tpl->getVariable('data_info')->value[4]['text'];?>
">
	<meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('data_info')->value[3]['text'];?>
">
	<meta name="author" content="">
	
  <link rel="stylesheet" href="/styles/libs/magnific-popup.css" />
  <link rel="stylesheet" href="/styles/libs/royalslider.css" />
  <link rel="stylesheet" href="/styles/libs/rs-default.css" />
  <link rel="stylesheet" href="/styles/libs/fontello.css" />
  <link rel="stylesheet" href="/styles/libs/jquery-ui.min.css" />
  <link rel="stylesheet" href="/styles/pager.css" />
  <link rel="stylesheet" href="/styles/alekses.css" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
</head>

<body class="">
<div class="l-wrapper JS-Scrolling">
    <header class="header l-header">
      <div class="l-layout">
        <div class="header__container">
          <div class="header__logo"><a href="/"><img alt="Отель Алексес" src="/images/logo.png" /></a></div>
          <div class="header-info">
            <?php ob_start();?><?php echo $_smarty_tpl->getVariable('data_info')->value[6]['text'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1){?><div class="header-info__address"><?php echo $_smarty_tpl->getVariable('data_info')->value[6]['text'];?>
</div><?php }?>
           <div class="header-info__weather JS-Weather">
              <div class="header-weather">
                <div class="header-weather__time"><div id="time"><?php echo $_smarty_tpl->getVariable('hour_now')->value;?>
:<?php echo $_smarty_tpl->getVariable('minute_now')->value;?>
</div></div>
                <div class="header-weather__degrees">
                  <span class="JS-Weather-Temp">0</span>
                </div>
                <div class="header-weather__precipitation">
                  <img class="JS-Weather-Image" src="" alt="" title="" />
                </div>
              </div>
            </div>
            <?php if ($_smarty_tpl->getVariable('data_info')->value[5]['text']){?>
            <div class="header-info__phone">
              <a class="header-info__link" href="tel:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_info')->value[5]['text'],' ','');?>
"><?php echo $_smarty_tpl->getVariable('data_info')->value[5]['text'];?>
</a>
              <?php if ($_smarty_tpl->getVariable('data_info')->value[8]['text']){?>
                <br/>
                <a class="header-info__link" href="tel:<?php echo smarty_modifier_replace($_smarty_tpl->getVariable('data_info')->value[8]['text'],' ','');?>
"><?php echo $_smarty_tpl->getVariable('data_info')->value[8]['text'];?>
</a>
              <?php }?>
            </div>
            <?php }?>
          </div>
          <div class="header-actions">
            <ul class="header-menu">
              <li class="header-menu__item"><a href="/about/" class="header-menu__link">о гостинице</a></li>
              <li class="header-menu__item"><a href="/room/" class="header-menu__link">номера и цены</a></li>
              <li class="header-menu__item"><a href="/service/" class="header-menu__link">услуги</a></li>
              <li class="header-menu__item"><a href="/contacts/" class="header-menu__link">контакты</a></li>
              <li class="header-menu__item"><a href="<?php if ($_smarty_tpl->getVariable('unit')->value!='index'){?>/<?php }?>#services" class="header-menu__link JS-Scrolling-Item">акции</a></li>
              <li class="header-menu__item"><a href="/review/" class="header-menu__link">отзывы</a></li>
              
            </ul>
            <div class="header-actions__callback">
              <a href="#popup-callback" class="header-actions__link JS-Popup">перезвоните мне</a>
            </div>
          </div>
        </div>
      </div>
    </header>
  
            <?php echo $_smarty_tpl->smarty->_smarty_vars['capture']['content'];?>

      
    <footer class="footer l-footer">
      <div class="l-layout">
        <div class="footer__col">
          <div class="footer-logo"><a href="/"><img alt="Отель Алексес" src="/images/logo-footer.png" /></a></div>
        </div>
        <div class="footer__col">
          <div class="footer-menu">
            <div class="footer-menu__title">Меню</div>
            <ul class="footer-menu__list">
              <li class="footer-menu__item"><a class="footer-menu__link" href="/about/">О гостинице</a></li>
              <li class="footer-menu__item"><a class="footer-menu__link" href="/room/">Номера и цены</a></li>
              <li class="footer-menu__item"><a class="footer-menu__link" href="/service/">Услуги</a></li>
              <li class="footer-menu__item"><a class="footer-menu__link" href="/contacts/">Контакты</a></li>
              <li class="footer-menu__item"><a class="footer-menu__link JS-Scrolling-Item" href="<?php if ($_smarty_tpl->getVariable('unit')->value!='index'){?>/<?php }?>#services">Акции</a></li>
              <li class="footer-menu__item"><a class="footer-menu__link" href="/review/">Отзывы</a></li>
              
            </ul>
          </div>
        </div>
        <div class="footer__col">
          <div class="footer-menu">
          <?php if ($_smarty_tpl->getVariable('data_stock')->value){?>
            <div class="footer-menu__title">Акции</div>
            <ul class="footer-menu__list">
              <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_stock')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
              <li class="footer-menu__item footer-menu__item_actions"><a class="footer-menu__link JS-Scrolling-Item" href="<?php if ($_smarty_tpl->getVariable('unit')->value!='index'){?>/<?php }?>#service-<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</a></li>
              <?php }} ?>
            </ul>
            <?php }?>
          </div>
        </div>
        <div class="footer__col">
          <div class="footer-contacts">
            <div class="footer-contacts__title">Контакты</div>
            <?php if ($_smarty_tpl->getVariable('data_info')->value[6]['text']){?><div class="footer-contacts__item"><?php echo $_smarty_tpl->getVariable('data_info')->value[6]['text'];?>
</div><?php }?>
            <?php if ($_smarty_tpl->getVariable('data_info')->value[5]['text']){?><div class="footer-contacts__item"><?php echo $_smarty_tpl->getVariable('data_info')->value[5]['text'];?>
</div><?php }?>
            <div class="footer-contacts__item footer-contacts__item_email"><a class="footer-contacts__link" href="mailto:<?php echo $_smarty_tpl->getVariable('data_info')->value[7]['text'];?>
"><?php echo $_smarty_tpl->getVariable('data_info')->value[7]['text'];?>
</a></div>
          </div>
        </div>
      </div>
    </footer>  
</div>
<div id="popup-book" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Бронирование номера</div>
  </div>
  <div class="popup__container">
    <form class="JS-Validate JS-RangeDatepikerRoom" data-validate-params="{
          'type': 'POST',
          'idPopupSuccess': '#popup-thankyou',
          'url': '/book_room/',
          'rules':{
              'date_input': {'required': true}, 
              'date_output': {'required': true}, 
              'email': {'required': true, 'email': true}, 
              'phone': {'required': true}
          }
      }"
      data-rangedatepikerroom-params="{
        'url': '/check_date_book/',
        'cssSelectedElement': 'gui-state-selected'
      }" >
      <input type="hidden" name="name_room" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['name_category'];?>
 <?php echo $_smarty_tpl->getVariable('data_item')->value['name'];?>
" />
      <input type="hidden" name="price_room" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['price'];?>
" />
      <input class="JS-RangeDatepikerRoom-Room" type="hidden" name="id_room" value="<?php echo $_smarty_tpl->getVariable('data_item')->value['id'];?>
" />
      <div class="row">
        <div class="col-xs-6">
          <div class="gui-field">
            <label class="gui-label">Дата заезда: *</label>
            <input class="gui-input gui-input_calendar  JS-Datepicker JS-RangeDatepikerRoom-Start" type="text" id="date_input" name="date_input" value="" />
          </div>
        </div>
        <div class="col-xs-6">
          <div class="gui-field">
            <label class="gui-label">Дата выезда: *</label>
            <input class="gui-input gui-input_calendar JS-Datepicker JS-RangeDatepikerRoom-End" type="text" id="date_output" name="date_output" value="" />
          </div>
        </div>
      </div>
      <!--<a class="JS-RangeDatepikerRoom-Clear" href="#">Очистить диапазон</a>-->
      <div class="gui-field">
          <label class="gui-label">Ваш телефон: *:</label>
          <input class="gui-input" type="text" name="phone" value="" />
      </div>
      <div class="gui-field">
          <label class="gui-label">Ваш почтовый ящик: *:</label>
          <input class="gui-input" type="text" name="email" value="" />
      </div>
      <button class="gui-button popup__button">Забронировать</button>
    </form>
  </div>
</div>
<div id="popup-callback" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Перезвоните мне</div>
  </div>
  <div class="popup__container">
    <form class="JS-Validate" data-validate-params="{
          'type': 'POST',
          'idPopupSuccess': '#popup-callback-success',
          'url': '/callback/',
          'rules':{
              'phone': {'required': true}
          }
      }">
      <input type="hidden" name="name_room" value="" />
      <input type="hidden" name="price_room" value="" />
      <input type="hidden" name="id_room" value="" />
      
      <div class="gui-field">
          <label class="gui-label">Ваше имя:</label>
          <input class="gui-input" type="text" name="name" value="" />
      </div>
      <div class="gui-field">
          <label class="gui-label">Ваш телефон: *:</label>
          <input class="gui-input" type="text" name="phone" value="" />
      </div>
      <div class="gui-field">
          <label class="gui-label">Комментарий:</label>
          <textarea class="gui-textarea" name="text"></textarea>
      </div>
      <button class="gui-button popup__button">Отправить</button>
    </form>
  </div>
</div>
<div id="popup-thankyou" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Заявка принята</div>
  </div>
  <div class="popup__container">
    <div class="popup-success__text">
          Вы забронировали номер.<br/>В течение часа наш администратор свяжется с вами.
    </div>
  </div>
</div>
<div id="popup-callback-success" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Заявка принята</div>
  </div>
  <div class="popup__container">
    <div class="popup-success__text">
          Ожидайте звонка.<br/>В ближайшее время администратор отеля свяжется с вами
    </div>
  </div>
</div>
<div id="popup-review" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Оставить отзыв</div>
  </div>
  <div class="popup__container">
    <form class="JS-Validate" data-validate-params="{
          'type': 'POST',
          'idPopupSuccess': '#popup-review-thankyou',
          'url': '/review_add/',
          'rules':{
              'name': {'required': true},
              'text': {'required': true}
          }
      }">      
      <div class="gui-field">
          <label class="gui-label">Ваше имя *:</label>
          <input class="gui-input" type="text" name="name" value="" />
      </div>
      <div class="gui-field">
          <label class="gui-label">Ваш почтовый ящик:</label>
          <input class="gui-input" type="text" name="email" value="" />
      </div>
      <div class="gui-field">
          <label class="gui-label">Отзыв *:</label>
          <textarea class="gui-textarea" name="text"></textarea>
      </div>
      <div class="gui-field gui-field_raiting">
         <div class="title_raiting">Ваша оценка: </div>
         <fieldset class="rating">
            <input type="radio" id="star5" name="rating" value="5" /><label for="star5">5 stars</label>
            <input type="radio" id="star4" name="rating" value="4" /><label for="star4">4 stars</label>
            <input type="radio" id="star3" name="rating" value="3" /><label for="star3">3 stars</label>
            <input type="radio" id="star2" name="rating" value="2" /><label for="star2">2 stars</label>
            <input type="radio" id="star1" name="rating" value="1" /><label for="star1">1 star</label>
        </fieldset>
        </div>
      <button class="gui-button popup__button">Отправить</button>
    </form>
  </div>
</div>
<div id="popup-review-thankyou" class="mfp-hide popup">
  <div class="popup__heading">
    <div class="popup__title">Отзыв отправлен</div>
  </div>
  <div class="popup__container">
    <div class="popup-success__text">
          Ваше мнение очень важно для нас
    </div>
  </div>
</div>
<script src="/js/libs/jquery-1.11.3.js"></script>
<script src="/js/libs/jquery-ui.min.js"></script>
<script src="/js/libs/datepicker-ru.js"></script>
<script src="/js/libs/jquery.validate.min.js"></script>
<script src="/js/libs/jquery.royalslider.min.js"></script>
<script src="/js/libs/jquery.magnific-popup.min.js"></script>
<script src="/js/global.min.js"></script>
<script src="/js/libs/JS.Scrolling.js"></script>
<script src="/js/JS.Weather.js"></script>
<script src="/js/JS.RangeDatepikerRoom.js"></script>
<script src="/js/main.js"></script>
<?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_seo')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
<?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>

<?php }} ?>
</body>
</html>