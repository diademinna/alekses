<?php /* Smarty version Smarty3-b7, created on 2016-11-15 10:48:13
         compiled from ".\templates\review.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4211582abdbd6ad8e2-57080529%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '166fc61ed9f904af02623519f2cf446c64b2fe20' => 
    array (
      0 => '.\\templates\\review.tpl',
      1 => 1479196057,
    ),
  ),
  'nocache_hash' => '4211582abdbd6ad8e2-57080529',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.date_format.php';
?><?php ob_start(); ?>
<div class="l-content content">
    <div class="l-layout">
        <div class="reviews">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item">Отзывы</li>
            </ul>
            <h2 class="gui-h2">Отзывы наших гостей</h2>  
            <div class="reviews__button">
                <a href="#popup-review" class="gui-button JS-Popup" >Оставить отзыв</a>  
            </div>
            <div class="reviews__wrap">
                <div class="row">
                    <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_review')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
					<div class="col-xs-6">
                        <div class="review">
                            <div class="review__name"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</div>
                            <div class="review__info">
                                <?php if ($_smarty_tpl->getVariable('cur')->value['rating']){?>
                                    <ul class="review-rating">
                                        <?php $_smarty_tpl->tpl_vars['var'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['var']->step = (5 - (1) < 0) ? -1 : 1;$_smarty_tpl->tpl_vars['var']->total = (int)ceil(($_smarty_tpl->tpl_vars['var']->step > 0 ? 5+1 - 1 : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['var']->step));
if ($_smarty_tpl->tpl_vars['var']->total > 0){
for ($_smarty_tpl->tpl_vars['var']->value = 1, $_smarty_tpl->tpl_vars['var']->iteration = 1;$_smarty_tpl->tpl_vars['var']->iteration <= $_smarty_tpl->tpl_vars['var']->total;$_smarty_tpl->tpl_vars['var']->value += $_smarty_tpl->tpl_vars['var']->step, $_smarty_tpl->tpl_vars['var']->iteration++){
$_smarty_tpl->tpl_vars['var']->first = $_smarty_tpl->tpl_vars['var']->iteration == 1;$_smarty_tpl->tpl_vars['var']->last = $_smarty_tpl->tpl_vars['var']->iteration == $_smarty_tpl->tpl_vars['var']->total;?>
                                            <li class="review-rating__item <?php if ($_smarty_tpl->getVariable('var')->value<=$_smarty_tpl->getVariable('cur')->value['rating']){?>review-rating__item_check<?php }?>"></li>
                                        <?php }} ?>
                                    </ul>
                                <?php }?>
                                <div class="review__date"> 
									<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d ");?>

									<?php ob_start();?><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%m");?>
<?php $_tmp1=ob_get_clean();?><?php $_smarty_tpl->assign("num_month",$_tmp1,null,null);?>
									<?php echo $_smarty_tpl->getVariable('MN')->value[$_smarty_tpl->getVariable('num_month')->value];?>

									<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%Y");?>

								</div>
                            </div>
                            <div class="review__text"><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</div>
                        </div>
                    </div>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']%2==0){?>
						</div>
						<div class="row">
					<?php }?>
				    <?php }} ?>
                </div>
            </div>       
        </div>
        <?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager_string"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>
    </div>
</div>


<!--<div class="reviews">
    <div class="l-layout">
        <ul class="breadcrumbs">
            <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/"><i class="icon-home"></i>Главная</a></li>
            <li class="breadcrumbs__item breadcrumbs__item_separate"><i class="icon-right-open-big"></i></li>
            <li class="breadcrumbs__item">Отзывы</li>
        </ul>
        <h1 class="gui-h1">Отзывы наших клиентов</h1>
        <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
                <div class="reviews-actions">
                    <a class="gui-button JS-Popup" href="#popup-give-review">Оставить отзыв</a>
                    <a class="reviews-actions__order JS-Popup" href="#popup-order-service">Заказать работу</a>
                </div>
            </div>
        </div>
        <div class="reviews-container">
            <div class="row">
				<?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_review')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
					<div class="col-xs-6">
                        <div class="review">
                            <div class="review__name"><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</div>
                            <div class="review__info">
                                <?php if ($_smarty_tpl->getVariable('cur')->value['rating']){?>
                                    <ul class="review-rating">
                                        <?php $_smarty_tpl->tpl_vars['var'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['var']->step = (5 - (1) < 0) ? -1 : 1;$_smarty_tpl->tpl_vars['var']->total = (int)ceil(($_smarty_tpl->tpl_vars['var']->step > 0 ? 5+1 - 1 : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['var']->step));
if ($_smarty_tpl->tpl_vars['var']->total > 0){
for ($_smarty_tpl->tpl_vars['var']->value = 1, $_smarty_tpl->tpl_vars['var']->iteration = 1;$_smarty_tpl->tpl_vars['var']->iteration <= $_smarty_tpl->tpl_vars['var']->total;$_smarty_tpl->tpl_vars['var']->value += $_smarty_tpl->tpl_vars['var']->step, $_smarty_tpl->tpl_vars['var']->iteration++){
$_smarty_tpl->tpl_vars['var']->first = $_smarty_tpl->tpl_vars['var']->iteration == 1;$_smarty_tpl->tpl_vars['var']->last = $_smarty_tpl->tpl_vars['var']->iteration == $_smarty_tpl->tpl_vars['var']->total;?>
                                            <li class="review-rating__item <?php if ($_smarty_tpl->getVariable('var')->value<=$_smarty_tpl->getVariable('cur')->value['rating']){?>review-rating__item_check<?php }?>"></li>
                                        <?php }} ?>
                                    </ul>
                                <?php }?>
                                <div class="review__date"> 
									<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d ");?>

									<?php ob_start();?><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%m");?>
<?php $_tmp2=ob_get_clean();?><?php $_smarty_tpl->assign("num_month",$_tmp2,null,null);?>
									<?php echo $_smarty_tpl->getVariable('MN')->value[$_smarty_tpl->getVariable('num_month')->value];?>

									<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%Y");?>

								</div>
                            </div>
                            <div class="review__text"><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</div>
                        </div>
                    </div>
					<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']%2==0){?>
						</div>
						<div class="row">
					<?php }?>
				<?php }} ?>
            </div>
        </div>
        <?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager_string"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>
    </div>-->
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
