<?php /* Smarty version Smarty3-b7, created on 2016-11-14 16:35:07
         compiled from ".\templates\admin/admin_review_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:230795829bd8b12c230-22272383%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cc553055859a98b3016df5963ff23c81a8bbdab3' => 
    array (
      0 => '.\\templates\\admin/admin_review_list.tpl',
      1 => 1472213559,
    ),
  ),
  'nocache_hash' => '230795829bd8b12c230-22272383',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.truncate.php';
?><?php ob_start(); ?>
	Отзывы
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

    <script type="text/javascript">
   
       function delRecord(page, id, get_param){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/review/list/"+page+"/delete/"+id+"/"+get_param;
			}
		}			
       
    </script>
    <?php if ($_smarty_tpl->getVariable('data')->value){?>
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/review/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
                    <i class="fa fa-plus"></i> Добавить отзыв</a>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">
                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');">
                        <?php $_template = new Smarty_Internal_Template("admin/common/select_count_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Оценка</th>
                            <th>Текст</th>
                            <th>На сайте</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
?>
                            <tr id="item_<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
">
                                <td>
                                <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d.%m.%Y");?>
<br/>
                                <?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%H:%M:%S");?>

                                </td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
</td>
                                <td><?php echo $_smarty_tpl->getVariable('cur')->value['email'];?>
</td>
                                 <td>
                                 <ul class="review-rating">
                                    <?php $_smarty_tpl->tpl_vars['var'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['var']->step = (5 - (1) < 0) ? -1 : 1;$_smarty_tpl->tpl_vars['var']->total = (int)ceil(($_smarty_tpl->tpl_vars['var']->step > 0 ? 5+1 - 1 : 1-(5)+1)/abs($_smarty_tpl->tpl_vars['var']->step));
if ($_smarty_tpl->tpl_vars['var']->total > 0){
for ($_smarty_tpl->tpl_vars['var']->value = 1, $_smarty_tpl->tpl_vars['var']->iteration = 1;$_smarty_tpl->tpl_vars['var']->iteration <= $_smarty_tpl->tpl_vars['var']->total;$_smarty_tpl->tpl_vars['var']->value += $_smarty_tpl->tpl_vars['var']->step, $_smarty_tpl->tpl_vars['var']->iteration++){
$_smarty_tpl->tpl_vars['var']->first = $_smarty_tpl->tpl_vars['var']->iteration == 1;$_smarty_tpl->tpl_vars['var']->last = $_smarty_tpl->tpl_vars['var']->iteration == $_smarty_tpl->tpl_vars['var']->total;?>
                                        <li class="review-rating__item <?php if ($_smarty_tpl->getVariable('var')->value<=$_smarty_tpl->getVariable('cur')->value['rating']){?>review-rating__item_check<?php }?>"></li>
                                    <?php }} ?>
                                </ul>
                                 </td>
                                 <td><?php echo smarty_modifier_truncate($_smarty_tpl->getVariable('cur')->value['text'],120,"...");?>
</td>
                                <td>
                                    <div class="checkbox">
                                        <input id="male<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
" type="checkbox" name="my_checkbox" value="<?php echo $_smarty_tpl->getVariable('cur')->value['active'];?>
" <?php if ($_smarty_tpl->getVariable('cur')->value['active']==1){?>checked<?php }?> onclick="xajax_Activate('<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
')">
                                        <label class="label_checkbox" for="male<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
"></label>
                                    </div>
                                </td>
                                <td style="font-size:18px;">
                                    <a href="/admin/review/add/<?php echo $_smarty_tpl->getVariable('page')->value;?>
/edit/<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('<?php echo $_smarty_tpl->getVariable('page')->value;?>
', '<?php echo $_smarty_tpl->getVariable('cur')->value['id'];?>
', '<?php echo $_smarty_tpl->getVariable('get_param')->value;?>
');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                <?php $_smarty_tpl->assign("min_pos",$_smarty_tpl->getVariable('cur')->value['pos'],null,null);?>
                            </tr>
                    <?php }} ?>
                    </tbody>
                    <input type="hidden" id="min_pos" value="<?php echo $_smarty_tpl->getVariable('min_pos')->value;?>
">
                </table>
            </div>
        </div>  
        <?php if ($_smarty_tpl->getVariable('pager_string')->value){?><div class="pager"><?php echo $_smarty_tpl->getVariable('pager_string')->value;?>
</div><?php }?>        
    </div>
</form>     

<?php }else{ ?>
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/review/add/<?php if ($_smarty_tpl->getVariable('get_param')->value){?><?php echo $_smarty_tpl->getVariable('get_param')->value;?>
<?php }?>">
            <i class="fa fa-plus"></i>	Добавить отзыв</a>
        </div>
    </div>

<?php }?>
	
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
