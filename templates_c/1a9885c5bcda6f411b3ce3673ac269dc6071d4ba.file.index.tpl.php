<?php /* Smarty version Smarty3-b7, created on 2016-11-15 12:03:09
         compiled from ".\templates\admin/index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:23947582acf4d7a5b24-05904028%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1a9885c5bcda6f411b3ce3673ac269dc6071d4ba' => 
    array (
      0 => '.\\templates\\admin/index.tpl',
      1 => 1479200584,
    ),
  ),
  'nocache_hash' => '23947582acf4d7a5b24-05904028',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include 'D:\PROGRAMMS\OpenServer\domains\alekses-test.ru\req\external\smarty\plugins\modifier.date_format.php';
?><?php ob_start(); ?>
	&nbsp;
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content_name"]=ob_get_clean();?>

<?php ob_start(); ?>

	<div clas="row">
			<div class="col-xs-4">
			<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Бронирование</h5>
					<?php if ($_smarty_tpl->getVariable('data_book_room_now')->value){?>
						<span class="label label-info pull-right"><?php echo $_smarty_tpl->getVariable('data_book_room_now')->value;?>
 за сегодня</span>
					<?php }?>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $_smarty_tpl->getVariable('data_book_room_all')->value;?>
</h1>
					</div>
				</div>
			</div>

			<div class="col-xs-4">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Заявки на звонок</h5>
						<?php if ($_smarty_tpl->getVariable('data_callback_now')->value){?>
							<span class="label label-info pull-right"><?php echo $_smarty_tpl->getVariable('data_callback_now')->value;?>
 за сегодня</span>
						<?php }?>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $_smarty_tpl->getVariable('data_callback_all')->value;?>
</h1>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Отзывы</h5>
						<?php if ($_smarty_tpl->getVariable('data_review_now')->value){?>
							<span class="label label-info pull-right"><?php echo $_smarty_tpl->getVariable('data_review_now')->value;?>
 за сегодня</span>
						<?php }?>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins"><?php echo $_smarty_tpl->getVariable('data_review_all')->value;?>
</h1>
					</div>
				</div>
			</div>


	</div>
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Бронирование</h5>
			<span class="label label-info pull-right">последние <?php echo count($_smarty_tpl->getVariable('data_book_room')->value);?>
</span>
		</div>
		<div class="ibox-content">
			<?php if ($_smarty_tpl->getVariable('data_book_room')->value){?>
			<table class="table table-striped">
				<tr>
					<th>Дата</th>
					<th>Имя</th>
					<th>E-mail</th>
					<th>Телефон</th>
					<th>Номер</th>
					<th>Комментарий</th>
				</tr>
				<?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_book_room')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
				<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']<=5){?>
				<tr>
					<td>
						<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d.%m.%Y");?>
<br/>
						<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%H:%M:%S");?>

					</td>
					<td><?php if ($_smarty_tpl->getVariable('cur')->value['name']){?><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
<?php }else{ ?>не указано<?php }?></td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['email'];?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['phone'];?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['name_room'];?>
<br/><?php echo $_smarty_tpl->getVariable('cur')->value['price_room'];?>
 руб/сут</td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</td>
				</tr>
				<?php }?>
				<?php }} ?>
			</table>
			<?php }else{ ?>
			0
			<?php }?>
		</div>
	</div>

	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Заявки на звонок</h5>
			<span class="label label-info pull-right">последние <?php echo count($_smarty_tpl->getVariable('data_callback')->value);?>
</span>
		</div>
		<div class="ibox-content">
			<?php if ($_smarty_tpl->getVariable('data_book_room')->value){?>
			<table class="table table-striped">
				<tr>
					<th>Дата</th>
					<th>Имя</th>
					<th>Телефон</th>
					<th>Комментарий</th>
				</tr>
				<?php  $_smarty_tpl->tpl_vars['cur'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('data_callback')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']=0;
if (count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['cur']->key => $_smarty_tpl->tpl_vars['cur']->value){
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['loop']['iteration']++;
?>
				<?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['loop']['iteration']<=5){?>
				<tr>
					<td>
						<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%d.%m.%Y");?>
<br/>
						<?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('cur')->value['date'],"%H:%M:%S");?>

					</td>
					<td><?php if ($_smarty_tpl->getVariable('cur')->value['name']){?><?php echo $_smarty_tpl->getVariable('cur')->value['name'];?>
<?php }else{ ?>не указано<?php }?></td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['phone'];?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('cur')->value['text'];?>
</td>
				</tr>
				<?php }?>
				<?php }} ?>
			</table>
			<?php }else{ ?>
			0
			<?php }?>
		</div>
	</div>
		
<?php  $_smarty_tpl->smarty->_smarty_vars['capture']["content"]=ob_get_clean();?>

<?php $_template = new Smarty_Internal_Template("admin/common/base_page.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
