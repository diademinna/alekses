{capture name="content"}
	{assign var="page_title" value="Ошибка 404. Error 404."}
    <div class="l-content content">
        <div class="l-layout">
            <div class="no-pages">
                <h2 class="gui-h2">Данной страницы не существует</h2>
                <div class="no-pages__text">
                <p>Нам очень жаль, но данная страница не активна или находится в разработке, нажмите кнопку "назад" окна вашего браузера</p><p>Попробуйте перейти по данной ссылке позже.</p>
                </div>
            </div>
        </div>
    </div>
	 
{/capture}
{include file="common/base_page.tpl"}