<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{if $page_title}{$page_title}{else}Crazy English - Школа английского языка в Пензе{/if}</title>	
	<link type="image/x-icon" href="/favicon.ico" rel="icon">
	<link type="text/css" href="/css/admin/font-awesome.min.css" rel="stylesheet" />
	<link href="/css/common.css" rel="stylesheet" type="text/css" />
	<meta name='yandex-verification' content='7bdf2a7dab41fa48' />
	<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
	
	<script type="text/javascript" src="/js/highslide/highslide-full.packed.js"></script>
	<script type="text/javascript" src="/js/highslide/highslide.config.js" charset="utf-8"></script>
	<link rel="stylesheet" type="text/css" href="/js/highslide/highslide.css" />
	
	<!--SLICK BEGIN-->
	<link rel="stylesheet" type="text/css" href="/js/slick/slick.css" />
	<link rel="stylesheet" type="text/css" href="/js/slick/slick-theme.css" />
	<script type="text/javascript" src="/js/slick/slick.min.js" charset="utf-8"></script>
	<!--SLICK END-->
	
	<!--  v1.11  -  для датапикера datepicker и sortable -->
	<link type="text/css" href="/js/ui/css/jquery-ui_kalendar.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/ui/jquery-ui.min.js"></script>
	
	<script src="/js/jquery.scrollTo.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		if (navigator.userAgent.indexOf ("MSIE 6") != -1 || navigator.userAgent.indexOf ("MSIE 7") != -1  || navigator.userAgent.indexOf ("MSIE 8") != -1 ){
	        window.location.href = "/index_ie/";
		}	
	</script>

	{if $ajaxCode}{$ajaxCode}{/if}
</head>

<body>
	<div id="ToTop" class="go-up" title="Вверх"></div>
	
	
		
	{*
	<!--  UI -->
		<!--  v1.11  -  для датапикера datepicker и sortable
		<link type="text/css" href="/js/ui/css/jquery-ui.css" rel="stylesheet" />
		<script type="text/javascript" src="/js/ui/jquery-ui.min.js"></script>-->
	*}

	
	<!--  ЛОГИНИЗАЦИЯ -->
	
	{$smarty.capture.content}

	<!--©Copyright 2014{assign var="footer_cur_year" value=$smarty.now|date_format:"%Y"}{if $footer_cur_year != '2014'}-{$footer_cur_year}{/if}-->

	
	
</html>