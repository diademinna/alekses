{capture name="content"}
	<div class="l-content content">
		<div class="l-layout">
			<div class="page">
				<h2 class="gui-h2">{$data.name}</h2>
				
				<div class="user_content page__text">
					{$data.text}
					{if $data.id == 2}
				<a href="/residence_rules/">Правила проживания в гостинице Алексес</a><br/>
				<a href="/contract_offer/">Договор оферты</a><br/>
				<a href="/fire_safety_regulations/">Памятка по пожарной безопасности</a>
				{/if}
				</div>
				
			</div>
		</div>
	</div>
		
{/capture}

{include file="common/base_page.tpl"}