{capture name="content"}

{literal}

<script type="text/javascript">	
	jQuery(function($){
		$.datepicker.regional['ru'] = {
			closeText: 'Закрыть',
			prevText: '',
			nextText: '',
			currentText: 'Сегодня',
			monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
			'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
			'Июл','Авг','Сен','Окт','Ноя','Дек'],
			dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
			dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
			dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
			dateFormat: 'yy-mm-dd', firstDay: 1,
			isRTL: false};
		$.datepicker.setDefaults($.datepicker.regional['ru']);
	}); 

	$(document).ready(function(){
		$('#dob').datepicker({dateFormat:'dd-mm-yy', changeMonth:true, changeYear:true, yearRange: "1950:2020" });			
	});
</script>
{/literal}	


		<h1>Редактировать / Личный кабинет</h1>
				
		{include file="common/errors_block.tpl"}
		
		{if $smarty.get.note=='save'}<div style="text-align:center;font-size:14px;"><b>Сохранено</b></div><br />{/if}
				
		<form action="" method="post" enctype="multipart/form-data">
			<table class="forma">
				<tr>
					<td>Ваш логин:</td>
					<td><b>{$data.login}</b></td>
				</tr>
				
				<tr>
					<td>ФИО:</td>
					<td><input class="text" type="text" name="name" value="{$data.name}" /></td>
				</tr>
				
				<tr>
					<td>E-mail:</td>
					<td><input class="text" type="text" name="email" value="{$data.email}" /></td>
				</tr>
				
				<tr>
					<td>Аватар:</td>
					<td>
						<input id="file_obzor" class="obzor" type="file" size="28" name="avatar" />						
					</td>
				</tr>
				
				{if $data.avatar}
				<tr>
					<td></td>
					<td><img style="border:1px solid #A0A0AF;" src="/uploaded/user/{$data.id}.{$data.avatar}?{$rand_new}"/></td>
				</tr>
				{/if}
				
			
				<tr>
					<td>Пол:</td>
					<td>
						<select name="sex">
							<option value="0" {if !$data.sex}selected{/if}>не указан</option>
							<option value="1" {if $data.sex == 1}selected{/if}>мужской</option>
							<option value="2" {if $data.sex == 2}selected{/if}>женский</option>
						</select>				
					</td>
				</tr>
				
				<tr>
					<td>Дата рождения:</td>
					<td>
						<input class="text" type="text" name="dob" id="dob" value="{if $data.dob!='0000-00-00' AND $data.dob}{$data.dob}{/if}" /></td>					
				</tr>
				
				<tr>
					<td>Город:</td>
					<td><input class="text" type="text" name="city" value="{$data.city}" /></td>
				</tr>
								
				<tr>
					<td>Сайт:</td>
					<td><input class="text" type="text" name="site" value="{$data.site}" /></td>
				</tr>
				
				<tr>
					<td>Skype:</td>
					<td><input class="text" type="text" name="skype" value="{$data.skype}" /></td>
				</tr>
				
				<tr>
					<td>Icq:</td>
					<td><input class="text" type="text" name="icq" value="{$data.icq}" /></td>
				</tr>
				
				<tr>
					<td>Телефон:</td>
					<td><input class="reg" type="text" name="tel" value="{$data.tel}" /></td>
				</tr>
				
				
				<tr>
					<td>Адрес:</td>
					<td><textarea name="address" id="address">{$data.address}</textarea></td>
				</tr>
								
				<tr>
					<td>О себе:</td>
					<td><textarea name="about" id="about">{$data.about}</textarea></td>
				</tr>
				
				<tr>
					<td></td>
					<td class="formvalue"><br />
						<input type="hidden" name="submitted" value="1" />
						<input type="hidden" name="photo_type" value="{$data.avatar}" />
						<input type="image" src="/img/btn_send.png" class="but" />
				</td></tr>
			</table>
		</form>


{/capture}

{include file="common/base_page.tpl"}











