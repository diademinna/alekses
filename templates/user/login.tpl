{capture name="content"}
	<h1>Войти</h1>

	{include file="common/errors_block.tpl"}<br />

	<form class="forma" action="" method="post" enctype="multipart/form-data">
		<table class="forma">
		  <tr>
			<td>Логин:</td>
			<td><div class="input_bgr"><input name="login" type="text"  value="{$login}" class="reg" /></div></td>
		  </tr>

		  <tr>
			<td>Пароль:</td>
			<td><div class="input_bgr"><input name="password" type="password" class="text" /></div></td>
		  </tr>			  

		  <tr>
			<td></td>
			<td><input type="checkbox" class="checkbox" name="remember" id="remember_login" checked> <label for="remember_login">Запомнить меня</label></td>
		  </tr>

		  <tr>
			<td></td>
			<td>
				<input type="hidden" name="submitted" value="1"/>
				<input type="image" src="/img/btn_send.png" class="but" />
			</td>
		  </tr>
		</table>
	</form>	



	{if !$user}
		<div style="clear:both;text-align:center;">
			<br/><br/>
			<span>Если вы ещё не зарегистрированы, пройдите процедуру <a href="/registration/">регистрации</a></span>
			<br/><br/>
		</div>
	{/if}

{/capture}

{include file="common/base_page.tpl"}