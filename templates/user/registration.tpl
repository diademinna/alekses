{capture name="content"}

	<h1>Регистрация</h1>
		
	{include file="common/errors_block.tpl"}

	<form name="regform" method="post" action="">
        <table class="forma">
        
		  <tr>
			<td colspan="2"><p><span class="red">(*)</span> - отмечены поля обязательные для заполнения</p></td>
		  </tr>

		  <tr>
			<td>Логин: <span class="red">*</span></td>
			<td><div class="input_bgr"><input name="login" type="text"  value="{$login}" /></div></td>
		  </tr>
		  
		  <tr>
			<td>Пароль: <span class="red">*</span></td>
			<td><div class="input_bgr"><input name="reg_password" type="password"  /></div></td>
		  </tr>
		  
		  <tr>
			<td>Подтверждение пароля: <span class="red">*</span></td>
			<td><div class="input_bgr"><input name="reg_password2" type="password" maxlength="50"  /></div></td>
		  </tr>
		  
		  <tr>
			<td>ФИО: </td>
			<td><div class="input_bgr"><input name="fio" type="text"  value="{$fio}" /></div></td>
		  </tr>
		  
		  <tr>
			<td>E-mail: <span class="red">*</span></td>
			<td><div class="input_bgr"><input name="email" type="text"  value="{$email}" /></div></td>
		  </tr>
		  
		  <tr>
		    <td></td>
			<td>
				<input type="image" src="/img/btn_send.png" class="but" />
				<input type="hidden" name="submitted" value="1"/>
				<input type="hidden" name="registr" value="1"/>
			</td>
		  </tr>
		  
		</table>
    </form>

{/capture}

{include file="common/base_page.tpl"}