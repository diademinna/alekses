{capture name="content"}

<h1>Восстановление пароля</h1>

	{include file="common/errors_block.tpl"}<br/>
			
	<form action="" method="post" enctype="multipart/form-data">
	
		<table class="forma">
			<tr>
				<td>Ваш E-mail: </td>
				<td><div class="input_bgr"><input type="text" name="email" value="{$email}" class="text" /></div></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="image" src="/img/btn_send.png" class="but" />
					<input type="hidden" name="submitted" value="1"/>
				</td>
			</tr> 
		</table>
			
	</form>	

	
	<br/><br/>
	<p style="text-align:center;font-size:14px;font-weight:bold;">{if $result}{$result}{/if}</p>
	<br/><br/>

{/capture}

{include file="common/base_page.tpl"}