{capture name="content"}

	<h1>Смена пароля</h1>	

	{include file="common/errors_block.tpl"}
	<br/>

	{if $smarty.get.note=='save'}
		<div style="text-align:center;"><b>Пароль изменен!</b></div><br>
	
	{else}
		<form action="" method="post" class="common_form">
				
			<table class="forma">
				<tr>
					<td>Текущий пароль: </td>
					<td><div class="input_bgr"><input type="text" class="text"  name="cur_password" value="{$cur_password|default:''}"/></div></td>
				</tr>

				<tr>
					<td>Новый пароль: </td>
					<td><div class="input_bgr"><input type="password" class="text" name="new_password" value="{$new_password|htmlspecialchars}"/></div></td>
				</tr>

				<tr>
					<td>Повтор нового пароля: </td>
					<td><div class="input_bgr"><input type="password" class="text" name="new_password_rep" value="{$new_password_rep|htmlspecialchars}"/></div></td>
				</tr>

				<tr>
					<td></td>
					<td>	      
						<input type="image" src="/img/btn_send.png" class="but">  
						<input type="hidden" name="submitted" value="1"/>
					</td>
				</tr>
			</table>
		
		</form>
	{/if}
	
{/capture}

{include file="common/base_page.tpl"}