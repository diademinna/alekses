{capture name="content"}
<div class="main-banner">
    <img class="main-banner__image" src="/_media/banner.jpg" />
    <div class="main-banner__wrapp">
        {if $data_main_text.text}
        <div class="main-banner__text">{$data_main_text.text}
            <a class="gui-button main-banner__button JS-Popup" href="#popup-book">Забронировать</a>
        </div>
        {/if}
    </div>
</div>
<div class="l-content content">
    <div class="l-layout">
        <div class="main">
            {if $data_room_main}
            <div class="main-rooms">
                <h2 class="gui-h2">Номера гостиницы</h2>
                <div class="main-rooms__wrap">
                    <div class="row">
                        {foreach from=$data_room_main item=cur}
                        <div class="col-xs-4">
                            <div class="room">
                                <div class="room__content">
                                    <div class="room-name"><a class="room-name__link" href="/room/{$cur.id}/">{$cur.name_category} {$cur.name}</a></div>
                                    {if $cur.ext}<img class="room__image" alt="{$cur.name}" src="/uploaded/room/{$cur.id}_prev.{$cur.ext}" />{else}<img class="room__image" src="/images/no-image_prev.png" />{/if}
                                    <div class="room__anons">{$cur.seats}-местный, {$cur.apartment}-комнатный</div>
                                    <div class="room__description">{$cur.anons}</div>
                                    <div class="room-price"><span class="room-price__coast">{$cur.price} </span>руб./сут.</div>
                                    <!--<button class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</button>-->
                                    <a class="gui-button" href="/room/{$cur.id}/">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            {/if}
            {if $data_stock}
            <div class="main-actions JS-Scrolling-Block" data-scrolling-id="services">
                <h2 class="gui-h2">Акции</h2>
                <div class="main-actions__wrapp">
                {foreach from=$data_stock item=cur name=loop}
                    <div class="actions JS-Scrolling-Block" data-scrolling-id="service-{$cur.id}">
                            {if $smarty.foreach.loop.iteration%2 != 0}
                            <div class="actions-image">
                                <img class="actions-image__img" alt="{$cur.name}" src="/uploaded/stock/{$cur.id}_sm.{$cur.ext}" />
                            </div>
                                <div class="actions-text actions-text_right">
                                    <div class="actions-text__name">{$cur.name}</div>
                                    <div class="actions-text__anons">{$cur.text}</div>
                                    <div class="actions-text__text">{$cur.participation}</div>
                                </div>
                            {else}
                                 <div class="actions-text actions-text_left">
                                    <div class="actions-text__name">{$cur.name}</div>
                                    <div class="actions-text__anons">{$cur.text}</div>
                                    <div class="actions-text__text">{$cur.participation}</div>
                                </div>
                                <div class="actions-image"><img class="actions-image__img actions-image__img_left" alt="{$cur.name}" src="/uploaded/stock/{$cur.id}_sm.{$cur.ext}" /></div>
                            {/if}
                    </div>
                {/foreach}
                </div>
            </div>
            {/if}
            {if $data_service}
            <div class="main-service">
                <h2 class="gui-h2">Дополнительные услуги</h2>
                <div class="main-service__wrapp">
                    <div class="row">
                        {foreach from=$data_service item=cur}
                        <div class="col-xs-4">
                            <div class="service">
                                <img alt="{$cur.name}" src="/uploaded/service/{$cur.id}.png" />
                                <div class="service__name">{if $cur.id==1}<a class="service__link" href="/contacts/">{/if}{$cur.name}</a></div>
                                <div class="service__text">{$cur.text}</div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
                {/if}
            </div>
        </div>
    </div>
</div>
{/capture}

{include file="common/base_page.tpl"}