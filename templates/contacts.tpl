{capture name="content"}

	{literal}
    <script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init); // Как только будет загружен API и готов DOM, выполняем инициализацию

        function init () {
            // Создание экземпляра карты и его привязка к контейнеру с заданным id ("map")
            var myMap = new ymaps.Map('map', {
                // При инициализации карты, обязательно нужно указать ее центр и коэффициент масштабирования
                center: [{/literal}{if $data_contacts.latitude && $data_contacts.longitude}{$data_contacts.latitude}, {$data_contacts.longitude}{else}53.199449, 45.020121{/if}{literal}], // Метка
//                center: [53.199449, 45.020121], // Пенза
                zoom: 14
            });

		
            // Для добавления элемента управления на карту используется поле controls, ссылающееся на коллекцию элементов управления картой.  Добавление элемента в коллекцию производится с помощью метода add().
            // В метод add можно передать строковый идентификатор элемента управления и его параметры.
            myMap.controls                
                .add('zoomControl') // Кнопка изменения масштаба
                .add('typeSelector') // Список типов карты
                .add('smallZoomControl', { right: 5, top: 75 }) // Кнопка изменения масштаба - компактный вариант.
                .add('mapTools') // Стандартный набор кнопок

            // Также в метод add можно передать экземпляр класса, реализующего определенный элемент управления.
            // Например, линейка масштаба ('scaleLine')
            myMap.controls.add(new ymaps.control.ScaleLine())
           		
			
			myPlacemark = new ymaps.Placemark([{/literal}{if $data_contacts.latitude && $data_contacts.longitude}{$data_contacts.latitude}, {$data_contacts.longitude}{else}53.199449, 45.020121{/if}{literal}], 
				{balloonContentBody: '{/literal}{$data_contacts.name_on_map|escape:'quotes'}{literal}'}
				
			);
				
//			myPlacemark.options.set({
//				iconImageHref: '/img/map_ico.png',
//				iconImageSize: [60, 94],
//				iconImageOffset: [-29, -94]
//			});
				
			myMap.geoObjects.add(myPlacemark);
			myPlacemark.balloon.open();  // открыть сразу балун
			
        }
    </script>
	{/literal}


	<div class="l-content content">
    <div class="l-layout">
        <div class="contacts">
            <h2 class="gui-h2">Контакты</h2>
            <div class="contacts-content">
                <div class="row">
                    <div class="col-xs-4">
                    {if $data_contacts.address}
                        <div class="contacts-content__address">
                            {$data_contacts.address}
                        </div>
                    {/if}
                    {if $data_contacts.phone}
                        <div class="contacts-content__phone">
                           {$data_contacts.phone}
                        </div>
                    {/if}
                    {if $data_contacts.email}
                        <div class="contacts-content__email">
                            <a class="contacts-content__link" href="mailto:{$data_contacts.email}">{$data_contacts.email}</a>
                        </div>
                    {/if}
                    {if $data_contacts.skype}
                        <div class="contacts-content__skype">
                            <a class="contacts-content__link" href="skype:{$data_contacts.skype|replace:' ':''}?call">{$data_contacts.skype}</a>
                        </div>
                    {/if}
                    {if $data_contacts.viber}
                        <div class="contacts-content__viber">
                            <a class="contacts-content__link" href="viber://tel:{$data_contacts.viber|replace:' ':''}">{$data_contacts.viber}</a>
                        </div>
                    {/if}
                    {if $data_contacts.whatsapp}
                        <div class="contacts-content__whatsapp">
                            <a class="contacts-content__link" href="tel:{$data_contacts.whatsapp|replace:' ':''}">{$data_contacts.whatsapp}</a>
                        </div>
                    {/if}
                        <div class="contacts-content__wrap">
                            <div class="contacts-content__taxi">
                                Такси до гостиницы совершенно бесплатно
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div id="map" style="width:100%;height:400px;"></div>
                    </div>
                </div>
                <div class="contacts-content__texttaxi user_content">{$data_contacts.taxi}</div>
            </div>
            <h2 class="gui-h3">В непосредственной близости находится</h2>
            <ul class="contacts-near">
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Пешеходная улица Московская" src="/images/icons/contacts-1.jpg" />
                    <div class="contacts-near__text">Пешеходная улица Московская</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Пензенский драматический театр им. Луначарского" src="/images/icons/contacts-2.jpg" />
                    <div class="contacts-near__text">Пензенский драматический театр<br/>им. Луначарского</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Центральный парк культуры и отдыха им. Белинского" src="/images/icons/contacts-3.jpg" />
                    <div class="contacts-near__text">Центральный парк культуры и отдыха<br/>им. Белинского</div>
                </li>
                <li class="contacts-near__item">
                    <img class="contacts-near__image" alt="Торгово-развлекательный центр 'Высшая лига'" src="/images/icons/contacts-4.jpg" />
                    <div class="contacts-near__text">Торгово-развлекательный центр "Высшая лига"</div>
                </li>
            </ul>
        </div>
    </div>
</div>
	
{/capture}


{include file="common/base_page.tpl"}