{capture name="content"}
<div class="l-content content">
    <div class="l-layout">
        <div class="reviews">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item">Отзывы</li>
            </ul>
            <h2 class="gui-h2">Отзывы наших гостей</h2>  
            <div class="reviews__button">
                <a href="#popup-review" class="gui-button JS-Popup" >Оставить отзыв</a>  
            </div>
            <div class="reviews__wrap">
                <div class="row">
                    {foreach from=$data_review item=cur name=loop}
					<div class="col-xs-6">
                        <div class="review">
                            <div class="review__name">{$cur.name}</div>
                            <div class="review__info">
                                {if $cur.rating}
                                    <ul class="review-rating">
                                        {for $var=1 to 5}
                                            <li class="review-rating__item {if $var<=$cur.rating}review-rating__item_check{/if}"></li>
                                        {/for}
                                    </ul>
                                {/if}
                                <div class="review__date"> 
									{$cur.date|date_format:"%d "}
									{assign var="num_month" value={$cur.date|date_format:"%m"}}
									{$MN.$num_month}
									{$cur.date|date_format:"%Y"}
								</div>
                            </div>
                            <div class="review__text">{$cur.text}</div>
                        </div>
                    </div>
					{if $smarty.foreach.loop.iteration%2==0}
						</div>
						<div class="row">
					{/if}
				    {/foreach}
                </div>
            </div>       
        </div>
        {if $pager_string}<div class="pager_string">{$pager_string}</div>{/if}
    </div>
</div>


<!--<div class="reviews">
    <div class="l-layout">
        <ul class="breadcrumbs">
            <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/"><i class="icon-home"></i>Главная</a></li>
            <li class="breadcrumbs__item breadcrumbs__item_separate"><i class="icon-right-open-big"></i></li>
            <li class="breadcrumbs__item">Отзывы</li>
        </ul>
        <h1 class="gui-h1">Отзывы наших клиентов</h1>
        <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
                <div class="reviews-actions">
                    <a class="gui-button JS-Popup" href="#popup-give-review">Оставить отзыв</a>
                    <a class="reviews-actions__order JS-Popup" href="#popup-order-service">Заказать работу</a>
                </div>
            </div>
        </div>
        <div class="reviews-container">
            <div class="row">
				{foreach from=$data_review item=cur name=loop}
					<div class="col-xs-6">
                        <div class="review">
                            <div class="review__name">{$cur.name}</div>
                            <div class="review__info">
                                {if $cur.rating}
                                    <ul class="review-rating">
                                        {for $var=1 to 5}
                                            <li class="review-rating__item {if $var<=$cur.rating}review-rating__item_check{/if}"></li>
                                        {/for}
                                    </ul>
                                {/if}
                                <div class="review__date"> 
									{$cur.date|date_format:"%d "}
									{assign var="num_month" value={$cur.date|date_format:"%m"}}
									{$MN.$num_month}
									{$cur.date|date_format:"%Y"}
								</div>
                            </div>
                            <div class="review__text">{$cur.text}</div>
                        </div>
                    </div>
					{if $smarty.foreach.loop.iteration%2==0}
						</div>
						<div class="row">
					{/if}
				{/foreach}
            </div>
        </div>
        {if $pager_string}<div class="pager_string">{$pager_string}</div>{/if}
    </div>-->
	
{/capture}

{include file="common/base_page.tpl"}