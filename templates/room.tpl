{capture name="content"}
<div class="l-content content">
    <div class="l-layout">
        {if $data}
        <div class="rooms">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item">Номера и цены</li>
            </ul>
            <h2 class="gui-h2">Номера и цены</h2>
            <div class="rooms-list">
                {foreach from=$data item=cur}
                    <h3 class="gui-h3 rooms-list__subtitle">{$cur.name_category}</h3>
                        {if $cur.rooms}
                        <div class="row">
                            {foreach from=$cur.rooms item=cur2 name=loop2}
                            <div class="col-xs-4">
                                <div class="room">
                                    <div class="room__content">
                                        <div class="room-name"><a class="room-name__link" href="/room/{$cur2.id}/">{$cur2.name}</a></div>
                                        {if $cur2.ext}<img class="room__image" alt="{$cur.name}" src="/uploaded/room/{$cur2.id}_prev.{$cur2.ext}" />{else}<img class="room__image" src="/images/no-image_prev.png"/>{/if}
                                        <div class="room__anons">{$cur2.seats}-местный, {$cur2.apartment}-комнатный</div>
                                        <div class="room__description">{$cur2.anons}</div>
                                        <div class="room-price"><span class="room-price__coast">{$cur2.price} </span>руб./сут.</div>
                                        <!--<button class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</button>-->
                                        <a class="gui-button " href="/room/{$cur2.id}/">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            {if $smarty.foreach.loop2.iteration%3==0}</div><div class="row">{/if}
                            {/foreach}
                        </div>
                        {/if}
                {/foreach}
            </div>
        </div>
        {else if $data_item}
        <div class="detailed">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a></li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/room/">Номера и цены</a></li>
                <li class="breadcrumbs__item">{$data_item.name}</li>
            </ul>
            <h2 class="gui-h2 name_room">{$data_item.name_category} {$data_item.name}</h2>
            <div class="detailed-content">
                <input type="hidden" name="name_room_select" value="{$data_item.name_category} {$data_item.name}">
                <input type="hidden" name="price_room_select" value="{$data_item.price}">
                <input type="hidden" name="id_room_select" value="{$data_item.id}">
                <div class="detailed-content__photo">
                    {if $data_item.ext || $data_photo}
                    <div class="royalSlider main-results__slider rsDefault JS-RoyalSlider">
                        {if $data_item.ext}
                        <a class="rsImg" data-rsw="435" data-rsh="315"  data-rsbigimg="/uploaded/room/{$data_item.id}.{$data_item.ext}" href="/uploaded/room/{$data_item.id}_big.{$data_item.ext}">
                            <img class="rsTmb" src="/uploaded/room/{$data_item.id}_sm.{$data_item.ext}" height="75" width="105">
                        </a>
                        {/if}
                        {if $data_photo}
                            {foreach from=$data_photo item=cur}
                            <a class="rsImg" data-rsw="{$cur.width_big}" data-rsh="{$cur.height_big}"  data-rsbigimg="/uploaded/room/{$data_item.id}/{$cur.id}.{$cur.ext}" href="/uploaded/room/{$data_item.id}/{$cur.id}_big.{$cur.ext}">
                                <img class="rsTmb" src="/uploaded/room/{$data_item.id}/{$cur.id}_sm.{$cur.ext}" height="{$cur.height_sm}" width="{$cur.width_sm}">
                            </a>
                            {/foreach}
                        {/if}
                    </div>
                    {else}
                        <img src="/images/no-image_prev.png" />
                    {/if}
                </div>
                <div class="detailed-content__description">
                    {if $data_item.attribute}
                    <ul class="detailed-attribute">
                        {foreach from=$data_item.attribute item=cur}
                        <li class="detailed-attribute__item">
                            <img class="detailed-attribute__image" src="/uploaded/attribute/{$cur.id_attribute}.{$cur.ext}" />
                            <div class="detailed-attribute__text">{$cur.name_attribute}</div>
                        </li>
                        {/foreach}
                    </ul>
                    {/if}
                    <div class="detailed-content__seats">{$data_item.seats}-местный, {$data_item.apartment}-комнатный</div>
                    {if $data_item.area}<div class="detailed-content__area">Площадь номера: {$data_item.area} кв.м.</div>{/if}
                    <div class="detailed-content__text">{$data_item.text}</div>
                    {if $data_info[5].text}
                     <div class="detailed-content__phone">
                        <a class="detailed-content__link" href="tel:{$data_info[5].text|replace:' ':''}">{$data_info[5].text}</a>
                    </div>
                    {/if}
                    {if $data_info[7].text}
                    <div class="detailed-content__email">
                        <a class="detailed-content__link" href="mailto:{$data_info[7].text}">{$data_info[7].text}</a>
                    </div>
                    {/if}
                    <div class="detailed-content__actions">
                        <div class="detailed-content__price">
                            <span class="detailed-content__cost">{$data_item.price}</span> руб./сут.
                        </div>
                        <div class="detailed-content__button"><a class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</a></div>
                    </div>
                </div>
            </div>
                {if $data_rooms}
                <h2 class="gui-h2">Другие номера</h2>
                <div class="detailed-other">
                    <div class="row">
                        {foreach from=$data_rooms item=cur}
                        <div class="col-xs-4">
                            <div class="room">
                                <div class="room__content">
                                    <div class="room-name"><a class="room-name__link" href="/room/{$cur.id}/">{$cur.name_category} {$cur.name}</a></div>
                                    {if $cur.ext}<img class="room__image" alt="{$cur.name}" src="/uploaded/room/{$cur.id}_prev.{$cur.ext}" />{else}<img class="room__image" src="/images/no-image_prev.png" />{/if}
                                    <div class="room__anons">{$cur.seats}-местный, {$cur.apartment}-комнатный</div>
                                    <div class="room__description">{$cur.anons}</div>
                                    <div class="room-price"><span class="room-price__coast">{$cur.price} </span>руб./сут.</div>
                                    <!--<a class="gui-button JS-Popup book-room" href="#popup-book">Забронировать</a>-->
                                    <a class="gui-button" href="/room/{$cur.id}/">Подробнее</a>
                                </div>
                            </div>
                        </div>
                        {/foreach}
                    </div>
                </div>
                {/if}
            </div>
        {/if}
    </div>
</div>
{/capture}

{include file="common/base_page.tpl"}