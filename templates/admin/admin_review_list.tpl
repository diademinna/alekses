{capture name="content_name"}
	Отзывы
{/capture}

{capture name="content"}

    <script type="text/javascript">
   
       function delRecord(page, id, get_param){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/review/list/"+page+"/delete/"+id+"/"+get_param;
			}
		}			
       
    </script>
    {if $data}
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/review/add/{if $get_param}{$get_param}{/if}">
                    <i class="fa fa-plus"></i> Добавить отзыв</a>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">
                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '{$get_param}');">
                        {include file="admin/common/select_count_page.tpl"}
                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Оценка</th>
                            <th>Текст</th>
                            <th>На сайте</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$data item=cur name=loop}
                            <tr id="item_{$cur.id}">
                                <td>
                                {$cur.date|date_format:"%d.%m.%Y"}<br/>
                                {$cur.date|date_format:"%H:%M:%S"}
                                </td>
                                <td>{$cur.name}</td>
                                <td>{$cur.email}</td>
                                 <td>
                                 <ul class="review-rating">
                                    {for $var=1 to 5}
                                        <li class="review-rating__item {if $var<=$cur.rating}review-rating__item_check{/if}"></li>
                                    {/for}
                                </ul>
                                 </td>
                                 <td>{$cur.text|truncate:120:"..."}</td>
                                <td>
                                    <div class="checkbox">
                                        <input id="male{$cur.id}" type="checkbox" name="my_checkbox" value="{$cur.active}" {if $cur.active==1}checked{/if} onclick="xajax_Activate('{$cur.id}')">
                                        <label class="label_checkbox" for="male{$cur.id}"></label>
                                    </div>
                                </td>
                                <td style="font-size:18px;">
                                    <a href="/admin/review/add/{$page}/edit/{$cur.id}/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('{$page}', '{$cur.id}', '{$get_param}');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                {assign var="min_pos" value=$cur.pos}
                            </tr>
                    {/foreach}
                    </tbody>
                    <input type="hidden" id="min_pos" value="{$min_pos}">
                </table>
            </div>
        </div>  
        {if $pager_string}<div class="pager">{$pager_string}</div>{/if}        
    </div>
</form>     

{else}
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/review/add/{if $get_param}{$get_param}{/if}">
            <i class="fa fa-plus"></i>	Добавить отзыв</a>
        </div>
    </div>

{/if}
	
{/capture}

{include file="admin/common/base_page.tpl"}