{capture name="content_name"}
	Добавить/Редактировать Изображения
{/capture}

{capture name="content"}
	{literal}
		<script type="text/javascript">
			function del(id_pages, id_image)	{
				if(confirm("Вы уверены?")) {
					xajax_deleteImage(id_pages, id_image);
				}
			}
				
			$(function() {			
				$( "#sortable" ).sortable({
					opacity: 0.8,
					revert: true,
					axis:'y'
				});
			});		
			$(document).ready(function() { 
				$("#sortable").sortable({

					  update : function () { 
						var mass_sort = $('#sortable').sortable('toArray');				      
						xajax_Sort(mass_sort); 
					  }
				});
			});
		</script>
	{/literal}
	<div class="ibox-content">
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
		{if $smarty.get.save}<b style="color:#4BB43F;">СОХРАНЕНО</b><br>{/if}
		<a href="/admin/pages/list/{$parent_id}/">Назад</a>
		<div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-10">
                <input name="name_img" id="name_img" class="form-control" type="text" value="" />
            </div>
        </div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Мультизагрузка* :</label>
			<div class="col-xs-2">
				<select name="type_resize" id="type_resize">
					<option value="1">Обрезать края</option>
					<option value="2">Добавлять пустые поля</option>
				</select>
				
				<br /><br />
				
				<div id="spanButtonPlaceHolder"></div><br/>
				<div id="divStatus"></div><br/>
				<div id="fsUploadProgress" style="color:#777;"></div>
				<a id="btnCancel" href="#cancel"></a>
			</div>
		</div>
		<input type="hidden" name="id_pages" id="id_pages" value="{$id_pages}" />
	</form>
	</div>
				
				
	{literal}
	<script type="text/javascript" src="/js/SWFUpload/swfupload.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/swfupload.queue.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/fileprogress.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/handlers.js"></script>
	<script type="text/javascript">
		var swfu;
		var myQueueComplete = function () {
			window.location.reload();
		};
			
		function uploadStart() {	// функция вызывается перед загрузкой фоток				
			this.setPostParams({"id_from_swfupload": $("#id_pages").val(),								
								"type_resize": $("#type_resize").val(),
								"name_img": $("#name_img").val()
								});
			return true;
		}
		
		window.onload = function() {
			
			var settings = {
				flash_url : "/js/SWFUpload/swfupload.swf",
				upload_url: "/admin/pages_photo/add/{/literal}{$page}{literal}/{/literal}{$id_pages}{literal}/?swfupload=1",
				//post_params: { "id_from_swfupload": '{/literal}{$id_pages}{literal}'},
				file_size_limit : "15 MB",
				file_types : "*.jpg; *.png; *.jpeg; *.gif",
				file_types_description : "Images",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings	
				button_image_url: "/js/SWFUpload/button_bgr.png",
				button_width: 100,
				button_height: 30,
				button_text_left_padding: 21,
				button_text_top_padding: 4,
				button_text : "<span class=\"uploadBtn\">Обзор...</span>",
				button_text_style : ".uploadBtn {font-size:16px; font-family:Arial; background-color:#FF0000;}",
				button_placeholder_id: "spanButtonPlaceHolder",

				// The event handler functions are defined in handlers.js
				swfupload_preload_handler : preLoad,
				swfupload_load_failed_handler : loadFailed,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : myQueueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
		};
	</script>
	{/literal}
	
	
	{if $data_photo}	
	
		<hr class="sep" />
	
		<h1>Фотогалерея</h1>
		
		<div class="sort_list gall_block">
			<div id="sortable"  class="sort">
				{foreach from=$data_photo item=cur name=loop}
					<div id="item_{$cur.id}" class="row">
						<form id="form_item_{$id_pages}" name="form_img_{$id_pages}" action="" method="post">
							<div class="col-xs-5">
									<a href="/uploaded/pages/{$cur.id_pages}/{$cur.id}.{$cur.ext}" target="_blank"><img src="/uploaded/pages/{$cur.id_pages}/{$cur.id}_sm.{$cur.ext}" alt="{$cur.name}" title="{$cur.name}" class="photo" /></a>
								<i class="icon-cancel" onclick="del('{$cur.id_pages}','{$cur.id}');" title="Удалить изображение" alt="Удалить изображение" style="cursor:pointer;" ></i><br><br>
							</div>

							<div class="col-xs-3">									
								<textarea  id="name_img_{$cur.id}" name="name_img_{$cur.id}" >{$cur.name}</textarea>
								<a href="" onclick="xajax_reSaveName(document.getElementById('name_img_{$cur.id}').value, {$cur.id}); return false;"><i class="icon-floppy" title="Сохранить название" alt="Сохранить название" ></i></a>								
							</div>
						</form>
					</div>
				{/foreach}
			</div>
		</div>
			
	{/if}
	

{/capture}

{include file="admin/common/base_page.tpl"}