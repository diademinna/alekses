{capture name="content_name"}
	&nbsp;
{/capture}

{capture name="content"}

	<div clas="row">
			<div class="col-xs-4">
			<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Бронирование</h5>
					{if $data_book_room_now}
						<span class="label label-info pull-right">{$data_book_room_now} за сегодня</span>
					{/if}
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">{$data_book_room_all}</h1>
					</div>
				</div>
			</div>

			<div class="col-xs-4">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Заявки на звонок</h5>
						{if $data_callback_now}
							<span class="label label-info pull-right">{$data_callback_now} за сегодня</span>
						{/if}
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">{$data_callback_all}</h1>
					</div>
				</div>
			</div>
			<div class="col-xs-4">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Отзывы</h5>
						{if $data_review_now}
							<span class="label label-info pull-right">{$data_review_now} за сегодня</span>
						{/if}
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">{$data_review_all}</h1>
					</div>
				</div>
			</div>


	</div>
	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Бронирование</h5>
			<span class="label label-info pull-right">последние {count($data_book_room)}</span>
		</div>
		<div class="ibox-content">
			{if $data_book_room}
			<table class="table table-striped">
				<tr>
					<th>Дата</th>
					<th>Имя</th>
					<th>E-mail</th>
					<th>Телефон</th>
					<th>Номер</th>
					<th>Комментарий</th>
				</tr>
				{foreach from=$data_book_room item=cur name=loop}
				{if $smarty.foreach.loop.iteration<=5}
				<tr>
					<td>
						{$cur.date|date_format:"%d.%m.%Y"}<br/>
						{$cur.date|date_format:"%H:%M:%S"}
					</td>
					<td>{if $cur.name}{$cur.name}{else}не указано{/if}</td>
					<td>{$cur.email}</td>
					<td>{$cur.phone}</td>
					<td>{$cur.name_room}<br/>{$cur.price_room} руб/сут</td>
					<td>{$cur.text}</td>
				</tr>
				{/if}
				{/foreach}
			</table>
			{else}
			0
			{/if}
		</div>
	</div>

	<div class="ibox float-e-margins">
		<div class="ibox-title">
			<h5>Заявки на звонок</h5>
			<span class="label label-info pull-right">последние {count($data_callback)}</span>
		</div>
		<div class="ibox-content">
			{if $data_book_room}
			<table class="table table-striped">
				<tr>
					<th>Дата</th>
					<th>Имя</th>
					<th>Телефон</th>
					<th>Комментарий</th>
				</tr>
				{foreach from=$data_callback item=cur name=loop}
				{if $smarty.foreach.loop.iteration<=5}
				<tr>
					<td>
						{$cur.date|date_format:"%d.%m.%Y"}<br/>
						{$cur.date|date_format:"%H:%M:%S"}
					</td>
					<td>{if $cur.name}{$cur.name}{else}не указано{/if}</td>
					<td>{$cur.phone}</td>
					<td>{$cur.text}</td>
				</tr>
				{/if}
				{/foreach}
			</table>
			{else}
			0
			{/if}
		</div>
	</div>
		
{/capture}

{include file="admin/common/base_page.tpl"}