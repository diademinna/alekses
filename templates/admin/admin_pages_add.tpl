{capture name="content_name"}
	Текстовые страницы / {if $data.name}Редактировать - {$data.name}{else}Добавить{/if}
{/capture}


{capture name="content"}

	
<div class="ibox-content">
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
		<input type="hidden" name="parent_id" value={$parent_id}>
        {include file="common/errors_block.tpl"}
        <div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-10">
                <input name="name" class="form-control" value="{$data.name}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Текст* :</label>
            <div class="col-sm-10">
                <textarea name="text" class="tiny" type="text" >{$data.text}</textarea>
            </div>
        </div>
		<div class="form-group">
            <label class="col-sm-2 control-label">Тайтл:</label>
            <div class="col-sm-10">
                <input name="title" class="form-control" value="{$data.title}" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>

		
{/capture}

{include file="admin/common/base_page.tpl"}