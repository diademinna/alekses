{capture name="content_name"}
	Номера / {if $data.name}Редактировать - {$data.name}{else}Добавить{/if}
{/capture}


{capture name="content"}
<div class="ibox-content">
    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        {include file="common/errors_block.tpl"}
        {if $data_room}
        <div class="form-group">
            <label class="col-sm-2 control-label">Номера :</label>
            <div class="col-sm-8">
                <div class="row">
                    {foreach from=$data_room item=cur}
                    <div class="col-xs-6">
                        <div class="radio">
                            <input id="apartment_{$cur.id}" type="radio" name="id_room" value="{$cur.id}" {if $data.id_room == $cur.id}checked{/if} onchange="getBookDate('{$cur.id}');">
                            <label class="label_radio" for="apartment_{$cur.id}">{$cur.name_category} {$cur.name} <strong>({$cur.price|cost} руб/сут)</strong></label>
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
        </div>
        {/if}
        <div id="info_book" {if !$data}style="display:none;"{/if}>
        <div class="form-group JS-RangeDate">
            <label class="col-sm-2 control-label">Дата заезда* :</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" class="form-control JS-RangeDate-Start" name="date_input" value="{$data.date_input}">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                </div>
            </div>
            <label class="col-sm-2 control-label">Дата выезда* :</label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" class="form-control JS-RangeDate-End" name="date_output" value="{$data.date_output}">
                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Телефон * :</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="phone" value="{$data.phone}">
                    <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                </div>
            </div>
            <label class="col-sm-2 control-label">Почтовый ящик* :</label>
            <div class="col-sm-3">
                <div class="input-group">
                    <input type="text" class="form-control" name="email" value="{$data.email}">
                    <div class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Комментарий :</label>
            <div class="col-sm-7">
                <textarea name="text" class="form-control" type="text">{$data.text}</textarea>
            </div>
        </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>

{literal}
	<script type="text/javascript">	
		jQuery(function($){
			$.datepicker.regional['ru'] = {
				closeText: 'Закрыть',
				prevText: '',
				nextText: '',
				currentText: 'Сегодня',
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
				'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
				'Июл','Авг','Сен','Окт','Ноя','Дек'],
				dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
				dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
				dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
				dateFormat: 'yy-mm-dd', firstDay: 1,
				isRTL: false};
			$.datepicker.setDefaults($.datepicker.regional['ru']);
		}); 

        function getBookDate(id_room) {
            $('#info_book').slideDown(500);

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/check_date_book/",
                async: false,
                data:{
                    id_room: id_room
                },
                success: function(response){
                    jQuery(window).trigger('jsChangeBookDate', [response.data || []]);
                }
            });
        }

        function initRangeDate() {
            var common = {
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true,
                minDate: new Date()
            };

            jQuery('.JS-RangeDate').not('JS-RangeDate-ready').each(function() {
                var $elem = jQuery(this).addClass('JS-RangeDate-ready'),
                    $star = $elem.find('.JS-RangeDate-Start'),
                    $end = $elem.find('.JS-RangeDate-End'),
                    bookDate = [];

                jQuery(window).on('jsChangeBookDate', function(event, data) {
                    bookDate = data;
                    
                    var option = {
                        minDate: new Date(),
                        maxDate: null,
                        beforeShowDay: function(date) {
                            if (bookDate.length) { 
                                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                                if (bookDate.indexOf(string) === -1) {
                                    return [true];
                                } else {
                                    return [false, 'book'];
                                }
                            } else {
                                return [true];
                            }
                        } 
                    };

                    $star.datepicker("option", option);
                    $end.datepicker("option", option);
                    $star.datepicker("setDate", null);
                    $end.datepicker("setDate", null);
                });

                 $star.datepicker(jQuery.extend({}, common, {
                     onSelect: function() {
                         $end.datepicker( "option", "minDate", $star[0].value );
                     }
                 }));

                 $end.datepicker(jQuery.extend({}, common, {
                     onSelect: function() {
                         $star.datepicker( "option", "maxDate", $end[0].value );
                     }
                 }));
            });
        }

        $(document).ready(function(){
            initRangeDate();
        });
	</script>
{/literal}

{/capture}

{include file="admin/common/base_page.tpl"}