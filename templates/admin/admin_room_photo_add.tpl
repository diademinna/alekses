{capture name="content_name"}
	Добавить/Редактировать Изображения <br/>
	<span style="font-size:16px;font-weight:700;">{$data_photo_category.name}</span>
{/capture}

{capture name="content"}
	{literal}
		<script type="text/javascript">
			function del(id_room, id_image)	{
				if(confirm("Вы уверены?")) {
					xajax_deleteImage(id_room, id_image);
				}
			}
				
			$(function() {			
				$( "#sortable" ).sortable({
					opacity: 0.8,
					revert: true,
					axis:'y'
				});
			});		
			$(document).ready(function() { 
				$("#sortable").sortable({

					  update : function () { 
						var mass_sort = $('#sortable').sortable('toArray');				      
						xajax_Sort(mass_sort); 
					  }
				});
			});
		</script>
	{/literal}
	<div class="ibox-content">
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
		{include file="common/errors_block.tpl"}
		 <div class="form-group">
            <a href="/admin/room/list/{$page}/{if $get_param}{$get_param}{/if}"><i class="fa fa-hand-o-left"></i> Назад к списку</a>
        </div>
		<div class="form-group">
			{if $smarty.get.save}<b style="color:#4BB43F;">СОХРАНЕНО</b>{/if}
		</div>
		<div class="form-group">
            <div class="col-sm-12">
               Мультизагрузка (возможен выбор более 1 фото)
			   <div class="form-group">
					<label class="col-sm-2 control-label">Название :</label>
					<div class="col-sm-3">
						<textarea class="form-control"  name="name_img" id="name_img" ></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Вид изображения :</label>
					<div class="col-sm-3">
						<select class="form-control" name="type_resize" id="type_resize">
							<option value="1">Обрезать края</option>
							<option value="2">Добавлять пустые поля</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-3">
						<div id="spanButtonPlaceHolder"></div><br/>
						<div id="divStatus"></div><br/>
						<div id="fsUploadProgress" style="color:#777;"></div>
						<a id="btnCancel" href="#cancel"></a>
					</div>
				</div>
			</div>
        </div>
		<input type="hidden" name="id_room" id="id_room" value="{$id_room}" />
	</form>	
				
	{literal}
	<script type="text/javascript" src="/js/SWFUpload/swfupload.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/swfupload.queue.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/fileprogress.js"></script>
	<script type="text/javascript" src="/js/SWFUpload/handlers.js"></script>
	<script type="text/javascript">
		var swfu;
		var myQueueComplete = function () {
			window.location.reload();
		};
			
		function uploadStart() {	// функция вызывается перед загрузкой фоток				
			this.setPostParams({"id_from_swfupload": $("#id_room").val(),								
								"type_resize": $("#type_resize").val(),
								"name_img": $("#name_img").val()
								});
			return true;
		}
		
		window.onload = function() {
			
			var settings = {
				flash_url : "/js/SWFUpload/swfupload.swf",
				upload_url: "/admin/room_photo/add/{/literal}{$page}{literal}/{/literal}{$id_room}{literal}/?swfupload=1",
				//post_params: { "id_from_swfupload": '{/literal}{$id_room}{literal}'},
				file_size_limit : "15 MB",
				file_types : "*.jpg; *.png; *.jpeg; *.gif",
				file_types_description : "Images",
				file_upload_limit : 100,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress",
					cancelButtonId : "btnCancel"
				},
				debug: false,

				// Button settings	
				button_image_url: "/js/SWFUpload/button_bgr.png",
				button_width: 100,
				button_height: 30,
				button_text_left_padding: 21,
				button_text_top_padding: 4,
				button_text : "<span class=\"uploadBtn\">Обзор...</span>",
				button_text_style : ".uploadBtn {font-size:16px; font-family:Arial; background-color:#FF0000;}",
				button_placeholder_id: "spanButtonPlaceHolder",

				// The event handler functions are defined in handlers.js
				swfupload_preload_handler : preLoad,
				swfupload_load_failed_handler : loadFailed,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : myQueueComplete	// Queue plugin event
			};

			swfu = new SWFUpload(settings);
		};
	</script>
	{/literal}
	
	
	{if $data_photo}	
	<div class="sort_list gall_block">
		<ul id="sortable"  class="sort" style="list-style-type:none;">
			{foreach from=$data_photo item=cur name=loop}
				<li style="margin-bottom:20px;" id="item_{$cur.id}">
					<form class="form-horizontal" id="form_item_{$id_room}" name="form_img_{$id_room}" action="" method="post">
						<div class="row">
							<div class="col-sm-3">
								<img  src="/uploaded/room/{$cur.id_room}/{$cur.id}_sm.{$cur.ext}" class="photo" />
							</div>
							<div class="col-sm-3">					
								<textarea  id="name_img_{$cur.id}" name="name_img_{$cur.id}" >{$cur.name}</textarea>
								<a href="" style="font-size:18px;" onclick="xajax_reSaveName(document.getElementById('name_img_{$cur.id}').value, {$cur.id}); return false;"><i class="fa fa-floppy-o" aria-hidden="true" title="Сохранить название"></i></a>									
							</div>
							<div class="col-sm-3">
								<i class="fa fa-times" onclick="del('{$cur.id_room}','{$cur.id}');" title="Удалить изображение" alt="Удалить изображение" style="cursor:pointer;font-size:16px;margin-left:10px;" ></i>
							</div>
						</div>
					</form>
				</li>					
			{/foreach}
		</ul>
	</div>
</div>
		<!--<hr class="sep" />
	
		<h1>Фотогалерея</h1>
		
		<div class="sort_list gall_block">
				
			<ul id="sortable"  class="sort">
				{foreach from=$data_photo item=cur name=loop}
					
					<li id="item_{$cur.id}">
						<form id="form_item_{$id_room}" name="form_img_{$id_room}" action="" method="post">
							<div style="width:50%;"><div class="padd">
									<a href="/uploaded/room/{$cur.id_room}/{$cur.id}.{$cur.ext}" target="_blank"><img src="/uploaded/room/{$cur.id_room}/{$cur.id}_sm.{$cur.ext}" alt="{$cur.name}" title="{$cur.name}" class="photo" /></a>
								<img class="pointer" src="/img/admin/del.png" onclick="del('{$cur.id_room}','{$cur.id}');" title="Удалить изображение" alt="Удалить изображение" style="cursor:pointer;" /><br><br>
							</div></div>

							<div style="width:50%;"><div class="padd">									
								<textarea  id="name_img_{$cur.id}" name="name_img_{$cur.id}" >{$cur.name}</textarea>
								<a href="" onclick="xajax_reSaveName(document.getElementById('name_img_{$cur.id}').value, {$cur.id}); return false;"><img class="pointer" src="/img/admin/save.png" title="Сохранить название" alt="Сохранить название" /></a>								
							</div></div>

							<div class="clean"></div>
						</form>
					</li>					
				{/foreach}
			</ul>
			
		</div>
		-->
			
	{/if}
	

{/capture}

{include file="admin/common/base_page.tpl"}