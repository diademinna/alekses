{capture name="content_name"}
	Номера
{/capture}

{capture name="content"}

<script type="text/javascript">
	function delRecord(page, id, get_param){
		if(confirm("Вы уверены?")) {
			top.window.location = "/admin/room/list/"+page+"/delete/"+id+"/"+get_param;
		}
	}
	
	$(function() {			
		$( "#sortable" ).sortable({
			opacity: 0.8,
			revert: true,
			axis:'y'
		});
	});		
	$(document).ready(function() { 
		$("#sortable").sortable({
				
				update : function () { 
				var mass_sort = $('#sortable').sortable('toArray');				      
				xajax_Sort(mass_sort, $('#min_pos').val()); 
				}
		});
	});
    function functionActivateMain(obj, id_room) {
        var num_check=0;
        $('#forma_category input[name="main"]:checkbox:checked').each(function(){
            if($(this).val() == 1)
                num_check++;
        });
        if (num_check>=3) { 
            $(obj).removeAttr('checked');
            alert("Нельзя выбрать больше 3 номеров для главной страницы");
        }
        else 
            xajax_ActivateMain(id_room);

    }
    
        
        


        
    
</script>
{if $data}
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <input type="hidden" name="number_main" value="{$number_main}"/>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/room/add/{if $get_param}{$get_param}{/if}">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
				<div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">

                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '{$get_param}');">
                        {include file="admin/common/select_count_page.tpl"}
                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <!--<th>На баннере</th>-->
                            <th>Категория</th>
                            <th>Название</th>
                            <th>Площадь</th>
                            <th>Цена</th>
                            <th>На главной</th>
                            <th>На сайте</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>  
                        {foreach from=$data item=cur name=loop}
                            <tr id="item_{$cur.id}">
                                <!--<td>
                                    <div class="radio">
                                        <input id="banner{$cur.id}" type="radio" name="banner" value="{$cur.banner}" {if $cur.banner==1}checked{/if} onclick="xajax_ActivateBanner('{$cur.id}');">
                                        <label class="label_radio" for="banner{$cur.id}"></label>
                                    </div>
                                </td>-->
                                <td>{$cur.name_category}</td>
                                <td>{$cur.name} <span style="font-style:italic;">({if $cur.seats==1}1-местный{elseif $cur.seats==2}2-местный{elseif $cur.seats==3}3-местный{/if}, {if $cur.apartment==1}1-комнатный{elseif $cur.apartment==2}2-комнатный{elseif $cur.apartment==3}3-комнатный{/if})</span></td>
                                <td>{$cur.area} кв.м.</td>
                                <td>{$cur.price} руб/сут</td>
                                
                                <td>
                                    <div class="checkbox">
                                        <input id="main{$cur.id}" type="checkbox" name="main" value="{$cur.main}" {if $cur.main==1}checked{/if} onclick="functionActivateMain(this, '{$cur.id}');">
                                        <label class="label_checkbox" for="main{$cur.id}"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="checkbox">
                                        <input id="male{$cur.id}" type="checkbox" name="my_checkbox" value="{$cur.active}" {if $cur.active==1}checked{/if} onclick="xajax_Activate('{$cur.id}')">
                                        <label class="label_checkbox" for="male{$cur.id}"></label>
                                    </div>
                                </td>
                                <td style="font-size:18px;">
									<a href="/admin/room_photo/add/{$page}/{$cur.id}/{if $get_param}{$get_param}{/if}"><i class="fa fa-picture-o" title="Фото" alt="Фото"></i></a> &nbsp &nbsp
                                    <a href="/admin/room/add/{$page}/edit/{$cur.id}/{if $get_param}{$get_param}{/if}"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('{$page}', '{$cur.id}', '{$get_param}');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                {assign var="min_pos" value=$cur.pos}
                            </tr>
                    {/foreach}
                    </tbody>
                    <input type="hidden" id="min_pos" value="{$min_pos}">
                </table>
            </div>
        </div>  
		{if $pager_string}<div class="pager">{$pager_string}</div>{/if}     
    </div>
</form>     

{else}
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/room/add/{if $get_param}{$get_param}{/if}">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>

{/if}
	
{/capture}

{include file="admin/common/base_page.tpl"}