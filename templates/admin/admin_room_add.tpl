{capture name="content_name"}
	Номера / {if $data.name}Редактировать - {$data.name}{else}Добавить{/if}
{/capture}


{capture name="content"}
<div class="ibox-content">
    <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        {include file="common/errors_block.tpl"}
        <div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-8">
                <input name="name" class="form-control" type="text" value="{$data.name}" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Категория* :</label>
            <div class="col-sm-10">
                {foreach from=$data_category item=cur}
                    <div class="radio">
                        <input id="category{$cur.id}_{$data.id}" type="radio" name="id_category" value="{$cur.id}" {if $cur.id==$data.id_category}checked{/if}>
                        <label class="label_radio" for="category{$cur.id}_{$data.id}">{$cur.name}</label>
                    </div>
                {/foreach}
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Количество мест* :</label>
            <div class="col-sm-2">
                <div class="radio">
                    <input id="seats1_{$data.id}" type="radio" name="seats" value="1" {if $data.seats == 1}checked{/if}>
                    <label class="label_radio" for="seats1_{$data.id}">1 - местный</label>
                </div>
                <div class="radio">
                    <input id="seats2_{$data.id}" type="radio" name="seats" value="2" {if $data.seats == 2}checked{/if}>
                    <label class="label_radio" for="seats2_{$data.id}">2 - местный</label>
                </div>
                <div class="radio">
                    <input id="seats3_{$data.id}" type="radio" name="seats" value="3" {if $data.seats == 3}checked{/if}>
                    <label class="label_radio" for="seats3_{$data.id}">3 - местный</label>
                </div>
            </div>
            <label class="col-sm-2 control-label">Количество комнат* :</label>
            <div class="col-sm-2">
                <div class="radio">
                    <input id="apartment1_{$data.id}" type="radio" name="apartment" value="1" {if $data.apartment == 1}checked{/if}>
                    <label class="label_radio" for="apartment1_{$data.id}">1 - комнатный</label>
                </div>
                <div class="radio">
                    <input id="apartment2_{$data.id}" type="radio" name="apartment" value="2" {if $data.apartment == 2}checked{/if}>
                    <label class="label_radio" for="apartment2_{$data.id}">2 - комнатный</label>
                </div>
                <div class="radio">
                    <input id="apartment3_{$data.id}" type="radio" name="apartment" value="3" {if $data.apartment == 3}checked{/if}>
                    <label class="label_radio" for="apartment3_{$data.id}">3 - комнатный</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Площадь* :</label>
            <div class="col-sm-2 input-group">
                <input type="text" class="form-control" name="area" value="{$data.area}">
                <div class="input-group-addon">кв. м.</div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Цена* :</label>
            <div class="col-sm-2 input-group">
                <input type="text" class="form-control" name="price" value="{$data.price}">
                <div class="input-group-addon">руб. / сут.</div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Оснащенность номера :</label>
            <div class="col-sm-8">
                <div class="row">
                    {foreach from=$data_attribute item=cur name=loop}
                    <div class="col-xs-6">
                        <div class="checkbox">
                            <input id="attribute{$cur.id}" type="checkbox" name="attribute[{$cur.id}]" value="{$cur.id}" {if $data_attribute_room[{$cur.id}]}checked{/if} >
                            <label class="label_checkbox" style="padding-left:25px;" for="attribute{$cur.id}">{$cur.name}</label>
                        </div>
                    </div>
                    {if $smarty.foreach.loop.iteration%2==0}</div><div class="row">{/if}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Краткое описание* :</label>
            <div class="col-sm-10">
                <textarea class="tiny" name="anons">{$data.anons}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Описание* :</label>
            <div class="col-sm-10">
                <textarea class="tiny" name="text">{$data.text}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Изображение :</label>
            <div class="col-sm-10">
                <input  type="file" name="image" />
                <br /><br />
                <div id="photo">
                    {if $data.ext}
                        <a href="/uploaded/room/{$data.id}.{$data.ext}" target="_blank"><img src="/uploaded/room/{$data.id}_sm.{$data.ext}" class="photo" /></a>
                        &nbsp;<i onmouseover="this.style.cursor='pointer';" class="fa fa-times" onclick="if(confirm('Вы уверены?')) xajax_deleteImage('{$data.id}'); return false;"></i>
                        <input type="hidden" name="ext" value="{$data.ext}" />
                    {/if}
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>
    </form>
</div>

{/capture}

{include file="admin/common/base_page.tpl"}