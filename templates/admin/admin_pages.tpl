{capture name="content_name"}
	Редактировать - {$data.name}
{/capture}

{capture name="content"}
<div class="ibox-content">
	<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
	
	{include file="common/errors_block.tpl"}

        <div class="form-group">
            <label class="col-sm-2 control-label">Название* :</label>
            <div class="col-sm-10">
                <input name="name" class="form-control" type="text" value="{$data.name}" />
            </div>
        </div>
	<div class="form-group">
            <label class="col-sm-2 control-label">Текст* :</label>
            <div class="col-sm-10">
                <textarea name="text" class="form-control">{$data.text}</textarea>
            </div>
        </div>
	<div class="form-group">
            <div class="col-sm-4 col-sm-offset-2">
                <input type="hidden" name="submitted" value="1" />
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
        </div>	
			
	</form>
</div>
{/capture}

{include file="admin/common/base_page.tpl"}