{capture name="content_name"}
	Текстовые страницы
{/capture}

{capture name="content"}

	<script type="text/javascript">
		function delRecord(id){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/pages/list/delete/"+id+"/";
			}
		}
	</script>
{if $data}
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
		<div class="ibox-title">
            <!--<div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/pages/add/{$id_pages}/">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
				<div class="col-xs-2">
                    Выводить по :
                </div>    
                <div class="col-xs-3">

                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '{$get_param}');">
                        {include file="admin/common/select_count_page.tpl"}
                    </select>	
                </div>
            </div>-->
        </div>
		<div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Текст</th>
                            <th width="140px">Действия</th>
                        </tr>
                    </thead>
                    <tbody id="sortable">  
                        {foreach from=$data item=cur name=loop}
                            <tr id="item_{$cur.id}">
                                <td>{$cur.name}</td>
                                <td>{$cur.text|truncate:150:"..."}</td>
                                
                                <td style="font-size:18px;">
                                    <a href="/admin/pages/add/edit/{$cur.id}/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('{$cur.id}');" onmouseover="this.style.cursor='pointer';"></i>

                                   
                                </td>
                                {assign var="min_pos" value=$cur.pos}
                            </tr>
                    {/foreach}
                    </tbody>
                    <input type="hidden" id="min_pos" value="{$min_pos}">
                </table>
            </div>
        </div>
	</div>
</form>
{else}
    <!--<div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/pages/add/{$id_pages}/">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>-->

{/if}
	
{/capture}

{include file="admin/common/base_page.tpl"}