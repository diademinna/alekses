{capture name="content_name"}
	Список аттрибутов номера
{/capture}

{capture name="content"}

    <script type="text/javascript">
   
       function delRecord(page, id, get_param){
			if(confirm("Вы уверены?")){
				top.window.location = "/admin/attribute/list/"+page+"/delete/"+id+"/"+get_param;
			}
		}
        $(document).ready(function() { 
			$("#sortable").sortable({
				  
				  update : function () { 
					var mass_sort = $('#sortable').sortable('toArray');				      
					xajax_Sort(mass_sort, $('#min_pos').val()); 
				  }
			});
		});		
       
    </script>
    {if $data}
<form action="" method="post" id="forma_category" enctype="multipart/form-data">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/attribute/add/{if $get_param}{$get_param}{/if}">
                    <i class="fa fa-plus"></i> Добавить аттрибут</a>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    Выводить по :
                </div>    
                <div class="col-xs-3">
                    <select class="form-control m-b" name="select_count_page" onchange="xajax_ChangeCountPage(this.value, '{$get_param}');">
                        {include file="admin/common/select_count_page.tpl"}
                    </select>	
                </div>
            </div>
        </div>
        <div class="ibox-content" >
            <div class="table-responsive">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Иконка</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody id="sortable">
                        {foreach from=$data item=cur name=loop}
                            <tr id="item_{$cur.id}">
                                <td>{$cur.name}</td>
                                <td>{if $cur.ext}<img height="50px" src="/uploaded/attribute/{$cur.id}.{$cur.ext}" />{else}изображение не загружено{/if}</td>
                                <td style="font-size:18px;">
                                    <a href="/admin/attribute/add/{$page}/edit/{$cur.id}/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                    <i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('{$page}', '{$cur.id}', '{$get_param}');" onmouseover="this.style.cursor='pointer';"></i>
                                </td>
                                {assign var="min_pos" value=$cur.pos}
                            </tr>
                    {/foreach}
                    </tbody>
                    <input type="hidden" id="min_pos" value="{$min_pos}">
                </table>
            </div>
        </div>  
        {if $pager_string}<div class="pager">{$pager_string}</div>{/if}        
    </div>
</form>     

{else}
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/attribute/add/{if $get_param}{$get_param}{/if}">
            <i class="fa fa-plus"></i>	Добавить аттрибут</a>
        </div>
    </div>

{/if}
	
{/capture}

{include file="admin/common/base_page.tpl"}