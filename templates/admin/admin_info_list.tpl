{capture name="content_name"}
	Общая информация
{/capture}

{capture name="content"}
{if $data}
<script type="text/javascript">
        function delRecord(page, id, get_param){
            if(confirm("Вы уверены?")) {
                top.window.location = "/admin/info/list/delete/"+id+"/";
            }
        }
</script>
<form action="" method="post" id="forma_category">
    <div class="ibox float-e-margins">
       <!-- <div class="ibox-title">
            <div class="row">
                <div class="col-xs-3">
                    <a class="btn btn-block btn-primary compose-mail" href="/admin/info/add/">
                    <i class="fa fa-plus"></i> Добавить</a>
                </div>
            </div>
        </div>-->
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Значение</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>  
                        {foreach from=$data item=cur name=loop}
                        {if $smarty.foreach.loop.iteration == 1}
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">Информация, отображаемая на сайте</td></tr>
                        {elseif $smarty.foreach.loop.iteration == 5}
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">SEO</td></tr>
                        {elseif $smarty.foreach.loop.iteration==8}
                        <tr><td style="font-weight:700;text-align:center;background-color:#898989;color:#fff;" colspan="3">Настройки</td></tr>
                        {/if}
                        <tr id="item_{$cur.id}">
                            <td>{$cur.name}</td>
                            <td>{$cur.text}</td>
                            <td style="font-size:18px;">
                                    <a href="/admin/info/add/edit/{$cur.id}/"><i class="fa fa-pencil" title="Редактировать" alt="Редактировать"></i></a> &nbsp &nbsp
                                <!--<i class="fa fa-times" title="Удалить" alt="Удалить" onclick="delRecord('{$page}', '{$cur.id}', '{$get_param}');" onmouseover="this.style.cursor='pointer';"></i>-->
                            </td>
                            {assign var="min_pos" value=$cur.pos}
                        </tr>
                    {/foreach}
                    </tbody>
                    <input type="hidden" id="min_pos" value="{$min_pos}">
                </table>
            </div>
        </div>       
    </div>
</form>     

{else}
    <div class="row" style="margin-top:20px;">
        <div class="col-xs-3">
            <a class="btn btn-block btn-primary compose-mail" href="/admin/info/add/">
            <i class="fa fa-plus"></i>	Добавить</a>
        </div>
    </div>

{/if}
	
{/capture}

{include file="admin/common/base_page.tpl"}