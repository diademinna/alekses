<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>АДМИНКА</title>
	<link href="/styles/admin/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="/styles/admin/style.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>

	<!--  v1.11  -  для датапикера datepicker и sortable -->
	<link type="text/css" href="/js/ui/css/jquery-ui.css" rel="stylesheet" />
	<link type="text/css" href="/styles/admin/font-awesome.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/js/ui/jquery-ui.min.js"></script>


	{$ajaxCode}

	{include file="common/tinymce.tpl"}

<script>
    jQuery(document).ready(function() {
        $('#side-menu a[data-toggle="collapse"] + ul').on('show.bs.collapse hide.bs.collapse', function () {
            jQuery(this).closest('li').toggleClass('active');
        });
    });
</script>

</head>

<body class="pace-done">
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul id="side-menu" class="nav metismenu">
                    <li class="nav-header" style="">
                        <a href="/admin/"><div class="dropdown profile-element">
                            <span>
                                <img style="max-width:100%;" class="" src="/images/logo_admin.png" alt="image" />
                            </span>
                        </a>
                        </div>
                    </li>
                    <li{if $unit=="room"} class="active"{/if}>
                        <a href="#" data-toggle="collapse" data-target="#sidebar-collapse-1">
                            <i class="fa fa-bed" aria-hidden="true"></i>
                            <span class="nav-label">Номера</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse{if $unit=='room'} in{/if}" id="sidebar-collapse-1">
                            <li{if $subunit=="room"} class="active"{/if}><a href="/admin/room/list/">Список номеров</a></li>
                            <li{if $subunit=="attribute"} class="active"{/if}><a href="/admin/attribute/list/">Оснащение номеров</a></li>
                            <li{if $subunit=="category"} class="active"{/if}><a href="/admin/category/list/">Категории номеров</a></li>
                        </ul>
                    </li>
                    <li{if $unit=="book_room"} class="active"{/if}>
                        <a href="/admin/book_room/list" >
                            <i class="fa fa-check-square-o"></i>
                            <span class="nav-label">Бронирование номеров</span>
                        </a>
                    </li>
                    <li{if $unit=="callback"} class="active"{/if}>
                        <a href="/admin/callback/list" >
                            <i class="fa fa-phone"></i>
                            <span class="nav-label">Заявки на звонок</span>
                        </a>
                    </li>
                    <li{if $unit=="page"} class="active"{/if}>
                        <a href="#" data-toggle="collapse" data-target="#sidebar-collapse-2">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Текстовые страницы</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse{if $unit=='page'} in{/if}" id="sidebar-collapse-2">
                            <li{if $subunit=="1"} class="active"{/if}><a href="/admin/pages/add/edit/1/">О гостинице (на главной)</a></li>
                            <li{if $subunit=="2"} class="active"{/if}><a href="/admin/pages/add/edit/2/">О гостинице</a></li>
                            <li{if $subunit=="3"} class="active"{/if}><a href="/admin/pages/add/edit/3/">Услуги</a></li>
                            <li{if $subunit=="4"} class="active"{/if}><a href="/admin/pages/add/edit/4/">Правила проживания</a></li>
                            <li{if $subunit=="5"} class="active"{/if}><a href="/admin/pages/add/edit/5/">Договор оферты</a></li>
                            <li{if $subunit=="6"} class="active"{/if}><a href="/admin/pages/add/edit/6/">Правила пожарной безопасности</a></li>
                        </ul>
                    </li>
                    <li{if $unit=="contacts"} class="active"{/if}>
                        <a href="/admin/contacts/">
                            <i class="fa fa-newspaper-o"></i>
                            <span class="nav-label">Контакты</span>
                        </a>
                    </li>
                    <li{if $unit=="stock"} class="active"{/if}>
                        <a href="/admin/stock/list" >
                            <i class="fa fa-gift"></i>
                            <span class="nav-label">Акции</span>
                        </a>
                    </li>
                    <li{if $unit=="service"} class="active"{/if}>
                        <a href="/admin/service/list" >
                            <i class="fa fa-cog"></i>
                            <span class="nav-label">Дополнительные услуги (на главной)</span>
                        </a>
                    </li>
                    <li{if $unit=="review"} class="active"{/if}>
                        <a href="/admin/review/list" >
                            <i class="fa fa-comments-o"></i>
                            <span class="nav-label">Отзывы</span>
                        </a>
                    </li>
                    <li{if $unit=="info"} class="active"{/if}>
                        <a href="/admin/info/list/">
                            <i class="fa fa-cogs"></i>
                            <span class="nav-label">Настройки</span>
                        </a>
                    </li>
                    <li{if $unit=="seo"} class="active"{/if}>
                        <a href="/admin/seo/list/">
                            <i class="fa fa-line-chart"></i>
                            <span class="nav-label">Счетчики, метрика</span>
                        </a>
                    </li>
                    <!--<li{if $unit=="room"} class="active"{/if}>
                        <a href="/admin/room/list/">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Номера</span>
                        </a>
                    </li>
                    <li{if $unit=="attribute"} class="active"{/if}>
                        <a href="/admin/attribute/list/">
                            <i class="fa fa-info"></i>
                            <span class="nav-label">Аттрибуты номеров</span>
                        </a>
                    </li>
                    <li{if $unit=="category"} class="active"{/if}>
                        <a href="/admin/category/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Категориии номеров</span>
                        </a>
                    </li>
                    <li{if $unit=="info"} class="active"{/if}>
                        <a href="/admin/info/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Настройки</span>
                        </a>
                    </li>
                    <li{if $unit=="book_room"} class="active"{/if}>
                        <a href="/admin/book_room/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Заявки</span>
                        </a>
                    </li>
                    <li{if $unit=="book_room"} class="active"{/if}>
                        <a href="/admin/pages/list/">
                            <i class="fa fa-list"></i>
                            <span class="nav-label">Текстовые страницы</span>
                        </a>
                    </li>-->
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg" >
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" style="margin-bottom: 0" role="navigation">
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message">Добро пожаловать в раздел администрирования</span>
                        </li>
                        <li>
                            <a target="_blank" href="/">
                                    <i class="fa fa-sign-out"></i>На сайт
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>{$smarty.capture.content_name}</h2>
                    {if $mass_navigation}
                        <ol class="breadcrumb">
                            {if $unit=='parameter'}
                                <li><a href="/admin/parameter/list/">Все параметры</a></li>
                            {else}
                                <li><a href="/admin/category/list/">Все категории</a></li>
                            {/if}
                            
                            {foreach from=$mass_navigation item=cur name=loop}
                                {if $smarty.foreach.loop.last}
                                    <li class="active">
                                        <strong>{$cur.name}</strong>
                                    </li>
                                {else}
                                    <li>
                                        {if $unit=='parameter'}
                                            <a href="/admin/parameter/list/{$cur.id}/">{$cur.name}</a>
                                        {else}
                                            <a href="/admin/category/list/{$cur.id}/">{$cur.name}</a>
                                        {/if}
                                    </li>
                                {/if}
                            {/foreach}
                        </ol>
                    {/if}
                </div>
            </div>
            {$smarty.capture.content}
        </div>
    </div>
</body>
</html>
